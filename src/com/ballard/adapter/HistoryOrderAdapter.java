package com.ballard.adapter;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ballard.activities.R;
import com.ballard.businesses.MyFoodOrder;
import com.ballard.fragments.OrderFoodFragment;
import com.ballard.fragments.ViewOrderDetailFragment;
import com.ballard.fragments.ViewOrderFragment;
import com.ballard.models.Food;
import com.ballard.models.Order;
import com.ballard.models.PriceQuantity;
import com.ballard.utilities.RestaurantCache;
import com.ballard.utilities.StringUtil;
import com.ballard.utilities.Utility;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by ballard on 1/3/2016.
 */
public class HistoryOrderAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Order> listOrder;
    TextView lbOrderQuantity;
    TextView lbRequestNoteContent;
    TextView lbTotal;
    private FragmentManager fragmentManager;

    public HistoryOrderAdapter(Context context, ArrayList<Order> listOrder, FragmentManager fragmentManager) {
        this.context = context;
        this.listOrder = listOrder;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public int getCount() {
        return listOrder.size();
    }

    @Override
    public Object getItem(int position) {
        return listOrder.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context.getApplicationContext()).inflate(R.layout.list_item_histories_order, null);
        }
        Order order = listOrder.get(position);
        TextView lbHisOrderId = (TextView) convertView.findViewById(R.id.lbHisOrderId);
        TextView lbHisOrderDate = (TextView) convertView.findViewById(R.id.lbHisOrderDate);
        TextView lbAmountOrderNoTax = (TextView) convertView.findViewById(R.id.lbAmountOrderNoTax);
        TextView lbAmountOrderService = (TextView) convertView.findViewById(R.id.lbAmountOrderService);
        TextView lbAmountOrderTax = (TextView) convertView.findViewById(R.id.lbAmountOrderTax);
        TextView lbAmountTotal = (TextView) convertView.findViewById(R.id.lbAmountTotal);
        Button bntViewDetailOrder = (Button) convertView.findViewById(R.id.bntViewDetailOrder);
        bntViewDetailOrder.setOnClickListener(new ButtonViewOrderDetails(position));
        Locale locale = StringUtil.getLocale(RestaurantCache.GetInstance().get().getDefaultsList().get(0).getCurrencyCode());
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);

        String strOrderId = StringUtil.format("{0}: {1}", "Order ID", String.valueOf(order.getOrderId()));

        /*SimpleDateFormat format = new SimpleDateFormat("MMM dd,yyyy  hh:mm a");
        String date = format.format(Date.parse("Your date string"));*/

        //Date orderDate = new Date(order.getTimeOrder());
        //String stringDate = DateFormat.getDateTimeInstance().format(orderDate);
        String stringDate = Utility.displayDateTimeFromUTCToLocalTime(order.getTimeOrder());
        String strOrderDate = StringUtil.format("{0}: {1}", "Order Date", stringDate);
        String strAmount = StringUtil.format("{0}: {1}", "Amount(Without Tax)", currencyFormatter.format(order.getAmount()));
        String strService = StringUtil.format("{0}: {1}", "Service", currencyFormatter.format(order.getServiceAmount()));
        String strTax = StringUtil.format("{0}: {1}", "Tax", currencyFormatter.format(order.getTaxAmount()));
        String strTotalAmount = StringUtil.format("{0}: {1}", "Total Amount", currencyFormatter.format(order.getTotalAmount()));
        lbHisOrderId.setText(strOrderId);
        lbHisOrderDate.setText(strOrderDate);
        lbAmountOrderNoTax.setText(strAmount);
        lbAmountOrderService.setText(strService);
        lbAmountOrderTax.setText(strTax);
        lbAmountTotal.setText(strTotalAmount);

        return convertView;
    }

    private class ButtonViewOrderDetails implements View.OnClickListener {
        private int pos;

        public ButtonViewOrderDetails(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            int orderID = listOrder.get(pos).getOrderId();
            MyFoodOrder.getInstance().setOrderId(orderID);
            ViewOrderDetailFragment viewOrderDetailFragment = new ViewOrderDetailFragment();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.frame_container, viewOrderDetailFragment);
            ft.addToBackStack(ViewOrderFragment.class.getName());
            ft.commit();
        }
    }

}
