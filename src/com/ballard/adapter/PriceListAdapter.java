package com.ballard.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ballard.activities.R;
import com.ballard.models.Food;
import com.ballard.models.PriceQuantity;
import com.ballard.utilities.RestaurantCache;
import com.ballard.utilities.StringUtil;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by ballard on 1/3/2016.
 */
public class PriceListAdapter extends BaseAdapter {
    private Context context;
    private Food selectedFood;
    private EditText txtQuantity;
    private TextView lbTotalMoney;
    private TextView lbPriceDescription;
    NumberFormat currencyFormatter;
    PriceQuantity currentPrice;

    public PriceListAdapter(Context context, Food selectedFood) {
        this.context = context;
        this.selectedFood = selectedFood;
    }

    @Override
    public int getCount() {
        return selectedFood.getPriceQuantities().size();
    }

    @Override
    public Object getItem(int position) {
        return selectedFood.getPriceQuantities().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item_price, null);
        }

        txtQuantity = (EditText) convertView.findViewById(R.id.txtQuantity);
        lbPriceDescription = (TextView) convertView.findViewById(R.id.lbPriceDescription);
        lbTotalMoney = (TextView) convertView.findViewById(R.id.lbTotalMoney);
        currentPrice = selectedFood.getPriceQuantities().get(position);
        Locale locale = StringUtil.getLocale(RestaurantCache.GetInstance().get().getDefaultsList().get(0).getCurrencyCode());
        currencyFormatter = NumberFormat.getCurrencyInstance(locale);
        //Price and Description
        String priceDesc = currencyFormatter.format(currentPrice.getPrice());
        priceDesc = StringUtil.format("{0}: {1}", currentPrice.getPriceName(), priceDesc);
        lbPriceDescription.setText(priceDesc);
        //Set initial quantity
        txtQuantity.setText(String.valueOf(currentPrice.getQuantity()));
        //Set initial total
        String totalMoney = currencyFormatter.format(currentPrice.getPrice() * currentPrice.getQuantity());
        lbTotalMoney = (TextView) convertView.findViewById(R.id.lbTotalMoney);
        lbTotalMoney.setText(totalMoney);
        //Increase button
        Button bntIncrease = (Button) convertView.findViewById(R.id.bntIncrease);
        bntIncrease.setOnClickListener(new IncreaseButton(convertView, position));
        //Decrease button
        Button bntDecrease = (Button) convertView.findViewById(R.id.bntDecrease);
        bntDecrease.setOnClickListener(new DecreaseButton(convertView, position));

        return convertView;
    }

    private class IncreaseButton implements View.OnClickListener {
        private int position;
        private View convertView;

        public IncreaseButton(View convertView, int position) {
            this.convertView = convertView;
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            PriceQuantity currentPrice = selectedFood.getPriceQuantities().get(position);
            int newOrderNumber = currentPrice.getQuantity() + 1;
            currentPrice.setQuantity(newOrderNumber);
            txtQuantity = (EditText) convertView.findViewById(R.id.txtQuantity);
            txtQuantity.setText(String.valueOf(newOrderNumber));
            Locale locale = StringUtil.getLocale(RestaurantCache.GetInstance().get().getDefaultsList().get(0).getCurrencyCode());
            NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
            String totalMoney = currencyFormatter.format(currentPrice.getPrice() * newOrderNumber);
            lbTotalMoney = (TextView) convertView.findViewById(R.id.lbTotalMoney);
            lbTotalMoney.setText(totalMoney);
        }
    }

    private class DecreaseButton implements View.OnClickListener {
        private int position;
        private View convertView;

        public DecreaseButton(View convertView, int position) {
            this.convertView = convertView;
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            PriceQuantity currentPrice = selectedFood.getPriceQuantities().get(position);
            if (currentPrice.getQuantity() - 1 >= 0) {
                int newOrderNumber = currentPrice.getQuantity() - 1;
                currentPrice.setQuantity(newOrderNumber);
                txtQuantity = (EditText) convertView.findViewById(R.id.txtQuantity);
                txtQuantity.setText(String.valueOf(newOrderNumber));
                Locale locale = StringUtil.getLocale(RestaurantCache.GetInstance().get().getDefaultsList().get(0).getCurrencyCode());
                NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
                String totalMoney = currencyFormatter.format(currentPrice.getPrice() * newOrderNumber);
                lbTotalMoney = (TextView) convertView.findViewById(R.id.lbTotalMoney);
                lbTotalMoney.setText(totalMoney);

            }
        }
    }


}
