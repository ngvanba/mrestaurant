package com.ballard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ballard.activities.R;
import com.ballard.businesses.MyFoodOrder;
import com.ballard.fragments.EditRequestFoodDialog;
import com.ballard.fragments.OrderFoodFragment;
import com.ballard.models.Food;
import com.ballard.models.PriceQuantity;
import com.ballard.utilities.RestaurantCache;
import com.ballard.utilities.StringUtil;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by ballard on 1/3/2016.
 */
public class FoodSelectAdapter extends BaseAdapter {
    EditText txtQuantity;
    TextView lbRequestNoteFood;
    TextView lbTotal;
    OrderFoodFragment parent;
    private Context context;
    private ArrayList<Food> selectedFoodList;

    public FoodSelectAdapter(Context context, ArrayList<Food> selectedFoodList, OrderFoodFragment parent) {
        this.context = context;
        this.selectedFoodList = selectedFoodList;
        this.parent = parent;
    }

    @Override
    public int getCount() {
        return selectedFoodList.size();
    }

    @Override
    public Object getItem(int position) {
        return selectedFoodList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context.getApplicationContext()).inflate(R.layout.list_item_order_foods, null);
            //Get LinearLayout of Prices
            LinearLayout linearLayoutPrices = (LinearLayout) convertView.findViewById(R.id.linearLayoutPrices);
            new BuildPriceOnItem(position, linearLayoutPrices);
        }

        ViewGroup.LayoutParams layoutParams = parent.getLayoutParams();
        if (selectedFoodList.size() > 1) {
            layoutParams.height = 800;
        } else {
            layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        }
        parent.setLayoutParams(layoutParams);

        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
        lbRequestNoteFood = (TextView) convertView.findViewById(R.id.lbRequestNoteFood);
        ImageButton bntEditRequest = (ImageButton) convertView.findViewById(R.id.bntEditRequest);

        /*Bitmap iconBitmap = selectedFoodList.get(position).getIconBitmap();
        if(iconBitmap != null){
            imgIcon.setImageBitmap(iconBitmap);
        }*/
        String title = selectedFoodList.get(position).getFoodName();
        String requestNote = selectedFoodList.get(position).getRequestNote();
        txtTitle.setText(title);
        lbRequestNoteFood.setText(requestNote);
        bntEditRequest.setOnClickListener(new EditRequestButton(lbRequestNoteFood, selectedFoodList.get(position)));
        return convertView;
    }

    private class BuildPriceOnItem {
        public BuildPriceOnItem(int position, LinearLayout linearLayoutPrices) {

            Food selectedFood = selectedFoodList.get(position);
            for (int i = 0; i < selectedFood.getPriceQuantities().size(); i++) {
                View priceView = LayoutInflater.from(context.getApplicationContext()).inflate(R.layout.list_item_price, null);
                linearLayoutPrices.addView(priceView);
                txtQuantity = (EditText) priceView.findViewById(R.id.txtQuantity);
                TextView lbPriceDescription = (TextView) priceView.findViewById(R.id.lbPriceDescription);
                TextView lbTotalMoney = (TextView) priceView.findViewById(R.id.lbTotalMoney);
                PriceQuantity currentPrice = selectedFood.getPriceQuantities().get(i);
                Locale locale = StringUtil.getLocale(RestaurantCache.GetInstance().get().getDefaultsList().get(0).getCurrencyCode());
                NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
                //Price and Description
                String priceDesc = currencyFormatter.format(currentPrice.getPrice());
                priceDesc = StringUtil.format("{0}: {1}", currentPrice.getPriceName(), priceDesc);
                lbPriceDescription.setText(priceDesc);
                //Set initial quantity
                txtQuantity.setText(String.valueOf(currentPrice.getQuantity()));
                //Set initial total
                String totalMoney = currencyFormatter.format(currentPrice.getPrice() * currentPrice.getQuantity());
                lbTotalMoney = (TextView) priceView.findViewById(R.id.lbTotalMoney);
                lbTotalMoney.setText(totalMoney);
                //Increase button
                Button bntIncrease = (Button) priceView.findViewById(R.id.bntIncrease);
                bntIncrease.setOnClickListener(new IncreaseButton(priceView, i, selectedFood));
                //Decrease button
                Button bntDecrease = (Button) priceView.findViewById(R.id.bntDecrease);
                bntDecrease.setOnClickListener(new DecreaseButton(priceView, i, selectedFood));
            }
        }
    }

    private class IncreaseButton implements View.OnClickListener {
        Food selectedFood;
        private int position;
        private View convertView;

        public IncreaseButton(View convertView, int position, Food selectedFood) {
            this.convertView = convertView;
            this.position = position;
            this.selectedFood = selectedFood;
        }

        @Override
        public void onClick(View v) {
            PriceQuantity currentPrice = selectedFood.getPriceQuantities().get(position);
            int newOrderNumber = currentPrice.getQuantity() + 1;
            currentPrice.setQuantity(newOrderNumber);
            txtQuantity = (EditText) convertView.findViewById(R.id.txtQuantity);
            txtQuantity.setText(String.valueOf(newOrderNumber));
            Locale locale = StringUtil.getLocale(RestaurantCache.GetInstance().get().getDefaultsList().get(0).getCurrencyCode());
            NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
            String totalMoney = currencyFormatter.format(currentPrice.getPrice() * newOrderNumber);
            TextView lbTotalMoney = (TextView) convertView.findViewById(R.id.lbTotalMoney);
            lbTotalMoney.setText(totalMoney);
            parent.UpdateTotal();
        }
    }

    private class DecreaseButton implements View.OnClickListener {
        Food selectedFood;
        private int position;
        private View convertView;

        public DecreaseButton(View convertView, int position, Food selectedFood) {
            this.convertView = convertView;
            this.position = position;
            this.selectedFood = selectedFood;
        }

        @Override
        public void onClick(View v) {
            PriceQuantity currentPrice = selectedFood.getPriceQuantities().get(position);
            if (currentPrice.getQuantity() - 1 >= 0) {
                int newOrderNumber = currentPrice.getQuantity() - 1;
                currentPrice.setQuantity(newOrderNumber);
                txtQuantity = (EditText) convertView.findViewById(R.id.txtQuantity);
                txtQuantity.setText(String.valueOf(newOrderNumber));
                Locale locale = StringUtil.getLocale(RestaurantCache.GetInstance().get().getDefaultsList().get(0).getCurrencyCode());
                NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
                String totalMoney = currencyFormatter.format(currentPrice.getPrice() * newOrderNumber);
                TextView lbTotalMoney = (TextView) convertView.findViewById(R.id.lbTotalMoney);
                lbTotalMoney.setText(totalMoney);
                parent.UpdateTotal();
            }
        }
    }

    private class EditRequestButton implements View.OnClickListener {
        private TextView lbRequestNote;
        private Food selectedFood;

        public EditRequestButton(TextView lbRequestNote, Food selectedFood) {
            this.lbRequestNote = lbRequestNote;
            this.selectedFood = selectedFood;
        }

        @Override
        public void onClick(View v) {
            MyFoodOrder.getInstance().setCurrentFood(selectedFood);
            MyFoodOrder.getInstance().setViewRequestNote(lbRequestNote);
            EditRequestFoodDialog dialog = new EditRequestFoodDialog();
            dialog.show(parent.getFragmentManager(), "");
        }
    }


}
