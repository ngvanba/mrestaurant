package com.ballard.adapter;

import android.app.FragmentManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ballard.activities.R;
import com.ballard.businesses.MyFoodOrder;
import com.ballard.fragments.CommentFoodFragmentDialog;
import com.ballard.fragments.RateFoodFragmentDialog;
import com.ballard.models.Food;
import com.ballard.models.PriceQuantity;
import com.ballard.utilities.RestaurantCache;
import com.ballard.utilities.StringUtil;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by ballard on 1/3/2016.
 */
public class ViewOrderDetailsAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Food> orderFoodList;
    TextView lbOrderQuantity;
    TextView lbRequestNoteContent;
    TextView lbTotal;
    FragmentManager fragmentManager;

    public ViewOrderDetailsAdapter(Context context, ArrayList<Food> orderFoodList, FragmentManager fragmentManager) {
        this.context = context;
        this.orderFoodList = orderFoodList;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public int getCount() {
        return orderFoodList.size();
    }

    @Override
    public Object getItem(int position) {
        return orderFoodList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context.getApplicationContext()).inflate(R.layout.list_item_view_order_details, null);
        }

        LinearLayout linearLayoutRequest = (LinearLayout) convertView.findViewById(R.id.linearLayoutRequest);


        TextView lbFoodName = (TextView) convertView.findViewById(R.id.lbFoodName);
        lbRequestNoteContent = (TextView) convertView.findViewById(R.id.lbRequestNoteContent);
        TextView lbPriceQuantityDescription = (TextView) convertView.findViewById(R.id.lbPriceQuantityDescription);
        TextView lbFoodOrderMoney = (TextView) convertView.findViewById(R.id.lbFoodOrderMoney);

        Button btnRateFood = (Button) convertView.findViewById(R.id.btnRateFood);
        Button btnAddComment = (Button) convertView.findViewById(R.id.btnAddComment);

        btnRateFood.setOnClickListener(new ButtonRate(position));
        btnAddComment.setOnClickListener(new ButtonComment(position));


        Food food = orderFoodList.get(position);
        String requestNote = food.getRequestNote();
        PriceQuantity priceQuantity = food.getPriceQuantities().get(0);
        Locale locale = StringUtil.getLocale(RestaurantCache.GetInstance().get().getDefaultsList().get(0).getCurrencyCode());
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
        //Price and Description
        String price = currencyFormatter.format(priceQuantity.getPrice());
        String priceDesc = StringUtil.format("{0} {1} x {2} = ", priceQuantity.getPriceName(), price, String.valueOf(priceQuantity.getQuantity()));
        double totalFoodItemNumber = priceQuantity.getPrice() * priceQuantity.getQuantity();
        String totalFoodItemMoney = currencyFormatter.format(totalFoodItemNumber);

        lbFoodName.setText(food.getFoodName());
        lbPriceQuantityDescription.setText(priceDesc);
        lbFoodOrderMoney.setText(totalFoodItemMoney);
        if (requestNote.length() > 0) {
            linearLayoutRequest.setVisibility(View.VISIBLE);
        } else {
            linearLayoutRequest.setVisibility(View.GONE);
        }
        lbRequestNoteContent.setText(requestNote);
        return convertView;
    }

    private class ButtonRate implements View.OnClickListener {
        private int pos;

        public ButtonRate(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            MyFoodOrder.getInstance().setCurrentFood(orderFoodList.get(pos));
            RateFoodFragmentDialog dialog = new RateFoodFragmentDialog();
            dialog.show(fragmentManager, "");
        }
    }

    private class ButtonComment implements View.OnClickListener {
        private int pos;

        public ButtonComment(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            MyFoodOrder.getInstance().setCurrentFood(orderFoodList.get(pos));
            MyFoodOrder.getInstance().setShowPanelWriteComment(true);
            CommentFoodFragmentDialog dialog = new CommentFoodFragmentDialog();
            dialog.show(fragmentManager, "");
        }
    }

}
