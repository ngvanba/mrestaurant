package com.ballard.adapter;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Filter;

import com.ballard.activities.R;
import com.ballard.constants.CommonConstant;
import com.ballard.localstore.UserLocalStore;
import com.ballard.models.SuggestGetSet;
import com.ballard.utilities.StringUtil;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ballard on 3/13/2016.
 */
public class RestaurantSuggestionAdapter extends ArrayAdapter<String> {
    protected static final String TAG = "SuggestionAdapter";
    private List<String> suggestions;
    private UserLocalStore userLocalStore;

    public RestaurantSuggestionAdapter(Context context, String nameFilter) {
        super(context, R.layout.res_dropdown_item_twoline);
        suggestions = new ArrayList<String>();
        userLocalStore = new UserLocalStore(context);
    }

    @Override
    public int getCount() {
        return suggestions.size();
    }

    @Override
    public String getItem(int index) {
        return suggestions.get(index);
    }

    @Override
    public Filter getFilter() {
        Filter myFilter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                JsonParse jp = new JsonParse();
                if (constraint != null) {
                    // A class that queries a web API, parses the data and
                    // returns an ArrayList<GoEuroGetSet>
                    List<SuggestGetSet> new_suggestions = jp.getParseJsonWCF(constraint.toString());
                    suggestions.clear();
                    for (int i = 0; i < new_suggestions.size(); i++) {
                        suggestions.add(new_suggestions.get(i).getName());
                    }

                    // Now assign the values and count to the FilterResults
                    // object
                    filterResults.values = suggestions;
                    filterResults.count = suggestions.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence contraint,
                                          FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return myFilter;
    }

    public class JsonParse {
        double current_latitude, current_longitude;

        public JsonParse() {
        }

        public JsonParse(double current_latitude, double current_longitude) {
            this.current_latitude = current_latitude;
            this.current_longitude = current_longitude;
        }

        public List<SuggestGetSet> getParseJsonWCF(String sName) {
            List<SuggestGetSet> ListData = new ArrayList<SuggestGetSet>();
            try {
                String temp = sName.replace(" ", "%20");
                String token = userLocalStore.getLoggedInUser().getTokenId();
                String url = StringUtil.format("{0}/{1}/{2}/{3}", CommonConstant.SERVER_ADDRESS, "restaurant", temp, token);
                URL js = new URL(url);
                //URL js = new URL("http://webheavens.com/suggestion.php?name="+temp);
                URLConnection jc = js.openConnection();
                BufferedReader reader = new BufferedReader(new InputStreamReader(jc.getInputStream()));
                String line = reader.readLine();
                /*JSONObject jsonResponse = new JSONObject(line);
                JSONArray jsonArray = jsonResponse.getJSONArray("results");*/

                JSONObject objects = (JSONObject) new JSONTokener(line).nextValue();
                JSONArray jsonArray = objects.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject r = jsonArray.getJSONObject(i);
                    ListData.add(new SuggestGetSet(r.getString("restaurant_id"), r.getString("restaurant_name")));
                }
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            return ListData;

        }

    }
}
