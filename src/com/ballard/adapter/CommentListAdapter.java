package com.ballard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ballard.activities.R;
import com.ballard.models.Comment;
import com.ballard.utilities.Utility;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ballard on 1/3/2016.
 */
public class CommentListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Comment> commentList;

    public CommentListAdapter(Context context, ArrayList<Comment> commentList) {
        this.context = context;
        this.commentList = commentList;
    }

    @Override
    public int getCount() {
        return commentList.size();
    }

    @Override
    public Object getItem(int position) {
        return commentList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context.getApplicationContext()).inflate(R.layout.list_item_comment, null);
        }

        ImageView imageUser = (ImageView) convertView.findViewById(R.id.imageUser);
        TextView lbUserName = (TextView) convertView.findViewById(R.id.lbUserName);
        TextView lbCommentContent = (TextView) convertView.findViewById(R.id.lbCommentContent);
        TextView lbCommentDateTime = (TextView) convertView.findViewById(R.id.lbCommentDateTime);


        Comment comment = commentList.get(position);

        lbUserName.setText(comment.getFullName());
        lbCommentContent.setText(comment.getComment());
        Picasso.with(context).load(R.drawable.ic_no_image).into(imageUser);
        lbCommentDateTime.setText(Utility.displayDateTimeFromUTCToLocalTime(comment.getUpdatedAt()));
        return convertView;
    }

}
