package com.ballard.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.ballard.activities.LoginActivity;
import com.ballard.activities.R;
import com.ballard.constants.AppConstant;
import com.ballard.constants.CommonConstant;
import com.ballard.localstore.UserLocalStore;
import com.ballard.utilities.StringUtil;
import com.facebook.login.LoginManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ballard on 3/12/2016.
 */
public class RestaurantSearchAutoCompleteAdapter extends BaseAdapter implements Filterable {
    private Context mContext;
    private List<String> resultList = new ArrayList<String>();
    private UserLocalStore userLocalStore;

    public RestaurantSearchAutoCompleteAdapter(Context context) {
        mContext = context;
        userLocalStore = new UserLocalStore(context);
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public String getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.res_dropdown_item_twoline, null);
        }
        ((TextView) convertView.findViewById(R.id.lbResSuggestion)).setText(getItem(position));
        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    List<String> restaurants = findRestaurantSuggestions(constraint.toString());
                    // Assign the data to the FilterResults
                    filterResults.values = restaurants;
                    filterResults.count = restaurants.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    resultList = (List<String>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }

    private List<String> findRestaurantSuggestions(String keySearch) {
        List<String> resultRestaurantSuggestions = new ArrayList<>();
        try {
            String temp = URLEncoder.encode(keySearch, "UTF-8");
            String token = userLocalStore.getLoggedInUser().getTokenId();
            String url = StringUtil.format("{0}/{1}/{2}/{3}/{4}", CommonConstant.SERVER_ADDRESS, "restaurants", "indexes", temp, token);
            URL js = new URL(url);
            URLConnection jc = js.openConnection();
            StringBuilder jsonResults = new StringBuilder();
            InputStreamReader in = new InputStreamReader(jc.getInputStream());
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }

            JSONObject objects = (JSONObject) new JSONTokener(jsonResults.toString()).nextValue();
            int resultCode = objects.getInt(AppConstant.Common.RESULT_KEY.toString());
            String message = objects.getString(AppConstant.Common.MESSAGE_KEY.toString());
            if (resultCode == CommonConstant.RESULT_UNAUTHORIZED) {
                Toast.makeText(mContext, mContext.getString(R.string.warning_message), Toast.LENGTH_LONG).show();
                //UNAUTHORIZED
                userLocalStore.clearUserData();
                try {
                    LoginManager.getInstance().logOut();
                } catch (Exception ex) {
                    Log.d("Error:", ex.getMessage());
                }
                mContext.startActivity(new Intent(mContext, LoginActivity.class));
                /*AlertDialog alert = new AlertDialog.Builder(mContext).create();
                alert.setTitle(mContext.getString(R.string.warning_message));
                alert.setMessage(message);
                alert.setButton(DialogInterface.BUTTON_POSITIVE,
                        mContext.getString(R.string.ok_button), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                userLocalStore.clearUserData();
                                try {
                                    LoginManager.getInstance().logOut();
                                } catch (Exception ex) {
                                    Log.d("Error:", ex.getMessage());
                                }
                                mContext.startActivity(new Intent(mContext, LoginActivity.class));
                            }
                        });
                alert.show();*/
            } else if (resultCode == CommonConstant.RESULT_FAILED_INVALID_INPUT) {
                //FAILED_INVALID_INPUT

            } else {
                //SUCCESS
                JSONArray results = objects.getJSONArray(AppConstant.Common.DATA_KEY.toString());
                for (int i = 0; i < results.length(); i++) {
                    String suggestionName = results.getString(i);
                    resultRestaurantSuggestions.add(suggestionName);
                }
            }

        } catch (JSONException e1) {
        } catch (UnsupportedEncodingException ex) {
        } catch (MalformedURLException ex) {
        } catch (IOException ex) {
        }
        return resultRestaurantSuggestions;
    }


}
