package com.ballard.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ballard.activities.R;
import com.ballard.models.Restaurant;

import java.util.ArrayList;

/**
 * Created by ballard on 1/3/2016.
 */
public class RestaurantListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Restaurant> listRestaurant;

    public RestaurantListAdapter(Context context, ArrayList<Restaurant> listRestaurant) {
        this.context = context;
        this.listRestaurant = listRestaurant;
    }

    @Override
    public int getCount() {
        return listRestaurant.size();
    }

    @Override
    public Object getItem(int position) {
        return listRestaurant.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item_select_restaurant, null);
        }

        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon_select_restaurant);
        TextView txtRestaurantName = (TextView) convertView.findViewById(R.id.lbRestaurantName);
        txtRestaurantName.setText(listRestaurant.get(position).getName());

        return convertView;
    }


}
