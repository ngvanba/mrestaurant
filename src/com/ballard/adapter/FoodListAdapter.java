package com.ballard.adapter;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ballard.activities.R;
import com.ballard.businesses.MyFoodOrder;
import com.ballard.constants.AppConstant;
import com.ballard.constants.CommonConstant;
import com.ballard.fragments.CommentFoodFragmentDialog;
import com.ballard.fragments.CurrentSelectFoodDialog;
import com.ballard.interfaces.IFood;
import com.ballard.localstore.UserLocalStore;
import com.ballard.models.CommonResult;
import com.ballard.models.Food;
import com.ballard.models.PriceQuantity;
import com.ballard.utilities.RestaurantCache;
import com.ballard.utilities.StringUtil;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ballard on 1/3/2016.
 */
public class FoodListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Food> ListFood;
    private UserLocalStore userLocalStore;
    private FragmentManager fragmentManager;

    public FoodListAdapter(Context context, ArrayList<Food> ListFood, FragmentManager fragmentManager) {
        this.context = context;
        this.ListFood = ListFood;
        userLocalStore = new UserLocalStore(context);
        this.fragmentManager = fragmentManager;
    }

    @Override
    public int getCount() {
        return ListFood.size();
    }

    @Override
    public Object getItem(int position) {
        return ListFood.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_foods, null);
            viewHolder = new ViewHolder();

            viewHolder.btnLike = (Button) convertView.findViewById(R.id.btnLike);
            viewHolder.bntComment = (Button) convertView.findViewById(R.id.btnComment);
            viewHolder.btnLike.setOnClickListener(new LikeButton(convertView, ListFood.get(position).getFoodId(), ListFood.get(position).getLiked()));
            viewHolder.btnAddCard = (Button) convertView.findViewById(R.id.btnAddCard);
            viewHolder.btnAddFavorite = (Button) convertView.findViewById(R.id.btnAddFavorite);
            viewHolder.btnAddFavorite.setOnClickListener(new FavoriteButton(position));
            viewHolder.headerItemLinearLayout = (LinearLayout) convertView.findViewById(R.id.headerItemLinearLayout);

            viewHolder.imgIcon = (ImageView) convertView.findViewById(R.id.icon);
            viewHolder.imgRated = (ImageView) convertView.findViewById(R.id.imgRated);
            viewHolder.lbFoodName = (TextView) convertView.findViewById(R.id.lbFoodName);
            viewHolder.lbFoodDescription = (TextView) convertView.findViewById(R.id.lbFoodDescription);
            viewHolder.lbPrice = (TextView) convertView.findViewById(R.id.lbPrice);
            viewHolder.iconTitleItem = (ImageView) convertView.findViewById(R.id.iconTitleItem);
            viewHolder.lbTitleItem = (TextView) convertView.findViewById(R.id.lbTitleItem);
            viewHolder.lbLike = (TextView) convertView.findViewById(R.id.lbLike);
            viewHolder.lbComment = (TextView) convertView.findViewById(R.id.lbComment);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.btnAddCard.setOnClickListener(new AddCard(position));
        viewHolder.bntComment.setOnClickListener(new CommentButton(position));

        if(ListFood.get(position).isMarkFavoriteItem()){
            viewHolder.btnAddFavorite.setText(context.getString(R.string.delete_favorite));
        }

        if (ListFood.get(position).isShowHeader()) {
            viewHolder.headerItemLinearLayout.setVisibility(View.VISIBLE);
            switch (ListFood.get(position).getHeaderType()) {
                case CommonConstant.RATED:
                    Picasso.with(context).load(R.drawable.ic_rate).into(viewHolder.iconTitleItem);
                    break;
                case CommonConstant.PROMOTION:
                    Picasso.with(context).load(R.drawable.ic_promotion).into(viewHolder.iconTitleItem);
                    break;
                case CommonConstant.FEATURED:
                    Picasso.with(context).load(R.drawable.ic_features).into(viewHolder.iconTitleItem);
                    break;
                case CommonConstant.NEW_FOOD:
                    Picasso.with(context).load(R.drawable.ic_whats_hot).into(viewHolder.iconTitleItem);
                    break;
            }
            viewHolder.lbTitleItem.setText(ListFood.get(position).getHeaderItemTitle());
        } else {
            viewHolder.headerItemLinearLayout.setVisibility(View.GONE);
        }


        //LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) imgRated.getLayoutParams();
        double rated = ListFood.get(position).getRated();
        //int marginLeft = -82;
        if (rated % 1 == 0) { //set full start
            if (rated == 5) {
                Picasso.with(context).load(R.drawable.ic_full_start5).into(viewHolder.imgRated);
            } else if (rated == 4) {
                Picasso.with(context).load(R.drawable.ic_full_start4).into(viewHolder.imgRated);
            } else if (rated == 3) {
                Picasso.with(context).load(R.drawable.ic_full_start3).into(viewHolder.imgRated);
            } else if (rated == 2) {
                viewHolder.imgStar = BitmapFactory.decodeResource(convertView.getResources(), R.drawable.ic_full_start2);
                viewHolder.imgRated.setImageBitmap(viewHolder.imgStar);
            } else if (rated == 1) {
                Picasso.with(context).load(R.drawable.ic_full_start1).into(viewHolder.imgRated);
            } else if (rated == 0) {
                Picasso.with(context).load(R.drawable.ic_full_start0).into(viewHolder.imgRated);
            }
        } else { //set half start
            if (rated == 4.5) {
                Picasso.with(context).load(R.drawable.ic_half_start45).into(viewHolder.imgRated);
            } else if (rated == 3.5) {
                Picasso.with(context).load(R.drawable.ic_half_start35).into(viewHolder.imgRated);
            } else if (rated == 2.5) {
                Picasso.with(context).load(R.drawable.ic_half_start25).into(viewHolder.imgRated);
            } else if (rated == 1.5) {
                Picasso.with(context).load(R.drawable.ic_half_start15).into(viewHolder.imgRated);
            } else if (rated == 0.5) {
                Picasso.with(context).load(R.drawable.ic_half_start05).into(viewHolder.imgRated);
            }
        }

//        Bitmap iconBitmap = ListFood.get(position).getIconBitmap();
//        if(iconBitmap != null){
//            wiewHolder.imgIcon.setImageBitmap(iconBitmap);
//            //Picasso.with(context).load(iconBitmap).into(wiewHolder.imgIcon);
//        } else {
            /*Bitmap noImage = BitmapFactory.decodeResource(convertView.getResources(),R.drawable.ic_no_image);
            wiewHolder.imgIcon.setImageBitmap(noImage);*/
        if (ListFood.get(position).getUrlImage().length() > 0) {
            Picasso.with(context).load(ListFood.get(position).getUrlImage()).placeholder(R.drawable.ic_no_image).into(viewHolder.imgIcon);
        } else {
            Picasso.with(context).load(R.drawable.ic_no_image).into(viewHolder.imgIcon);
        }

//        }

        List<PriceQuantity> listPrices = ListFood.get(position).getPriceQuantities();
        StringBuilder sbPrices = new StringBuilder();

        for (PriceQuantity itemPrice : listPrices) {
            String price =  RestaurantCache.GetInstance().getNumberFormat().format(itemPrice.getPrice());
            String stringPrice = StringUtil.format("{0}: {1} \t\t\t", itemPrice.getPriceName(), price);
            sbPrices.append(stringPrice);
        }
        Spanned sp = Html.fromHtml(sbPrices.toString());
        viewHolder.lbPrice.setText(sp);
        String strLike = String.valueOf(ListFood.get(position).getLiked());
        if (ListFood.get(position).getLiked() > 1) {
            String stringLiked = StringUtil.format("{0} {1}", strLike, context.getString(R.string.likes));
            viewHolder.lbLike.setText(stringLiked);
        } else {
            String stringLiked = StringUtil.format("{0} {1}", strLike, context.getString(R.string.like));
            viewHolder.lbLike.setText(stringLiked);
        }

        String strCommentCount = String.valueOf(ListFood.get(position).getCommentCount());
        if (ListFood.get(position).getCommentCount() > 1) {
            String stringComment = StringUtil.format("{0} {1}", strCommentCount, context.getString(R.string.comments));
            viewHolder.lbComment.setText(stringComment);
        } else {
            String stringComment = StringUtil.format("{0} {1}", strCommentCount, context.getString(R.string.comment));
            viewHolder.lbComment.setText(stringComment);
        }

        // wiewHolder.lbComment

        String title = ListFood.get(position).getFoodName();
        viewHolder.lbFoodName.setText(title);
        String desc = ListFood.get(position).getDescription();
        viewHolder.lbFoodDescription.setText(desc);
        return convertView;
    }

    private class CommentButton implements View.OnClickListener {
        private int pos;

        public CommentButton(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            //Toast.makeText(context, "Show comment this food:"+String.valueOf(foodId), Toast.LENGTH_LONG);
           /* AlertDialog alert = new AlertDialog.Builder(context).create();
            alert.setTitle("Test");
            alert.setMessage("Show comment this food:" + String.valueOf(foodId));
            alert.show();*/

            MyFoodOrder.getInstance().setCurrentFood(ListFood.get(pos));
            MyFoodOrder.getInstance().setShowPanelWriteComment(false);
            CommentFoodFragmentDialog dialog = new CommentFoodFragmentDialog();
            dialog.show(fragmentManager, "");
        }
    }

    private class ViewHolder {
        Button btnLike;
        Button bntComment;
        LinearLayout headerItemLinearLayout;

        ImageView imgIcon;
        ImageView imgRated;
        TextView lbFoodName;
        TextView lbFoodDescription;
        TextView lbPrice;

        TextView lbLike;
        TextView lbComment;

        ImageView iconTitleItem;
        TextView lbTitleItem;
        Bitmap imgStar;
        Button btnAddCard;
        Button btnAddFavorite;

    }

    private class AddCard implements View.OnClickListener {
        int pos;

        public AddCard(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            //Select food item on list
            Food foodSelect = ListFood.get(pos);
            //If this food was existed in my order -> get it. Otherwise, keep the selected food
            foodSelect = MyFoodOrder.getInstance().findFoodInOrder(foodSelect);
            //Set to current dialog
            MyFoodOrder.getInstance().setCurrentFood(foodSelect);
            CurrentSelectFoodDialog currentSelectFoodDialog = new CurrentSelectFoodDialog();
            //FragmentManager fragmentManager = fragment..getFragmentManager();
            currentSelectFoodDialog.show(fragmentManager, "");
        }
    }

    private class LikeButton implements View.OnClickListener {
        private int foodId;
        private View convertView;
        private int liked;
        private boolean status = false;

        public LikeButton(View convertView, int foodId, int liked) {
            this.convertView = convertView;
            this.foodId = foodId;
            this.liked = liked;
        }

        @Override
        public void onClick(View v) {
            String likeWord = "";
            if (status) {
                status = false;
            } else {
                status = true;
            }

            if (status) {
                //Post a like
                likeWord = (liked + 1 > 1) ? context.getString(R.string.likes) : context.getString(R.string.like);
                TextView lbLike = (TextView) convertView.findViewById(R.id.lbLike);
                String stringLiked = StringUtil.format("{0} {1}", String.valueOf(liked + 1), likeWord);
                lbLike.setText(stringLiked);
            } else {
                //Delete a like
                likeWord = (liked > 1) ? context.getString(R.string.likes) : context.getString(R.string.like);
                TextView lbLike = (TextView) convertView.findViewById(R.id.lbLike);
                String stringLiked = StringUtil.format("{0} {1}", String.valueOf(liked), likeWord);
                lbLike.setText(stringLiked);
            }

            String token = userLocalStore.getLoggedInUser().getTokenId();

            JsonObject postData = new JsonObject();
            postData.addProperty(AppConstant.Foods.FOOD_ID_KEY.toString(), foodId);
            postData.addProperty(AppConstant.Common.TOKEN_ID_KEY.toString(), token);


            if (status) {
                postLike(postData);
            } else {
                deleteLike(postData);
            }
        }

        public void postLike(JsonObject postData) {
            Log.d("Info", "This is post");
            //Post a like
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

            Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(CommonConstant.SERVER_ADDRESS_RETROFIT).client(client)
                    .build();

            IFood service = retrofit.create(IFood.class);
            Call<CommonResult> call = service.like(postData);
            call.enqueue(new Callback<CommonResult>() {
                @Override
                //public void onResponse(Call<CommonResult> response) {
                public void onResponse(Call<CommonResult> call, Response<CommonResult> response) {

                    int statusCode = response.code();
                    CommonResult jsonResponse = response.body();
                    int result = Integer.parseInt(jsonResponse.getResult());
                    if (result != 0) {
                        String likeWord = "";
                        likeWord = (liked - 1 > 1) ? context.getString(R.string.likes) : context.getString(R.string.like);
                        TextView lbLike = (TextView) convertView.findViewById(R.id.lbLike);
                        String stringLiked = StringUtil.format("{0} {1}", String.valueOf(liked - 1), likeWord);
                        lbLike.setText(stringLiked);
                        deleteLike(postData);
                    }
                    if (result == 0) {
                           /* String likeWord = "";
                            likeWord = (liked+1 > 1)?"likes":"like";
                            TextView lbLike = (TextView) convertView.findViewById(R.id.lbLike);
                            String stringLiked = StringUtil.format("{0} {1}", String.valueOf(liked+1) , likeWord);
                            lbLike.setText(stringLiked);*/
                    }

                }

                @Override
                public void onFailure(Call<CommonResult> call, Throwable t) {
                    Log.d("Error", t.getMessage());
                }
            });
        }

        public void deleteLike(JsonObject postData) {
            //Delete a like
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

            Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(CommonConstant.SERVER_ADDRESS_RETROFIT).client(client)
                    .build();

            IFood service = retrofit.create(IFood.class);
            Call<CommonResult> call = service.deleteLike(postData);
            call.enqueue(new Callback<CommonResult>() {
                @Override
                public void onResponse(Call<CommonResult> call, Response<CommonResult> response) {

                    int statusCode = response.code();
                    CommonResult jsonResponse = response.body();
                    int result = Integer.parseInt(jsonResponse.getResult());

                    if (result != 0) {
                        String likeWord = "";
                        likeWord = (liked > 1) ? context.getString(R.string.likes) : context.getString(R.string.like);
                        TextView lbLike = (TextView) convertView.findViewById(R.id.lbLike);
                        String stringLiked = StringUtil.format("{0} {1}", String.valueOf(liked), likeWord);
                        lbLike.setText(stringLiked);
                        postLike(postData);
                    }

                    if (result == 0) {
                            /*String likeWord = "";
                            likeWord = (liked > 1)?"likes":"like";
                            TextView lbLike = (TextView) convertView.findViewById(R.id.lbLike);
                            String stringLiked = StringUtil.format("{0} {1}", String.valueOf(liked) , likeWord);
                            lbLike.setText(stringLiked);*/
                    }

                }

                @Override
                public void onFailure(Call<CommonResult> call, Throwable t) {
                    Log.d("Error", t.getMessage());
                }
            });
        }
    }

    private class FavoriteButton implements View.OnClickListener {
        private int pos;

        public FavoriteButton(int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            Food food = ListFood.get(pos);
            String token = userLocalStore.getLoggedInUser().getTokenId();
            JsonObject postData = new JsonObject();
            postData.addProperty(AppConstant.Foods.FOOD_ID_KEY.toString(), food.getFoodId());
            postData.addProperty(AppConstant.Common.TOKEN_ID_KEY.toString(), token);
            if (food.isMarkFavoriteItem()) {
                //Delete Favorite
                deleteFavorite(postData, pos);
                ListFood.remove(pos);
                refreshApdater();
            } else {
                //Add Favorite
                addFavorite(postData);
            }

           /* if (status) {
                addFavorite(postData);
            } else {
                deleteFavorite(postData);
            }*/

        }
    }

    public void addFavorite(JsonObject postData) {
        //Delete a like
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(CommonConstant.SERVER_ADDRESS_RETROFIT).client(client)
                .build();

        IFood service = retrofit.create(IFood.class);
        Call<CommonResult> call = service.addFavorite(postData);
        call.enqueue(new Callback<CommonResult>() {
            @Override
            public void onResponse(Call<CommonResult> call, Response<CommonResult> response) {

                int statusCode = response.code();
                CommonResult jsonResponse = response.body();
                int result = Integer.parseInt(jsonResponse.getResult());

                /*if (result != 0) {
                    deleteFavorite(postData);
                }*/

                if (result == 0) {
                }

            }

            @Override
            public void onFailure(Call<CommonResult> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    public void deleteFavorite(JsonObject postData, int pos) {
        //Delete a like
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(CommonConstant.SERVER_ADDRESS_RETROFIT).client(client)
                .build();

        IFood service = retrofit.create(IFood.class);
        Call<CommonResult> call = service.deleteFavorite(postData);
        call.enqueue(new Callback<CommonResult>() {
            @Override
            public void onResponse(Call<CommonResult> call, Response<CommonResult> response) {

                int statusCode = response.code();
                CommonResult jsonResponse = response.body();
                int result = Integer.parseInt(jsonResponse.getResult());
                /*if(result == CommonConstant.RESULT_SUCCESS){
                    ListFood.remove(pos);
                    refreshApdater();
                }*/
                /*if (result == 0) {
                } else {
                    addFavorite(postData);
                }*/
            }

            @Override
            public void onFailure(Call<CommonResult> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    public void refreshApdater(){
        this.notifyDataSetChanged();
    }

}
