package com.ballard.businesses;

import android.widget.Button;
import android.widget.TextView;

import com.ballard.models.Food;
import com.ballard.models.PriceQuantity;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ballard on 1/11/2016.
 */
public class MyFoodOrder {
    private static MyFoodOrder instance = null;
    private ArrayList<Food> selectedFoodList;
    private Food currentFood = null;
    private JSONObject successOrder = null;
    private Button notifyCart = null;
    private TextView viewRequestNote = null;
    private int orderId = -1;
    private boolean showPanelWriteComment = true;

    private MyFoodOrder(){
        selectedFoodList = new ArrayList<Food>();
    }

    public static MyFoodOrder getInstance(){
        if(instance == null){
            instance = new MyFoodOrder();
        }
        return instance;
    }

    public void addFoodToMyOrder(Food food){
        for(Food item : selectedFoodList){
            if(item.getFoodId() == food.getFoodId()){
                selectedFoodList.remove(item);
                break;
            }
        }
        selectedFoodList.add(food);
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public Button getNotifyCart() {
        return notifyCart;
    }

    public void setNotifyCart(Button notifyCart) {
        this.notifyCart = notifyCart;
    }

    public TextView getViewRequestNote() {
        return viewRequestNote;
    }

    public void setViewRequestNote(TextView viewRequestNote) {
        this.viewRequestNote = viewRequestNote;
    }

    public double getTotalMyOrder(){
        double total = 0;
        for (Food food : selectedFoodList) {
            for (PriceQuantity itemPrice : food.getPriceQuantities()){
                total += itemPrice.getPrice() * itemPrice.getQuantity();
            }
        }
        return total;
    }

    public double getTotalQuantities(){
        double total = 0;
        for (Food food : selectedFoodList) {
            for (PriceQuantity itemPrice : food.getPriceQuantities()){
                total += itemPrice.getQuantity();
            }
        }
        return total;
    }

    public void clearMyOrder(){
        for (Food food : selectedFoodList ) {
            List<PriceQuantity> priceQuantityList = food.getPriceQuantities();
            for (PriceQuantity priceQuantity:  priceQuantityList) {
                priceQuantity.setQuantity(0);
            }
            if(priceQuantityList.size() > 0){
                priceQuantityList.get(0).setQuantity(1);
            }
        }
        selectedFoodList.clear();
    }

    public void setCurrentFood(Food currentFood){
        this.currentFood = currentFood;
    }

    public Food getCurrentSelectFood(){
        return currentFood;
    }

    public void clearCurrentSelectFood(){
        currentFood = null;
    }

    public  ArrayList<Food> getMyFoodOrder(){
        return selectedFoodList;
    }

    public Food findFoodInOrder(Food food){
        Food foodResult = food;
        for(Food foodOrder : selectedFoodList){
            if(foodOrder.getFoodId() == food.getFoodId()){
                foodResult = foodOrder;
                break;
            }
        }
        return foodResult;
    }

    public JSONObject getSuccessOrder() {
        return successOrder;
    }

    public void setSuccessOrder(JSONObject successOrder) {
        this.successOrder = successOrder;
    }

    public boolean isShowPanelWriteComment() {
        return showPanelWriteComment;
    }

    public void setShowPanelWriteComment(boolean showPanelWriteComment) {
        this.showPanelWriteComment = showPanelWriteComment;
    }
}
