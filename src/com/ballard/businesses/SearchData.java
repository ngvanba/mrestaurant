package com.ballard.businesses;

/**
 * Created by ballard on 6/25/2016.
 */
public class SearchData {
    private String keySearch;
    private static SearchData instance = null;
    public static SearchData getInstance(){
        if(instance == null){
            instance = new SearchData();
        }
        return instance;
    }

    public String getKeySearch() {
        return keySearch;
    }

    public void setKeySearch(String keySearch) {
        this.keySearch = keySearch;
    }
}
