package com.ballard.constants;

public class CommonConstant {
	public static final String TAG = "M_RESTAURANT";
	public static final String SP_NAME = "userStore";
	//public static final String PASSWORD_FIELD = "password";
	/*public static final String EMAIL_FIELD = "email";
	public static final String PWD_FIELD = "pwd";
	public static final String FIRST_NAME_FIELD = "firstname";
	public static final String LAST_NAME_FIELD = "lastname";
	public static final String BIRTHDAY_NAME_FIELD = "birthday";*/
	public static final String LOGGED_IN_KEY = "loggedIn";
	public static final String CATEGORY_ID = "CategoryId";
	/*public static final String TOKEN_ID = "tokenID";
	public static final String MESSAGE = "message";
	public static final String RESULT = "result";*/
	public static final String DATA = "data";
	public static final int UI_PAGE_SIZE = 18;

	public static final String RESTAURANT_ID = "RestaurantId";
	public static final String RESTAURANT_NAME = "RestaurantName";
	public static final String WIFI_NAME = "WifiName";
	public static final String TOP_MENU_SCREEN = "screenLevelTop";
	
	//
	public static final int TIMEOUT_CONNECTION = 100 * 15;
	//public static final String SERVER_ADDRESS = "http://10.0.3.2:8080";
	public static final String SERVER_ADDRESS = "http://www.order-com.com/mRestaurantAPI";
	public static final String SERVER_ADDRESS_RETROFIT = "http://www.order-com.com/mRestaurantAPI/";
	public static final String REGISTER_PAGE ="/user";
	public static final String CHANGE_PASSWORD_PAGE ="/user/password";

	public static final int APP_KEY =4578945;
	public static final String COUNTRY_CODE_KEY ="084";

	public static final String LOGIN_PAGE ="/user/login";
	public static final String CATEGORIES_PAGE ="/category/1/900150983cd24fb0d6963f7d28e17f72";

	public static final String EMAIL_FIELD = "email";
	public static final String PASSWORD_FIELD = "password";
	public static final String PWD_FIELD = "pwd";
	public static final String FIRST_NAME_FIELD = "firstName";
	public static final String LAST_NAME_FIELD = "lastName";
	public static final String ADDRESS1_FIELD = "address1";
	public static final String ADDRESS2_FIELD = "address2";
	public static final String CITY_FIELD = "city";
	public static final String COUNTRY_FIELD = "country";
	public static final String POSTCODE_FIELD = "postcode";
	public static final String BIRTHDAY_NAME_FIELD = "birthday";

	public static final String CURRENT_PASSWORD_FIELD = "current_password";
	public static final String NEW_PASSWORD_FIELD = "new_password";


	public static final String TOKEN_ID = "tokenID";
	public static final String MESSAGE = "message";
	public static final String RESULT = "result";
	public static final int SERVER_PAGE_SIZE = 20;

	public static final int NEW_FOOD = 1;
	public static final int RATED = 2;
	public static final int FEATURED = 3;
	public static final int PROMOTION = 4;

	public static final int PROCESSED = 1;

	//RESULT CODE DEFINE
	public static final int RESULT_SUCCESS = 0; //SUCCESS
	public static final int RESULT_UNAUTHORIZED = 1; //UNAUTHORIZED
	public static final int RESULT_FAILED_INVALID_INPUT = 2; //UNAUTHORIZED
}
