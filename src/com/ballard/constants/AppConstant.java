package com.ballard.constants;

/**
 * Created by ballard on 3/19/2016.
 */
public class AppConstant {
    public static enum  Common {
        DATA_KEY("data"),
        ORDERS_KEY("orders"),
        ORDER_KEY("order"),
        ORDER_ITEMS_KEY("order_items"),
        ORDER_SERVICES_KEY("order_service"),
        ORDER_TAX_KEY("order_tax"),
        MESSAGE_KEY("message"),
        RESULT_KEY("result"),
        TOKEN_ID_KEY("tokenID");


        private final String keyName;
        Common(String keyName) {
            this.keyName = keyName;
        }
        public String toString() {
            return this.keyName;
        }
    }
    public static enum  Foods {
        FOOD_ID_KEY("food_id"),FOOD_NAME_KEY("food_name"),FOOD_DESCRIPTION_KEY("food_description"),FOOD_PRICES_KEY("prices"),
        FOOD_PRICE_KEY("price"),FOOD_QUANTITY_KEY("qty"),FOOD_PHOTO_KEY("food_photo"),FOODS_KEY("foods"),FOOD_KEY("food"),
        HOME_KEY("home"),FOOD_NOTES_KEY("notes"),LIKED_KEY("liked"),LIKE_KEY("like"),COMMENT_COUNT_KEY("comment_count"),
        FOOD_ITEMS_KEY("food_items"),FOOD_SIZE_KEY("size"),RATED_KEY("rated"),TOP_RATED_KEY("top_rated"),VALUE_KEY("value"),
        PROMOTION_KEY("promotion"),FEATURED_KEY("featured"),FEATURE_KEY("feature"),NEW_FOOD_KEY("new_foods"), POINTS_KEY("points"),
        FOOD_PHOTO_URL_KEY("food_photo_url"),PHOTO_PROCESSED_KEY("photo_proccessed");

        private final String keyName;
        Foods(String keyName) {
            this.keyName = keyName;
        }
        public String toString() {
            return this.keyName;
        }
    }
    public static enum  Orders {
        ORDER_ID_KEY("order_id"),
        IS_PRE_ORDER_KEY("is_pre_order"),
        ID_KEY("id");

        private final String keyName;
        Orders(String keyName) {
            this.keyName = keyName;
        }
        public String toString() {
            return this.keyName;
        }
    }

    public static enum  CategoriesFood {
        CATEGORIES_KEY("categories"), CATEGORY_KEY("category"), CATEGORY_ID_KEY("category_id"),
        CATEGORY_NAME_KEY("category_name"), FOOD_COUNT_KEY("food_count"), FOOD_FAVOR_ID("food_favor_id");

        private final String keyName;
        CategoriesFood(String keyName) {
            this.keyName = keyName;
        }
        public String toString() {
            return this.keyName;
        }
    }

    public static enum  User {
        USER_ID_KEY("id"),
        EMAIL_KEY("email"),
        PWD_KEY("pwd"),
        PASSWORD_KEY("password"),
        FIRST_NAME_KEY("firstname"),
        LAST_NAME_KEY("lastname"),
        BIRTHDAY_KEY("birthday");

        private final String keyName;
        User(String keyName) {
            this.keyName = keyName;
        }
        public String toString() {
            return this.keyName;
        }
    }
    public static enum  Restaurant {
        RESTAURANTS_KEY("restaurants"),RESTAURANT_KEY("restaurant"),RESTAURANT_ID_KEY("restaurant_id"),RESTAURANT_NAME_KEY("restaurant_name"),RESTAURANT_OWNER_KEY("owner"),
        RESTAURANT_CHAIN_ID_KEY("chain_id"),RESTAURANT_CHAINS_ID_KEY("chains_id"),RESTAURANT_LAT_KEY("lat"),RESTAURANT_LNG_KEY("lng"),RESTAURANT_WIFI_KEY("wifi"), RESTAURANT_ADDRESS1_KEY("address_1"),
        RESTAURANT_ADDRESS2_KEY("address_2"),RESTAURANT_CITY_KEY("city"),RESTAURANT_COUNTRY_KEY("country"),RESTAURANT_ZIP_KEY("zip"),RESTAURANT_PHONE_KEY("phone"),
        RESTAURANT_TAGS_KEY("tags"),RESTAURANT_PHOTOS_KEY("photos"), RESTAURANT_TABLE_NAME("Restaurant"),RESTAURANT_SETTING_KEY("setting"),
        RESTAURANT_DEFAULT_KEY("defautl"), RESTAURANT_TAX_KEY("tax"), RESTAURANT_SERVICE_KEY("service"),DEFAULT_ID_KEY("id"), ALLOW_PUBLISH_MENU_KEY("allow_public_menu"),
        ALLOW_WIFI_MENU_KEY("allow_wifi_menu"), ALLOW_CODE_MENU_KEY("allow_code_menu"),MENU_ACCESS_CODE_KEY("menu_access_code"),CURRENCY_TYPE_KEY("currency_type"),WIFI_KEY("wifi_key"),
        TAX_ID_KEY("id"), TAX_NAME_KEY("tax_name"), TAX_TYPE_KEY("tax_type"), TAX_VALUE_KEY("tax_value"),
        SERVICE_ID_KEY("id"), SERVICE_NAME_KEY("service_name"), SERVICE_TYPE_KEY("service_type"), SERVICE_VALUE_KEY("service_value");

        private final String keyName;
        Restaurant(String keyName) {
            this.keyName = keyName;
        }
        public String toString() {
            return this.keyName;
        }
    }

    public static enum  Histories {
        HISTORIES_TABLE_NAME("histories"), ORDER_ITEMS_TABLE_NAME("order_items"),
        ORDER_SERVICES_TABLE_NAME("order_services"), ORDER_TAX_TABLE_NAME("order_tax");

        private final String keyName;
        Histories(String keyName) {
            this.keyName = keyName;
        }
        public String toString() {
            return this.keyName;
        }
    }

    public static enum  Order {
        ORDER_ID_KEY("order_id"), AMOUNT_KEY("amount"), SERVICE_AMOUNT_KEY("service_amount"), TAX_AMOUNT_KEY("tax_amount"),
        TOTAL_AMOUNT_KEY("total_amount"),USER_ID_KEY("user_id"), DATETIME_ORDER_KEY("time"),STATUS_KEY("status"), TABLE_ID_KEY("table_id"),
        TABLE_KEY("table");

        private final String keyName;
        Order(String keyName) {
            this.keyName = keyName;
        }
        public String toString() {
            return this.keyName;
        }
    }

    public static enum  OrderItems {
        ID_KEY("id"), ORDER_ID_KEY("order_id"), FOOD_ID_KEY("food_id"), FOOD_NAME_KEY("food_name"), QUANTITY_KEY("qty"),
        NOTES_KEY("notes"),STATUS_KEY("status"), SIZE_KEY("size"),TV_STATUS_KEY("tv_status"), LAST_UPDATE_KEY("last_update"),
        CANCELED_NOTE_KEY("canceled_note"),REJECT_NOTE_KEY("rejected_note"), RETURN_NOTE_KEY("return_note");

        private final String keyName;
        OrderItems(String keyName) {
            this.keyName = keyName;
        }
        public String toString() {
            return this.keyName;
        }
    }

    public static enum  OrderServices {
        SERVICE_ID_KEY("service_id"), ORDER_ID_KEY("order_id"), SERVICE_NAME_KEY("service_name"), AMOUNT_KEY("amount");

        private final String keyName;
        OrderServices(String keyName) {
            this.keyName = keyName;
        }
        public String toString() {
            return this.keyName;
        }
    }

    public static enum  Comment {
        COMMENT_KEY("comment"), COMMENT_ID_KEY("comment_id"), USER_KEY("user"), CREATED_AT_KEY("created_at"), UPDATED_AT_KEY("updated_at"),
        CONTENT_KEY("content"), USER_DISPLAY_NAME_KEY("user_display_name");

        private final String keyName;
        Comment(String keyName) {
            this.keyName = keyName;
        }
        public String toString() {
            return this.keyName;
        }
    }

    public static enum  OrderTax {
        TAX_ID_KEY("tax_id"), ORDER_ID_KEY("order_id"), TAX_NAME_KEY("tax_name"), AMOUNT_KEY("amount");

        private final String keyName;
        OrderTax(String keyName) {
            this.keyName = keyName;
        }
        public String toString() {
            return this.keyName;
        }
    }


    public static enum TaxType {
        FIX_VALUE(0),PERCENT(1);
        private final int taxType;
        TaxType(int taxType){
            this.taxType = taxType;
        }
        public int getValue(){
            return taxType;
        }
    }
    public static enum ServiceType {
        FIX_VALUE(0),PERCENT(1);
        private final int serviceType;
        ServiceType(int serviceType){
            this.serviceType = serviceType;
        }
        public int getValue(){
            return serviceType;
        }
    }

    public static enum  FragmentApp {
        HOME_FRAGMENT_ID_KEY("home_fragment"), RATE_FRAGMENT_ID_KEY("rate_fragment"), PROMOTION_AMOUNT_KEY("promotion_fragment"),
        FEATURE_FRAGMENT_ID_KEY("feature_fragment"), CATEGORY_FRAGMENT_ID_KEY("categories_fragment"), FAVORITE_AMOUNT_KEY("favorite_fragment"),
        HISTORY_FRAGMENT_ID_KEY("history_fragment"), PROFILE_FRAGMENT_ID_KEY("profile_fragment");

        private final String keyName;
        FragmentApp(String keyName) {
            this.keyName = keyName;
        }
        public String toString() {
            return this.keyName;
        }
    }

    public static enum  AppConfig {
        SERVICE_END_POINT_KEY("service_end_point"), PHOTO_URLS_KEY("photo_urls");

        private final String keyName;
        AppConfig(String keyName) {
            this.keyName = keyName;
        }
        public String toString() {
            return this.keyName;
        }
    }
}
