package com.ballard.constants;

/**
 * Created by ballard on 3/13/2016.
 */
public class ResquestConstant {
    public static final int JSON_DATA = 1;
    public static final int PAIR_VALUE_DATA = 2;
    public static final String POST = "POST";
    public static final String PUT = "PUT";
    public static final String DELETE = "DELETE";
    public static final String GET = "GET";
}
