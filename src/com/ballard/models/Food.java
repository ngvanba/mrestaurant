package com.ballard.models;

import android.graphics.Bitmap;

import java.util.List;

/**
 * Created by ballard on 1/3/2016.
 */
public class Food {
    private int foodId;
    private String urlImage;
    private String foodName;
    private String description;
    private Bitmap iconBitmap;
    private String requestNote;
    private boolean showHeader = false;
    private List<PriceQuantity> priceQuantities;
    //private Bitmap headerItemImage;
    private int headerType = -1;
    private String headerItemTitle;
    private double rated;
    private int liked;
    private int commentCount;
    private boolean markFavoriteItem = false;


    public Food(String urlImage, int foodId, String title, String description, List<PriceQuantity> priceQuantities, int orderNumber) {
        this.urlImage = urlImage;
        this.foodId = foodId;
        this.foodName = title;
        this.description = description;
        this.priceQuantities = priceQuantities;
    }

    public int getFoodId() {
        return foodId;
    }

    public void setFoodId(int foodId) {
        this.foodId = foodId;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<PriceQuantity> getPriceQuantities() {
        return priceQuantities;
    }

    public void setPriceQuantities(List<PriceQuantity> priceQuantities) {
        this.priceQuantities = priceQuantities;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public Bitmap getIconBitmap() {
        return iconBitmap;
    }

    public void setIconBitmap(Bitmap iconBitmap) {
        this.iconBitmap = iconBitmap;
    }

    public String getRequestNote() {
        return requestNote;
    }

    public void setRequestNote(String requestNote) {
        this.requestNote = requestNote;
    }

    public boolean isShowHeader() {
        return showHeader;
    }

    public void setShowHeader(boolean showHeader) {
        this.showHeader = showHeader;
    }

    public int getHeaderType() {
        return headerType;
    }

    public void setHeaderType(int headerType) {
        this.headerType = headerType;
    }

    /*public Bitmap getHeaderItemImage() {
        return headerItemImage;
    }

    public void setHeaderItemImage(Bitmap headerItemImage) {
        this.headerItemImage = headerItemImage;
    }*/

    public String getHeaderItemTitle() {
        return headerItemTitle;
    }

    public void setHeaderItemTitle(String headerItemTitle) {
        this.headerItemTitle = headerItemTitle;
    }

    public double getRated() {
        return rated;
    }

    public void setRated(double rated) {
        this.rated = rated;
    }

    public int getLiked() {
        return liked;
    }

    public void setLiked(int liked) {
        this.liked = liked;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public boolean isMarkFavoriteItem() {
        return markFavoriteItem;
    }

    public void setMarkFavoriteItem(boolean markFavoriteItem) {
        this.markFavoriteItem = markFavoriteItem;
    }
}
