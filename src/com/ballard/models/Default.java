package com.ballard.models;

/**
 * Created by ballard on 3/21/2016.
 */
public class Default {
    private int defaultId;
    private int restaurantId;
    private String allowPushMenu;
    String allowWifiMenu;
    String allowCodeMenu;
    String menuAccessCode;
    String currencyCode;
    String wifiName;

    public Default(int defaultId, int restaurantId, String allowPushMenu, String allowWifiMenu, String allowCodeMenu, String menuAccessCode, String currencyCode, String wifiName) {
        this.defaultId = defaultId;
        this.restaurantId = restaurantId;
        this.allowPushMenu = allowPushMenu;
        this.allowWifiMenu = allowWifiMenu;
        this.allowCodeMenu = allowCodeMenu;
        this.menuAccessCode = menuAccessCode;
        this.currencyCode = currencyCode;
        this.wifiName = wifiName;
    }

    public int getDefaultId() {
        return defaultId;
    }

    public void setDefaultId(int defaultId) {
        this.defaultId = defaultId;
    }

    public int getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(int restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getAllowPushMenu() {
        return allowPushMenu;
    }

    public void setAllowPushMenu(String allowPushMenu) {
        this.allowPushMenu = allowPushMenu;
    }

    public String getAllowWifiMenu() {
        return allowWifiMenu;
    }

    public void setAllowWifiMenu(String allowWifiMenu) {
        this.allowWifiMenu = allowWifiMenu;
    }

    public String getAllowCodeMenu() {
        return allowCodeMenu;
    }

    public void setAllowCodeMenu(String allowCodeMenu) {
        this.allowCodeMenu = allowCodeMenu;
    }

    public String getMenuAccessCode() {
        return menuAccessCode;
    }

    public void setMenuAccessCode(String menuAccessCode) {
        this.menuAccessCode = menuAccessCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getWifiName() {
        return wifiName;
    }

    public void setWifiName(String wifiName) {
        this.wifiName = wifiName;
    }
}
