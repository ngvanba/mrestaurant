package com.ballard.models;

import java.util.List;

/**
 * Created by ballard on 3/26/2016.
 */
public class Order {
    private int orderId;
    private int restaurantId;
    private int userId;
    private long timeOrder;
    private  int table;
    private int status;
    private double amount;
    private double serviceAmount;
    private double taxAmount;
    private double totalAmount;

    public Order(){

    }

    public Order(int orderId, int restaurantId, int userId, long timeOrder, int table, int status, double amount, double serviceAmount, double taxAmount, double totalAmount, List<Food> foodOrderList) {
        this.orderId = orderId;
        this.restaurantId = restaurantId;
        this.userId = userId;
        this.timeOrder = timeOrder;
        this.table = table;
        this.status = status;
        this.amount = amount;
        this.serviceAmount = serviceAmount;
        this.taxAmount = taxAmount;
        this.totalAmount = totalAmount;
        this.foodOrderList = foodOrderList;
    }

    private List<Food> foodOrderList;

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(int restaurantId) {
        this.restaurantId = restaurantId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public long getTimeOrder() {
        return timeOrder;
    }

    public void setTimeOrder(long timeOrder) {
        this.timeOrder = timeOrder;
    }

    public int getTable() {
        return table;
    }

    public void setTable(int table) {
        this.table = table;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getServiceAmount() {
        return serviceAmount;
    }

    public void setServiceAmount(double serviceAmount) {
        this.serviceAmount = serviceAmount;
    }

    public double getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(double taxAmount) {
        this.taxAmount = taxAmount;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public List<Food> getFoodOrderList() {
        return foodOrderList;
    }

    public void setFoodOrderList(List<Food> foodOrderList) {
        this.foodOrderList = foodOrderList;
    }
}
