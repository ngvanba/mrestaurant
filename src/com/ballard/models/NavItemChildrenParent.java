package com.ballard.models;

/**
 * Created by ballard on 1/1/2016.
 */
public class NavItemChildrenParent {

    private int itemId;
    private  int parentId;

    public NavItemChildrenParent(int itemId, int parentId){
        this.itemId = itemId;
        this.parentId = parentId;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

}
