package com.ballard.models;

/**
 * Created by ballard on 1/1/2016.
 */
public class Categories {
    private int categoryId;
    private String categoryName;
    private int footCount;


    public Categories(int categoryId, String categoryName, int footCount) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.footCount = footCount;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getFootCount() {
        return footCount;
    }

    public void setFootCount(int footCount) {
        this.footCount = footCount;
    }
}
