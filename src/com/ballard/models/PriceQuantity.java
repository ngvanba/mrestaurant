package com.ballard.models;

/**
 * Created by ballard on 3/15/2016.
 */
public class PriceQuantity {
    private double price;
    private String priceName;
    private String descriptionPrice = "";
    private int quantity;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getPriceName() {
        return priceName;
    }

    public void setPriceName(String priceName) {
        this.priceName = priceName;
    }

    public String getDescriptionPrice() {
        return descriptionPrice;
    }

    public void setDescriptionPrice(String descriptionPrice) {
        this.descriptionPrice = descriptionPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
