package com.ballard.models;

/**
 * Created by ballard on 3/21/2016.
 */
public class Tax {
    private int taxId;
    private String taxName;
    private int taxType;
    private double taxValue;

    public Tax(int taxId, String taxName, int taxType, double taxValue) {
        this.taxId = taxId;
        this.taxName = taxName;
        this.taxType = taxType;
        this.taxValue = taxValue;
    }

    public int getTaxId() {
        return taxId;
    }

    public void setTaxId(int taxId) {
        this.taxId = taxId;
    }

    public String getTaxName() {
        return taxName;
    }

    public void setTaxName(String taxName) {
        this.taxName = taxName;
    }

    public int getTaxType() {
        return taxType;
    }

    public void setTaxType(int taxType) {
        this.taxType = taxType;
    }

    public double getTaxValue() {
        return taxValue;
    }

    public void setTaxValue(double taxValue) {
        this.taxValue = taxValue;
    }
}
