package com.ballard.models;

import android.util.Pair;

import java.util.ArrayList;

/**
 * Created by ballard on 6/25/2016.
 */
public class AppConfig {
    private String serviceEndPoint;
    private ArrayList<KeyValue<Integer, String>> photoUrls;

    public String getServiceEndPoint() {
        return serviceEndPoint;
    }

    public void setServiceEndPoint(String serviceEndPoint) {
        this.serviceEndPoint = serviceEndPoint;
    }

    public ArrayList<KeyValue<Integer, String>> getPhotoUrls() {
        return photoUrls;
    }

    public void setPhotoUrls(ArrayList<KeyValue<Integer, String>> photoUrls) {
        this.photoUrls = photoUrls;
    }
}
