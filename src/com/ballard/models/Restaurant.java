package com.ballard.models;

import android.graphics.Bitmap;

import java.util.List;

/**
 * Created by ballard on 2/21/2016.
 */
public class Restaurant {
    private int id;
    private String name;
    private String owner;
    private int chainsId;
    private double latitude;
    private double longitude;
    private String wifiName;
    private String address1;
    private String address2;
    private String city;
    private String country;
    private String zipCode;
    private String cellphone;
    private String tags;
    private String photoUrl;
    private Bitmap photoBitmap;
    private List<Default> defaultsList;
    private List<Tax> taxList;
    private List<Service> serviceList;

    public Restaurant(){

    }
    public Restaurant(int id, String name, String owner, int chainsId, double latitude, double longitude, String wifiName, String address1, String address2, String city, String country, String zipCode, String cellphone, String tags, String photoUrl) {
        this.id = id;
        this.name = name;
        this.owner = owner;
        this.chainsId = chainsId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.wifiName = wifiName;
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.country = country;
        this.zipCode = zipCode;
        this.cellphone = cellphone;
        this.tags = tags;
        this.photoUrl = photoUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWifiName() {
        return wifiName;
    }

    public void setWifiName(String wifiName) {
        this.wifiName = wifiName;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public int getChainsId() {
        return chainsId;
    }

    public void setChainsId(int chainsId) {
        this.chainsId = chainsId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Bitmap getPhotoBitmap() {
        return photoBitmap;
    }

    public void setPhotoBitmap(Bitmap photoBitmap) {
        this.photoBitmap = photoBitmap;
    }

    public List<Default> getDefaultsList() {
        return defaultsList;
    }

    public void setDefaultsList(List<Default> defaultsList) {
        this.defaultsList = defaultsList;
    }

    public List<Tax> getTaxList() {
        return taxList;
    }

    public void setTaxList(List<Tax> taxList) {
        this.taxList = taxList;
    }

    public List<Service> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<Service> serviceList) {
        this.serviceList = serviceList;
    }
}


