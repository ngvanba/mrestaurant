package com.ballard.activities;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.ballard.constants.CommonConstant;
import com.ballard.constants.ResquestConstant;
import com.ballard.models.User;
import com.ballard.requests.RequestAsyncTask;
import com.ballard.responses.AsyncResponse;

public class RegisterActivity extends Activity implements View.OnClickListener {

	private int PICK_IMAGE_REQUEST = 1;
	private Button bntRegister, bntBackLogin;
	private EditText txtEmail,txtPassword,txtFirstName,txtLastName,txtRetypePassword;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		
		txtEmail = (EditText)findViewById(R.id.txtEmail);
		txtFirstName = (EditText)findViewById(R.id.txtFirstName);
		txtLastName = (EditText)findViewById(R.id.txtLastName);
		txtPassword = (EditText)findViewById(R.id.txtPassword);
		txtRetypePassword = (EditText)findViewById(R.id.txtRetypePassword);
		
		
		bntRegister = (Button)findViewById(R.id.bntRegister);
		bntBackLogin = (Button)findViewById(R.id.bntBackLogin);
		
		bntRegister.setOnClickListener(this);
		bntBackLogin.setOnClickListener(this);
	
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.buttonChoose:
			showFileChooser();
			break;
		case R.id.bntRegister:
			//Do something
			String email = txtEmail.getText().toString();
			String firstName = txtFirstName.getText().toString();
			String lastName = txtLastName.getText().toString();
			String password = txtPassword.getText().toString();
			String retypePassword = txtRetypePassword.getText().toString();
			
			User userRegister = new User();
			userRegister.setEmail(email);
			userRegister.setPassword(password);
			userRegister.setRetypePassword(retypePassword);
			userRegister.setFirstName(firstName);
			userRegister.setLastName(lastName);
			Register(userRegister);
			break;
		case R.id.bntBackLogin:
			lauchLoginPage();
			break;

		default:
			break;
		}		
	}
	
	private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }
	
	public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

	
	protected void lauchLoginPage(){
		Intent loginIntent = new Intent(this, LoginActivity.class);
		startActivity(loginIntent);
	}
	
	protected void Register(User user){
		HashMap<String, String> postData = new HashMap<String, String>();
		postData.put("email",user.getEmail());
		postData.put("lastname",user.getLastName());
		postData.put("firstname",user.getFirstName());
		postData.put("password",user.getPassword());
		postData.put("retyped_password", user.getRetypePassword());
		RequestAsyncTask registerRequest = new RequestAsyncTask(this, new AsyncResponse() {
			
			@Override
			public void processFinish(String output) {
				try{				
				JSONObject object = new JSONObject(output);			
				if(object.length() > 0){
					int result = object.getInt("result");
					if(result == 0){
						//process success here
						showDialogMessage(object.getString("message"));
					} else {
						//Show error message
						showDialogMessage(object.getString("message"));
					}
					
				}
			}catch(Exception ex){
				showDialogMessage("There is no internet connection!");
			}	
				
			}
		}, postData, ResquestConstant.JSON_DATA);
		registerRequest.execute(CommonConstant.SERVER_ADDRESS + CommonConstant.REGISTER_PAGE);
	}
	
	protected void showDialogMessage(String message){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message);
		builder.setPositiveButton("OK", null);
		builder.show();
	}
	

}
