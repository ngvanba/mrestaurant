package com.ballard.activities;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;

import org.json.JSONObject;

import com.ballard.constants.CommonConstant;
import com.ballard.constants.ResquestConstant;
import com.ballard.localstore.UserLocalStore;
import com.ballard.models.User;
import com.ballard.requests.RequestAsyncTask;
import com.ballard.responses.AsyncResponse;
import com.ballard.utilities.DateDialog;
import com.ballard.utilities.Utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class UpdateProfileActivity extends Activity implements
		View.OnClickListener {

	private int PICK_IMAGE_REQUEST = 1;
	private Button bntUpdateProfile, bntCancelUpdateProfile, buttonChoose;
	private EditText txtFirstName, txtLastName, txtPhone, txtDateBirthday,
			txtCity, txtCountry, txtAddress1, txtAddress2;
	private Uri filePath;
	private ImageView imageView;
	private Bitmap bitmap;
	private UserLocalStore userLocalStore;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_update_profile);

		txtPhone = (EditText) findViewById(R.id.txtPhone);
		txtFirstName = (EditText) findViewById(R.id.txtFirstName);
		txtLastName = (EditText) findViewById(R.id.txtLastName);
		txtDateBirthday = (EditText) findViewById(R.id.txtDateBirthday);

		txtCity = (EditText) findViewById(R.id.txtCity);
		txtCountry = (EditText) findViewById(R.id.txtCountry);

		txtAddress1 = (EditText) findViewById(R.id.txtAddress1);
		txtAddress2 = (EditText) findViewById(R.id.txtAddress2);
		buttonChoose = (Button) findViewById(R.id.buttonChoose);
		imageView = (ImageView) findViewById(R.id.imageView);
		bntUpdateProfile = (Button) findViewById(R.id.bntUpdateProfile);
		bntCancelUpdateProfile = (Button) findViewById(R.id.bntCancelUpdateProfile);
		userLocalStore = new UserLocalStore(this);

		bntUpdateProfile.setOnClickListener(this);
		bntCancelUpdateProfile.setOnClickListener(this);
		buttonChoose.setOnClickListener(this);

		// Call Calendar Dialog
		txtDateBirthday
				.setOnFocusChangeListener(new View.OnFocusChangeListener() {

					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						if (hasFocus) {
							DateDialog dateDialog = new DateDialog(v);
							FragmentTransaction ft = getFragmentManager()
									.beginTransaction();
							dateDialog.show(ft, "DatePicker");
						}

					}
				});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.buttonChoose:
			showFileChooser();
			break;
		case R.id.bntUpdateProfile:
			// Do something
			User userStore = userLocalStore.getLoggedInUser();
			String firstName = txtFirstName.getText().toString();
			String lastName = txtLastName.getText().toString();
			String phone = txtPhone.getText().toString();
			Date birthday = Utility.getDate(txtDateBirthday.getText()
					.toString());
			String city = txtCity.getText().toString();
			String country = txtCountry.getText().toString();
			String address1 = txtAddress1.getText().toString();
			String address2 = txtAddress2.getText().toString();
			File imageFile = new File(getRealPathFromURI(filePath));
			Date currentDate = new Date();
			String photoName = imageFile.getName().concat("_")
					.concat(String.valueOf(currentDate.getTime()));
			String photoContent = getStringImage(bitmap);
			User userProfile = new User();
			userProfile.setUserId(userStore.getUserId());
			userProfile.setEmail(userStore.getEmail());
			userProfile.setFirstName(firstName);
			userProfile.setLastName(lastName);
			userProfile.setPhone(phone);
			userProfile.setPhoto(photoName);
			userProfile.setPhotoContent(photoContent);
			userProfile.setBirthday(birthday);
			userProfile.setCity(city);
			userProfile.setCountry(country);
			userProfile.setAddress1(address1);
			userProfile.setAddress2(address2);
			UpdateProfile(userProfile);
			break;
		case R.id.bntCancelUpdateProfile:
			// lauchLoginPage();
			break;

		default:
			break;
		}
	}

	private void showFileChooser() {
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(Intent.createChooser(intent, "Select Picture"),
				PICK_IMAGE_REQUEST);
	}

	public String getStringImage(Bitmap bmp) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		byte[] imageBytes = baos.toByteArray();
		String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
		return encodedImage;
	}

	/**
	 * Call file name based on: File imageFile = new
	 * File(getRealPathFromURI(selectedImageURI));
	 * 
	 * @param contentURI
	 * @return
	 */
	private String getRealPathFromURI(Uri contentURI) {
		String result;
		Cursor cursor = getContentResolver().query(contentURI, null, null,
				null, null);
		if (cursor == null) { // Source is Dropbox or other similar local file
								// path
			result = contentURI.getPath();
		} else {
			cursor.moveToFirst();
			int idx = cursor
					.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
			result = cursor.getString(idx);
			cursor.close();
		}
		return result;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
				&& data != null && data.getData() != null) {

			filePath = data.getData();
			try {
				bitmap = MediaStore.Images.Media.getBitmap(
						getContentResolver(), filePath);
				imageView.setImageBitmap(bitmap);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	protected void UpdateProfile(User user) {
		HashMap<String, String> postData = new HashMap<String, String>();
		postData.put("id", String.valueOf(user.getUserId()));
		postData.put("email", user.getEmail());
		postData.put("lastname", user.getLastName());
		postData.put("firstname", user.getFirstName());
		postData.put("photo", user.getPhoto());
		postData.put("photocontent", user.getPhotoContent());
		postData.put("birthday", String.valueOf(user.getBirthday().getTime()));
		postData.put("city", user.getCity());
		postData.put("country", user.getCountry());
		postData.put("address1", user.getAddress1());
		postData.put("address2", user.getAddress2());
		postData.put("isowner", "0");
		RequestAsyncTask registerRequest = new RequestAsyncTask(this,
				new AsyncResponse() {

					@Override
					public void processFinish(String output) {
						try {
							JSONObject object = new JSONObject(output);
							if (object.length() > 0) {
								showDialogMessage(object.getString("message"));
								// responseStatus.setStatus(object.getInt("updateStatus"));
								// responseStatus.setMessage(object.getString("message"));
							}
						} catch (Exception ex) {
							showDialogMessage("Update Error. Please check your data again!");
							// ex.printStackTrace();
						}

					}
				}, postData, ResquestConstant.JSON_DATA);
		registerRequest.execute(CommonConstant.SERVER_ADDRESS
				+ "/Account/UpdateProfile");
	}

	protected void showDialogMessage(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message);
		builder.setPositiveButton("OK", null);
		builder.show();
	}

	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// // Inflate the menu; this adds items to the action bar if it is present.
	// getMenuInflater().inflate(R.menu.update_profile, menu);
	// return true;
	// }
	//
	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// // Handle action bar item clicks here. The action bar will
	// // automatically handle clicks on the Home/Up button, so long
	// // as you specify a parent activity in AndroidManifest.xml.
	// int id = item.getItemId();
	// if (id == R.id.action_settings) {
	// return true;
	// }
	// return super.onOptionsItemSelected(item);
	// }
}
