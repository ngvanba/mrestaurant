package com.ballard.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ballard.constants.AppConstant;
import com.ballard.constants.CommonConstant;
import com.ballard.constants.ResquestConstant;
import com.ballard.database.DatabaseHelper;
import com.ballard.fragments.SelectRestaurantFragment;
import com.ballard.interfaces.IApp;
import com.ballard.interfaces.IFood;
import com.ballard.localstore.AppStore;
import com.ballard.localstore.UserLocalStore;
import com.ballard.models.AppConfig;
import com.ballard.models.Comment;
import com.ballard.models.CommonResult;
import com.ballard.models.KeyValue;
import com.ballard.models.Restaurant;
import com.ballard.models.User;
import com.ballard.requests.RequestAsyncTask;
import com.ballard.responses.AsyncResponse;
import com.ballard.utilities.Utility;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class LoginActivity extends Activity implements
        View.OnClickListener {
    private Button bntLogin;
    private EditText txtEmail, txtPassword;
    private UserLocalStore userLocalStore;
    private TextView txtLinkSignUp;
    private LoginButton loginFacebook;
    private CallbackManager callbackManager;
    private Intent mainMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            FacebookSdk.sdkInitialize(getApplicationContext());
            callbackManager = CallbackManager.Factory.create();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        setContentView(R.layout.activity_login);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        bntLogin = (Button) findViewById(R.id.bntLogin);
        txtLinkSignUp = (TextView) findViewById(R.id.txtLinkSignUp);
        bntLogin.setOnClickListener(this);
        txtLinkSignUp.setOnClickListener(this);
        userLocalStore = new UserLocalStore(this);
        mainMenu = new Intent(this, GuestMenuActivity.class);
        try {
            loginFacebook = (LoginButton) findViewById(R.id.loginFacebook);
            loginFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {

                    final String userId = loginResult.getAccessToken().getUserId();
                    final String tokenId = loginResult.getAccessToken().getToken();

                    //New code
                    final User returnUser = new User();
                    GraphRequestAsyncTask request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject user, GraphResponse graphResponse) {
                            returnUser.setEmail(user.optString("email"));
                            returnUser.setFirstName(user.optString("first_name"));
                            returnUser.setLastName(user.optString("last_name"));
                            returnUser.setTokenId(tokenId);
                            //showDialogMessage(userId + ":" + tokenId);
                            logUserIn(returnUser);
                        }
                    }).executeAsync();

                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onError(FacebookException e) {

                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        if (authenticate()) {
            //Get app setting
            getSettingApp();
            showRestaurant();
        }
    }

    private boolean authenticate() {
        return userLocalStore.getUserLoggedIn();
    }

    private void showRestaurant() {
        //Check Wifi Name
        String wifiName = Utility.getCurrentWifiName(getApplicationContext());

        //Get store restaurant base on WIFI name
        DatabaseHelper helper = new DatabaseHelper(getApplicationContext());
        SQLiteDatabase db = helper.getReadableDatabase();
        String[] tableColumns = new String[]{
                AppConstant.Restaurant.RESTAURANT_ID_KEY.toString(),
                AppConstant.Restaurant.RESTAURANT_NAME_KEY.toString(),
                AppConstant.Restaurant.RESTAURANT_OWNER_KEY.toString(),
                AppConstant.Restaurant.RESTAURANT_CHAIN_ID_KEY.toString(),
                AppConstant.Restaurant.RESTAURANT_WIFI_KEY.toString(),
                AppConstant.Restaurant.RESTAURANT_LAT_KEY.toString(),
                AppConstant.Restaurant.RESTAURANT_LNG_KEY.toString(),
                AppConstant.Restaurant.RESTAURANT_ADDRESS1_KEY.toString(),
                AppConstant.Restaurant.RESTAURANT_ADDRESS2_KEY.toString(),
                AppConstant.Restaurant.RESTAURANT_CITY_KEY.toString(),
                AppConstant.Restaurant.RESTAURANT_COUNTRY_KEY.toString(),
                AppConstant.Restaurant.RESTAURANT_ZIP_KEY.toString(),
                AppConstant.Restaurant.RESTAURANT_PHONE_KEY.toString(),
                AppConstant.Restaurant.RESTAURANT_TAGS_KEY.toString(),
                AppConstant.Restaurant.RESTAURANT_PHOTOS_KEY.toString()
        };
        String whereClause = AppConstant.Restaurant.RESTAURANT_WIFI_KEY.toString() + " = '" + wifiName + "'";
        Cursor cursor = db.query(AppConstant.Restaurant.RESTAURANT_TABLE_NAME.toString(), tableColumns, whereClause, null, null, null, null, null);

        //Cursor cursor = db.rawQuery("select * from todo where "+ TableConstant.COLUMN_RES_WIFI +" = '?'", new String[]{wifiName});
        List<Restaurant> listRestaurant = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Restaurant restaurant = findDBRestaurant(cursor);
            listRestaurant.add(restaurant);
            cursor.moveToNext();
        }
        db.close();
        if (listRestaurant.size() > 0) {
            userLocalStore.saveRestaurantSelect(listRestaurant.get(0));
            startActivity(new Intent(this, GuestMenuActivity.class));
        } else {
            SelectRestaurantFragment selectRestaurantDialog = new SelectRestaurantFragment();
            FragmentManager fragmentManager = getFragmentManager();
            selectRestaurantDialog.show(fragmentManager, "");
        }
    }

    private Restaurant findDBRestaurant(Cursor cursor) {
        Restaurant restaurant = new Restaurant();
        restaurant.setId(cursor.getInt(0));
        restaurant.setName(cursor.getString(1));
        restaurant.setOwner(cursor.getString(2));
        restaurant.setChainsId(cursor.getInt(3));
        restaurant.setWifiName(cursor.getString(4));
        restaurant.setLatitude(cursor.getDouble(5));
        restaurant.setLongitude(cursor.getDouble(6));
        restaurant.setAddress1(cursor.getString(7));
        restaurant.setAddress2(cursor.getString(8));
        restaurant.setCity(cursor.getString(9));
        restaurant.setCountry(cursor.getString(10));
        restaurant.setZipCode(cursor.getString(11));
        restaurant.setCellphone(cursor.getString(12));
        restaurant.setTags(cursor.getString(13));
        restaurant.setPhotoUrl(cursor.getString(14));
        return restaurant;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bntLogin:
                User user = new User();
                user.setEmail(txtEmail.getText().toString());
                user.setPassword(txtPassword.getText().toString());
                if (user.getEmail().trim().length() == 0 && user.getPassword().trim().length() == 0) {
                    showDialogMessage(getString(R.string.invalid_account));
                } else if (user.getEmail().trim().length() == 0) {
                    showDialogMessage(getString(R.string.email_require));
                } else if (user.getPassword().trim().length() == 0) {
                    showDialogMessage(getString(R.string.password_require));
                } else {
                    //Validation email
                    //showDialogMessage(getString(R.string.invalid_email));
                    authenticate(user);
                }


                break;
            case R.id.txtLinkSignUp:
                launchRegisterPage();
                break;
            default:
                break;
        }
    }

    protected void launchRegisterPage() {
        Intent registerIntent = new Intent(this, RegisterActivity.class);
        startActivity(registerIntent);
    }

    private JsonObject createJsonObjectUser(User user) {
        JsonObject postData = new JsonObject();
        postData.addProperty(AppConstant.User.EMAIL_KEY.toString(), user.getEmail());
        postData.addProperty(AppConstant.User.PASSWORD_KEY.toString(), user.getPassword());
        return postData;
    }

    protected void authenticate(User user){
        JsonObject postData = createJsonObjectUser(user);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(CommonConstant.SERVER_ADDRESS_RETROFIT).client(client)
                .build();

        Call<JsonObject> call = null;
        IApp service = retrofit.create(IApp.class);
        call = service.login(postData);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject jsonResponse = response.body();
                try {
                    JSONObject objects = (JSONObject) new JSONTokener(jsonResponse.toString()).nextValue();
                    JSONArray resultData = objects.getJSONArray(AppConstant.Common.DATA_KEY.toString());
                    String message = objects.getString(AppConstant.Common.MESSAGE_KEY.toString());
                    int result = objects.getInt(AppConstant.Common.RESULT_KEY.toString());
                    String tokenId = objects.getString(AppConstant.Common.TOKEN_ID_KEY.toString());

                    if (result == 0) {
                        JSONObject object = resultData.getJSONObject(0);
                        int userId = object.getInt(AppConstant.User.USER_ID_KEY.toString());
                        String email = object.getString(AppConstant.User.EMAIL_KEY.toString());
                        String lastName = object.getString(AppConstant.User.LAST_NAME_KEY.toString());
                        String firstName = object.getString(AppConstant.User.FIRST_NAME_KEY.toString());
                        String password = object.getString(AppConstant.User.PWD_KEY.toString());
                        User returnUser = new User();
                        returnUser.setUserId(userId);
                        returnUser.setEmail(email);
                        returnUser.setFirstName(firstName);
                        returnUser.setLastName(lastName);
                        returnUser.setPassword(password);
                        returnUser.setTokenId(tokenId);
                        returnUser.setIsMultipleOrder(false);
                        logUserIn(returnUser);
                        getSettingApp();
                    } else {
                        showDialogMessage(message);
                    }

                } catch (Exception ex) {
                    //showDialogMessage(getString(R.string.invalid_account));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    protected void getSettingApp() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(CommonConstant.SERVER_ADDRESS_RETROFIT).client(client)
                .build();
        IApp app = retrofit.create(IApp.class);
        Call<JsonObject> call = app.getSettingApp(CommonConstant.APP_KEY, CommonConstant.COUNTRY_CODE_KEY);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject jsonResponse = response.body();
                try {
                    JSONObject objects = (JSONObject) new JSONTokener(jsonResponse.toString()).nextValue();
                    JSONObject objectData = objects.getJSONObject(AppConstant.Common.DATA_KEY.toString());
                    String serviceEndPoint = objectData.getString(AppConstant.AppConfig.SERVICE_END_POINT_KEY.toString());
                    JSONObject photoUrlObject = objectData.getJSONObject(AppConstant.AppConfig.PHOTO_URLS_KEY.toString());
                    AppConfig appConfig = new AppConfig();
                    appConfig.setServiceEndPoint(serviceEndPoint);
                    ArrayList<KeyValue<Integer, String>> listPhoto = new ArrayList<KeyValue<Integer, String>>();
                    for(int i = 0; i<photoUrlObject.names().length(); i++){
                        String key = photoUrlObject.names().getString(i);
                        String photoUrl = photoUrlObject.getString(key);
                        KeyValue<Integer, String> photoKeyValue = new KeyValue<Integer, String>(Integer.parseInt(key),photoUrl);
                        listPhoto.add(photoKeyValue);
                    }
                    appConfig.setPhotoUrls(listPhoto);
                    AppStore.GetInstance().setAppConfig(appConfig);
                } catch (JSONException ex) {
                    Toast.makeText(getApplicationContext(), getString(R.string.no_data_message), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }


    protected void showDialogMessage(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(msg);
        builder.setPositiveButton(getString(R.string.ok_button), null);
        builder.show();
    }

    protected void logUserIn(User returnUser) {
        userLocalStore.storeUserData(returnUser);
        userLocalStore.setUserLoggedIn(true);
        showRestaurant();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
