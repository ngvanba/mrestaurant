package com.ballard.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.ballard.adapter.NavDrawerListAdapter;
import com.ballard.businesses.MyFoodOrder;
import com.ballard.businesses.SearchData;
import com.ballard.constants.AppConstant;
import com.ballard.constants.CommonConstant;
import com.ballard.constants.ResquestConstant;
import com.ballard.fragments.CategoriesFragment;
import com.ballard.fragments.FavoriteFragment;
import com.ballard.fragments.FeatureFragment;
import com.ballard.fragments.FoodSearchResultFragment;
import com.ballard.fragments.HistoryFragment;
import com.ballard.fragments.HomeFragment;
import com.ballard.fragments.OrderFoodFragment;
import com.ballard.fragments.ProfileFragment;
import com.ballard.fragments.PromotionFragment;
import com.ballard.fragments.RatedFragment;
import com.ballard.fragments.ViewOrderFragment;
import com.ballard.interfaces.IFood;
import com.ballard.localstore.AppStore;
import com.ballard.localstore.UserLocalStore;
import com.ballard.models.Categories;
import com.ballard.models.Food;
import com.ballard.models.NavDrawerItem;
import com.ballard.models.NavItemChildrenParent;
import com.ballard.requests.RequestAsyncTask;
import com.ballard.responses.AsyncResponse;
import com.ballard.utilities.MemoryCache;
import com.ballard.utilities.RestaurantCache;
import com.ballard.utilities.StringUtil;
import com.ballard.utilities.Utility;
import com.facebook.login.LoginManager;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.view.View.OnClickListener;

public class GuestMenuActivity extends Activity {
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	// nav drawer title
	private CharSequence mDrawerTitle;

	// used to store app title
	private CharSequence mTitle;

	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;

	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;
	//
	private UserLocalStore userLocalStore;
	private Button bntLogout;
	private TextView txtHomeLabel;
	private ArrayList<NavItemChildrenParent> navItemChildrenParentList;
	private ArrayList<Categories> listCategories;
	private int currentSelectedIndex = -1;
	private int currentSelectedcategoryIdIndex = -1;
	EditText textView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_guest_menu);
		userLocalStore = new UserLocalStore(this);

		//Build Slide Menu
		mTitle = mDrawerTitle = getTitle();

		// load slide menu items
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

		// nav drawer icons from resources
		navMenuIcons = getResources()
				.obtainTypedArray(R.array.nav_drawer_icons);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

		buildMenu();

		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		//Set listener button
		Button menuCurrentOrder = (Button)findViewById(R.id.menuCurrentOrder);
		menuCurrentOrder.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(MyFoodOrder.getInstance().getSuccessOrder() != null){
					ViewOrderFragment viewOrderFragment = new ViewOrderFragment();
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					ft.replace(R.id.frame_container, viewOrderFragment);
					ft.addToBackStack(ViewOrderFragment.class.getName());
					ft.commit();
				} else {
					AlertDialog alert = new AlertDialog.Builder(v.getContext()).create();
					alert.setTitle(getString(R.string.warning_message));
					alert.setMessage(getString(R.string.empty_current_order_message));
					alert.setButton(DialogInterface.BUTTON_POSITIVE,
							getString(R.string.ok_button), new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialogInterface, int i) {
								}
							});
					alert.show();
				}

			}
		});
		Button menuYourCart = (Button)findViewById(R.id.menuYourCart);
		long quantities = (long)MyFoodOrder.getInstance().getTotalQuantities();
		String strQuantities = "\t\t"+String.valueOf(quantities);
		menuYourCart.setText(strQuantities);
		MyFoodOrder.getInstance().setNotifyCart(menuYourCart);
		menuYourCart.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(MyFoodOrder.getInstance().getTotalQuantities() > 0){
					Fragment fragment = new OrderFoodFragment();
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					ft.replace(R.id.frame_container, fragment);
					ft.addToBackStack(OrderFoodFragment.class.getName());
					ft.commit();
				} else {
					AlertDialog alert = new AlertDialog.Builder(v.getContext()).create();
					alert.setTitle(getString(R.string.warning_message));
					alert.setMessage(getString(R.string.empty_order_message));
					alert.setButton(DialogInterface.BUTTON_POSITIVE,
							getString(R.string.ok_button), new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialogInterface, int i) {
								}
							});
					alert.show();
					//Toast.makeText(getApplicationContext(), getString(R.string.empty_order_message), Toast.LENGTH_LONG).show();
				}
			}
		});

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
//				R.drawable.ic_drawer, //nav menu toggle icon
				R.string.app_name, // nav drawer open - description for accessibility
				R.string.app_name // nav drawer close - description for accessibility
		) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		ActionBar actionBar = this.getActionBar(); // you can use ABS or the non-bc ActionBar
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowCustomEnabled(true);

		ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

		LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflator.inflate(R.layout.actionbar_search, null);
		actionBar.setCustomView(v, params);
		textView =  (EditText) v.findViewById(R.id.search_box);
		ImageButton bnGo = (ImageButton) v.findViewById(R.id.bntSearch);
		bnGo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.e("Search text:",textView.getText().toString());
				SearchData.getInstance().setKeySearch(textView.getText().toString());
				FoodSearchResultFragment foodSearchResultFragment = new FoodSearchResultFragment();
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				ft.replace(R.id.frame_container, foodSearchResultFragment);
				ft.commit();
			}
		});

		/*ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_dropdown_item_1line, COUNTRIES);*/

		//textView.setAdapter(adapter);

		/*textView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// do something when the user clicks
				String searchString=(String)parent.getItemAtPosition(position);
				textView.setText(""+searchString);
				Toast.makeText(GuestMenuActivity.this, "you clicked "+searchString, Toast.LENGTH_LONG).show();
			}
		});*/

	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 */
	public void displayView(int position) {
		boolean isConnected = Utility.isInternetOn(getApplicationContext());
		if(isConnected){
			NavItemChildrenParent navItem = navItemChildrenParentList.get(position);
			MemoryCache.GetInstance().setCurrentMenuSlide(position);
			MemoryCache.GetInstance().setGuestMenuActivity(this);
			// update the main content by replacing fragments
			Fragment fragment = null;
			Object fragmentObject = null;
			switch (navItem.getParentId()) {
				case 0:
					if(currentSelectedIndex != position){
						fragment = new HomeFragment();
						currentSelectedIndex = position;
						currentSelectedcategoryIdIndex = -1;
					}
					break;
				case 1:
					if(currentSelectedIndex != position){
						fragment = new RatedFragment();
						currentSelectedIndex = position;
						currentSelectedcategoryIdIndex = -1;
					}
					break;
				case 2:
					if(currentSelectedIndex != position){
						fragment = new PromotionFragment();
						currentSelectedIndex = position;
						currentSelectedcategoryIdIndex = -1;
					}
					break;
				case 3:
					if(currentSelectedIndex != position){
						fragment = new FeatureFragment();
						currentSelectedIndex = position;
						currentSelectedcategoryIdIndex = -1;
					}

					break;
				case 4:
					int index = position - 4;
					int categoryId = -1000;
					if(index > 0){
						categoryId = listCategories.get(index - 1).getCategoryId();
					}

					if(currentSelectedIndex != position){
						fragment = new CategoriesFragment();
						currentSelectedIndex = position;
						if(currentSelectedcategoryIdIndex != categoryId){
							Bundle bundle = new Bundle(2);
							bundle.putInt(CommonConstant.CATEGORY_ID, categoryId);
							fragment.setArguments(bundle);
							currentSelectedcategoryIdIndex = categoryId;
						}
					}


					break;
				case 5:
					if(currentSelectedIndex != position){
						fragment = new FavoriteFragment();
						currentSelectedIndex = position;
						currentSelectedcategoryIdIndex = -1;
					}
					break;
				case 6:
					if(currentSelectedIndex != position){
						fragment = new HistoryFragment();
						currentSelectedIndex = position;
						currentSelectedcategoryIdIndex = -1;
					}
					break;
				case 7:
					if(currentSelectedIndex != position){
						fragment = new ProfileFragment();
						currentSelectedIndex = position;
						currentSelectedcategoryIdIndex = -1;
					}
					break;

				default:
					break;
			}

			if (fragment != null) {
				clearBackStack();
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				ft.replace(R.id.frame_container, fragment);
				ft.addToBackStack(CommonConstant.TOP_MENU_SCREEN);
				ft.commit();

				// update selected item and title, then close the drawer
				mDrawerList.setItemChecked(position, true);
				mDrawerList.setSelection(position);
				//setTitle(navMenuTitles[position]);
				setTitle(navDrawerItems.get(position).getTitle());
				mDrawerLayout.closeDrawer(mDrawerList);
			} else {
				// update selected item and title, then close the drawer
				mDrawerList.setItemChecked(position, true);
				mDrawerList.setSelection(position);
				//setTitle(navMenuTitles[position]);
				setTitle(navDrawerItems.get(position).getTitle());
				mDrawerLayout.closeDrawer(mDrawerList);
				// error in creating fragment
				Log.e("MainActivity", "Error in creating fragment");
			}
		} else {
			AlertDialog alert = new AlertDialog.Builder(this).create();
			alert.setTitle(getString(R.string.warning_title));
			alert.setMessage(getString(R.string.no_internet_connection));
			/*alert.setButton(DialogInterface.BUTTON_POSITIVE,
					"OK", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {

						}
					});*/
			alert.show();
		}
	}

	private void clearBackStack() {
		while (getFragmentManager().getBackStackEntryCount() != 0) {
			getFragmentManager().popBackStackImmediate();
		}
	}

	public int getMaxMenuSize(){
		return navItemChildrenParentList.size();
	}

	@Override
	protected void onStart(){
		super.onStart();
	}

	private boolean authenticate(){
		return userLocalStore.getUserLoggedIn();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.guest_menu, menu);

		// Get the SearchView and set the searchable configuration
		/*SearchManager searchManager =  (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) menu.findItem(R.id.act).getActionView();
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));*/

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		// toggle nav drawer on selecting action bar app icon/title
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		switch (item.getItemId()) {
			/*case R.id.menu_search:
				onSearchRequested();
				return true;*/
			case R.id.action_settings:
				return true;
			case R.id.action_logout:
				userLocalStore.clearUserData();
				try {
					LoginManager.getInstance().logOut();
				}catch (Exception ex){
					Log.d("Error:",ex.getMessage());
				}
				startActivity(new Intent(GuestMenuActivity.this, LoginActivity.class));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	/* *
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	//Get Categories
	protected ArrayList<Categories> buildMenu() {
		listCategories = new ArrayList<Categories>();
		HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
		interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
		OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

		Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
				.baseUrl(CommonConstant.SERVER_ADDRESS_RETROFIT).client(client)
				.build();

		String token = userLocalStore.getLoggedInUser().getTokenId();
		IFood service = retrofit.create(IFood.class);
		Call<JsonObject> call = service.getCategories(RestaurantCache.GetInstance().get().getId(), token);
		call.enqueue(new Callback<JsonObject>() {
			@Override
			public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

				int statusCode = response.code();
				try {
					JsonObject jsonResponse = response.body();
					JSONObject objects = (JSONObject) new JSONTokener(jsonResponse.toString()).nextValue();
					JSONObject objectData = objects.getJSONObject(AppConstant.Common.DATA_KEY.toString());
					JSONArray results = objectData.getJSONArray(AppConstant.CategoriesFood.CATEGORIES_KEY.toString());
					//String message = objects.getString("message");
					for(int i=0;i<results.length();i++){
						JSONObject object = results.getJSONObject(i);
						int categoryId = object.getInt(AppConstant.CategoriesFood.CATEGORY_ID_KEY.toString());
						String categoryName = object.getString(AppConstant.CategoriesFood.CATEGORY_NAME_KEY.toString());
						int footCount = object.getInt(AppConstant.CategoriesFood.FOOD_COUNT_KEY.toString());
						if(footCount > 0){
							listCategories.add(new Categories(categoryId, categoryName, footCount));
						}
					}

				} catch (JSONException ex) {
				} catch (NullPointerException ex) {
				}
				//Create a arraylist to contain to draw.
				navDrawerItems = new ArrayList<NavDrawerItem>();
				//a list of NavItemChildrenParent to keep relationship between parent - children
				navItemChildrenParentList = new ArrayList<NavItemChildrenParent>();

				// adding nav drawer items to array
				// Home -> 0
				navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
				navItemChildrenParentList.add(new NavItemChildrenParent(navDrawerItems.size() - 1, 0));
				// Rated -> 1
				navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
				navItemChildrenParentList.add(new NavItemChildrenParent(navDrawerItems.size() - 1, 1));

				// Promotion -> 2
				navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
				navItemChildrenParentList.add(new NavItemChildrenParent(navDrawerItems.size() - 1, 2));

				// Feature -> 3
				navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
				navItemChildrenParentList.add(new NavItemChildrenParent(navDrawerItems.size() - 1, 3));

				// Categories -> 4
				navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
				navItemChildrenParentList.add(new NavItemChildrenParent(navDrawerItems.size() - 1, 4));

				for(int i = 0; i < listCategories.size(); i++){
					Categories category = listCategories.get(i);
					navDrawerItems.add(new NavDrawerItem(category.getCategoryName(), R.drawable.ic_blank, true, String.valueOf(category.getFootCount())));
					navItemChildrenParentList.add(new NavItemChildrenParent(navDrawerItems.size() - 1, 4));
				}
				// My Favorate -> 5
				navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));
				navItemChildrenParentList.add(new NavItemChildrenParent(navDrawerItems.size() - 1, 5));
				// Histories -> 6
				navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1), true, "22"));
				navItemChildrenParentList.add(new NavItemChildrenParent(navDrawerItems.size() - 1, 6));
				// My Profile -> 7
				navDrawerItems.add(new NavDrawerItem(navMenuTitles[7], navMenuIcons.getResourceId(7, -1)));
				navItemChildrenParentList.add(new NavItemChildrenParent(navDrawerItems.size() - 1, 7));
				// What's hot, We  will add a counter here -> 6
//						navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1), true, "50+"));
//						navItemChildrenParentList.add(new NavItemChildrenParent(navDrawerItems.size() - 1, 6));

				// Recycle the typed array
				navMenuIcons.recycle();

				mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

				// setting the nav drawer list adapter
				adapter = new NavDrawerListAdapter(getApplicationContext(),
						navDrawerItems);
				mDrawerList.setAdapter(adapter);

				displayView(0);
				//End Slide Menu
			}

			@Override
			public void onFailure(Call<JsonObject> call, Throwable t) {
				Log.d("Error", t.getMessage());
			}
		});
		return listCategories;
	}

	/*protected ArrayList<Categories> buildMenu() {
		listCategories = new ArrayList<Categories>();
		HashMap<String, String> postData = new HashMap<String, String>();
		RequestAsyncTask categoriesTask = new RequestAsyncTask(this,
				new AsyncResponse() {

					@Override
					public void processFinish(String output) {
						if (output.length() == 0) {
							//showDialogMessage("Your account is invalid!");
						} else {
							try {
								//output = output.replaceAll("\"", "");
								JSONObject objects = (JSONObject) new JSONTokener(output).nextValue();
								JSONObject objectData = objects.getJSONObject(AppConstant.Common.DATA_KEY.toString());
								JSONArray results = objectData.getJSONArray(AppConstant.CategoriesFood.CATEGORIES_KEY.toString());
								//String message = objects.getString("message");
								for(int i=0;i<results.length();i++){
									JSONObject object = results.getJSONObject(i);
									int categoryId = object.getInt(AppConstant.CategoriesFood.CATEGORY_ID_KEY.toString());
									String categoryName = object.getString(AppConstant.CategoriesFood.CATEGORY_NAME_KEY.toString());
									int footCount = object.getInt(AppConstant.CategoriesFood.FOOD_COUNT_KEY.toString());
									if(footCount > 0){
										listCategories.add(new Categories(categoryId, categoryName, footCount));
									}
								}
							} catch (Exception ex) {
								//showDialogMessage("Your account is invalid!");
							}
						}
						//Create a arraylist to contain to draw.
						navDrawerItems = new ArrayList<NavDrawerItem>();
						//a list of NavItemChildrenParent to keep relationship between parent - children
						navItemChildrenParentList = new ArrayList<NavItemChildrenParent>();

						// adding nav drawer items to array
						// Home -> 0
						navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
						navItemChildrenParentList.add(new NavItemChildrenParent(navDrawerItems.size() - 1, 0));
						// Rated -> 1
						navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
						navItemChildrenParentList.add(new NavItemChildrenParent(navDrawerItems.size() - 1, 1));

						// Promotion -> 2
						navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
						navItemChildrenParentList.add(new NavItemChildrenParent(navDrawerItems.size() - 1, 2));

						// Feature -> 3
						navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
						navItemChildrenParentList.add(new NavItemChildrenParent(navDrawerItems.size() - 1, 3));

						// Categories -> 4
						navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
						navItemChildrenParentList.add(new NavItemChildrenParent(navDrawerItems.size() - 1, 4));

						for(int i = 0; i < listCategories.size(); i++){
							Categories category = listCategories.get(i);
							navDrawerItems.add(new NavDrawerItem(category.getCategoryName(), R.drawable.ic_blank, true, String.valueOf(category.getFootCount())));
							navItemChildrenParentList.add(new NavItemChildrenParent(navDrawerItems.size() - 1, 4));
						}
						// My Favorate -> 5
						navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));
						navItemChildrenParentList.add(new NavItemChildrenParent(navDrawerItems.size() - 1, 5));
						// Histories -> 6
						navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1), true, "22"));
						navItemChildrenParentList.add(new NavItemChildrenParent(navDrawerItems.size() - 1, 6));
						// My Profile -> 7
						navDrawerItems.add(new NavDrawerItem(navMenuTitles[7], navMenuIcons.getResourceId(7, -1)));
						navItemChildrenParentList.add(new NavItemChildrenParent(navDrawerItems.size() - 1, 7));
						// What's hot, We  will add a counter here -> 6
//						navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1), true, "50+"));
//						navItemChildrenParentList.add(new NavItemChildrenParent(navDrawerItems.size() - 1, 6));

						// Recycle the typed array
						navMenuIcons.recycle();

						mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

						// setting the nav drawer list adapter
						adapter = new NavDrawerListAdapter(getApplicationContext(),
								navDrawerItems);
						mDrawerList.setAdapter(adapter);

						displayView(0);
						//End Slide Menu

					}
				}, postData, ResquestConstant.JSON_DATA);
		categoriesTask.setRequestMethod(ResquestConstant.GET);
		String token = userLocalStore.getLoggedInUser().getTokenId();
		String restaurant = String.valueOf(RestaurantCache.GetInstance().get().getId());
		String url = StringUtil.format("{0}/{1}/{2}/{3}", CommonConstant.SERVER_ADDRESS, AppConstant.CategoriesFood.CATEGORY_KEY.toString(), restaurant, token);
		categoriesTask.execute(url);
		return listCategories;
	}*/



	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

	/*@Override
	public void onBackPressed() {
		super.onBackPressed();
		Toast.makeText(GuestMenuActivity.this, "back back back", Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void onResume() {
		super.onResume();
		Toast.makeText(GuestMenuActivity.this, "resume", Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void onPause() {
		super.onPause();
		Toast.makeText(GuestMenuActivity.this, "pause", Toast.LENGTH_SHORT).show();
	}*/

	/**
	 * Slide menu item click listener
	 * */
	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
								long id) {
			// display view for selected nav drawer item
			displayView(position);
		}
	}

}
