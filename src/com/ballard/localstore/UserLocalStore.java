package com.ballard.localstore;

import android.content.Context;
import android.content.SharedPreferences;

import com.ballard.constants.AppConstant;
import com.ballard.constants.CommonConstant;
import com.ballard.models.Restaurant;
import com.ballard.models.User;

import java.util.Date;

public class UserLocalStore {

    SharedPreferences userLocalDatabase;

    public UserLocalStore(Context context) {
        userLocalDatabase = context.getSharedPreferences(
                CommonConstant.SP_NAME, 0);
    }

    public void storeUserData(User user) {
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.putString(AppConstant.User.EMAIL_KEY.toString(), user.getEmail());
        spEditor.putInt(AppConstant.User.USER_ID_KEY.toString(), user.getUserId());
        spEditor.putString(AppConstant.User.FIRST_NAME_KEY.toString(), user.getFirstName());
        spEditor.putString(AppConstant.User.LAST_NAME_KEY.toString(), user.getLastName());
        spEditor.putString(AppConstant.Common.TOKEN_ID_KEY.toString(), user.getTokenId());
        //spEditor.putLong(CommonConstant.BIRTHDAY_NAME_FIELD, user.getBirthday()
        //.getTime());
        spEditor.commit();
    }

    public void saveRestaurantSelect(Restaurant restaurant) {
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.remove(CommonConstant.RESTAURANT_ID);
        spEditor.remove(CommonConstant.RESTAURANT_NAME);
        spEditor.remove(CommonConstant.WIFI_NAME);

        spEditor.putInt(CommonConstant.RESTAURANT_ID, restaurant.getId());
        spEditor.putString(CommonConstant.RESTAURANT_NAME, restaurant.getName());
        spEditor.putString(CommonConstant.WIFI_NAME, restaurant.getWifiName());
        spEditor.commit();
    }

    public Restaurant getRestaurantSelect() {
        int restaurantId = userLocalDatabase.getInt(CommonConstant.RESTAURANT_ID, -1);
        String restaurantName = userLocalDatabase.getString(CommonConstant.RESTAURANT_NAME, "");
        String wifiName = userLocalDatabase.getString(CommonConstant.WIFI_NAME, "");
        Restaurant restaurant = new Restaurant();
        restaurant.setId(restaurantId);
        restaurant.setName(restaurantName);
        restaurant.setWifiName(wifiName);
        return restaurant;
    }

    public User getLoggedInUser() {
        String email = userLocalDatabase.getString(
                AppConstant.User.EMAIL_KEY.toString(), "");
        int userId = userLocalDatabase.getInt(AppConstant.User.USER_ID_KEY.toString(), -1);
        String firstName = userLocalDatabase.getString(
                AppConstant.User.FIRST_NAME_KEY.toString(), "");
        String lastName = userLocalDatabase.getString(
                AppConstant.User.LAST_NAME_KEY.toString(), "");
        long birthday = userLocalDatabase.getLong(
                AppConstant.User.BIRTHDAY_KEY.toString(), -1);
        String tokenId = userLocalDatabase.getString(
                AppConstant.Common.TOKEN_ID_KEY.toString(), "");
        User user = new User();
        user.setEmail(email);
        user.setUserId(userId);
        user.setLastName(lastName);
        user.setFirstName(firstName);
        Date createDate = new Date(birthday);
        user.setBirthday(createDate);
        user.setTokenId(tokenId);
        return user;
    }

    public void setUserLoggedIn(boolean loggedIn) {
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.putBoolean(CommonConstant.LOGGED_IN_KEY, loggedIn);
        spEditor.commit();
    }

    public boolean getUserLoggedIn() {
        if (userLocalDatabase.getBoolean(CommonConstant.LOGGED_IN_KEY, false)) {
            return true;
        } else {
            return false;
        }
    }

    public void clearUserData() {
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.clear();
        spEditor.commit();
    }
}
