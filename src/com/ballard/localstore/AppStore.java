package com.ballard.localstore;

import com.ballard.models.AppConfig;
import com.ballard.models.KeyValue;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ballard on 5/17/2016.
 */
public class AppStore {

    private static AppStore instance = null;
    private AppConfig appConfig;


    private HashMap<String, Object> fragmentSaveList;

    public static AppStore GetInstance() {
        if (instance == null) {
            instance = new AppStore();
        }
        return instance;
    }

    private AppStore() {
        fragmentSaveList = new HashMap<>();
    }

    public void setFragment(String fragmentName, Object fragment) {
        if (!fragmentSaveList.containsKey(fragmentName)) {
            fragmentSaveList.put(fragmentName, fragment);
        }
    }

    public Object getFragment(String fragmentName) {
        Object result = null;
        if (fragmentSaveList.containsKey(fragmentName)) {
            result = fragmentSaveList.get(fragmentName);
        }
        return result;
    }

    public AppConfig getAppConfig() {
        return appConfig;
    }

    public void setAppConfig(AppConfig appConfig) {
        this.appConfig = appConfig;
    }

    public String getPhotoUrl(int proxyId){
        String photoUrl = "";
        ArrayList<KeyValue<Integer, String>> photoDataList = appConfig.getPhotoUrls();
        for (KeyValue<Integer, String> photoData : photoDataList ) {
            if(photoData.getKey() == proxyId){
                photoUrl = photoData.getValue();
                break;
            }
        }
        return photoUrl;
    }
}
