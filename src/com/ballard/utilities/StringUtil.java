package com.ballard.utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by ballard on 2/20/2016.
 */
public class StringUtil {
    public static String format(final String source, final String... params) {
        String sourceSave = source;
        if (params.length > 0) {
            for (int i = 0; i < params.length; i++) {
                String findValue = String.format("{%s}", String.valueOf(i));
                sourceSave = sourceSave.replace(findValue, params[i]);
            }
        }
        return sourceSave;
    }

   /* public static Locale getSettingLocale(String cultureDefault){
        //String cultureDefault = getResources().getString(R.string.default_language);
        String[] culture = cultureDefault.split("-");
        Locale locale = new Locale(culture[0], culture[1]);
        return locale;
    }*/

    /**
     * Get Locale base on currency code
     *
     * @param currencyCode (ex: USD, VND)
     * @return
     */
    public static Locale getLocale(String currencyCode) {

        for (Locale locale : NumberFormat.getAvailableLocales()) {
            String code = NumberFormat.getCurrencyInstance(locale).getCurrency().getCurrencyCode();
            if (currencyCode.equals(code)) {
                return locale;
            }
        }
        return null;
    }

    public static String getPairValuePost(Object params)
            throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        if (params instanceof HashMap) {
            HashMap<String, String> paramsConvert = (HashMap<String, String>) params;
            for (Map.Entry<String, String> entry : paramsConvert.entrySet()) {
                if (first) {
                    first = false;
                } else {
                    result.append("&");
                }

                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            }
        }
        return result.toString();
    }

    //HashMap<String, String> params
    public static String getJsonDataPost(Object params)
            throws UnsupportedEncodingException {
        JSONObject object = null;
        if (params instanceof HashMap) {
            object = new JSONObject();
            HashMap<String, String> paramsConvert = (HashMap<String, String>) params;
            for (Map.Entry<String, String> entry : paramsConvert.entrySet()) {
                try {
                    // object.put(URLEncoder.encode(entry.getKey(), "UTF-8"),
                    // URLEncoder.encode(entry.getValue(), "UTF-8"));
                    object.put(entry.getKey(), entry.getValue());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (params instanceof JSONObject) {
            object = (JSONObject) params;
        }

        String dataPost = "";
        /*try {
            //dataPost = URLEncoder.encode(object.toString(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
        }*/
        dataPost = object.toString();

        return dataPost;
    }

    /**
     * Rounded function
     *
     * @param value
     * @param places
     * @return
     */
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
