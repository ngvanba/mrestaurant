package com.ballard.utilities;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ballard.activities.R;
import com.ballard.constants.AppConstant;
import com.ballard.models.Food;
import com.ballard.models.PriceQuantity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class Utility {

    public static Date getDate(String value) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = null;
        try {
            date = formatter.parse(value);
        } catch (java.text.ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return date;
    }

    public static String getCurrentWifiName(Context context) {
        String ssid = "";
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (networkInfo.isConnected()) {
            final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {
                ssid = connectionInfo.getSSID();
                ssid = ssid.replaceAll("\"", "");
            }
        }
        return ssid;
    }

    /**
     * Check network is available to use
     * @param context
     * @return true (available), false (not available)
     */
    public final static boolean isInternetOn(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public static boolean checkNetworkConnection(Activity activity) {
        final ConnectivityManager connMgr = (ConnectivityManager) activity.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        final NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo activeNetwork = connMgr.getActiveNetworkInfo();

        if (wifi.isAvailable() || mobile.isAvailable() || activeNetwork.isAvailable()) {
            return true;
        } else {
            return false;
        }
    }


	/*
        //Get Hask Key for facebook
		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					"com.ballard.activities",
					PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (PackageManager.NameNotFoundException e) {

		} catch (NoSuchAlgorithmException e) {

		}*/

    public static EditText createEditText(int _intID, Context context) {
        EditText editText = new EditText(context);
        editText.setId(_intID);
        // editText.setHint("My lines");
        //editText.setWidth(180);
        editText.setBackgroundColor(Color.WHITE);
        return editText;
    }

    public static TextView createTextView(int _intID, Context context) {
        TextView txtview = new TextView(context);
        txtview.setId(_intID);
        //txtview.setText("My lines:");
        txtview.setTextColor(Color.BLACK);
        txtview.setTypeface(Typeface.DEFAULT_BOLD);
        return txtview;
    }

    public static Button createButton(int _intID, Context context) {
        Button btnCreate = new Button(context);
        btnCreate.setId(_intID);
        btnCreate.setTextColor(context.getResources().getColor(R.color.button_text_color));
        btnCreate.setTypeface(Typeface.DEFAULT_BOLD);
        btnCreate.setTextSize(18);
        //btnCreate.setBackgroundResource(R.drawable.rounded_btn);
        btnCreate.setBackgroundColor(Color.BLUE);
        return btnCreate;
    }

    public static List<PriceQuantity> getPrices(int foodId, JSONArray pricesData) {
        List<PriceQuantity> priceQuantityList = new ArrayList<>();
        PriceQuantity priceQuantity = null;
        try {
            for (int i = 0; i < pricesData.length(); i++) {
                JSONObject object = pricesData.getJSONObject(i);
                int resultFoodId = object.getInt(AppConstant.Foods.FOOD_ID_KEY.toString());
                if (resultFoodId == foodId) {
                    priceQuantity = new PriceQuantity();
                    priceQuantity.setPrice(object.getDouble(AppConstant.Foods.FOOD_PRICE_KEY.toString()));
                    priceQuantity.setPriceName(object.getString(AppConstant.Foods.FOOD_SIZE_KEY.toString()));
                    priceQuantityList.add(priceQuantity);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (priceQuantityList.size() > 0) {
            priceQuantityList.get(0).setQuantity(1);
        }
        return priceQuantityList;
    }

    public static void updateFood(int foodId, Food newFood, ArrayList<Food> foodList) {
        for (Food food : foodList) {
            if (food.getFoodId() == foodId) {
                food.setFoodName(newFood.getFoodName());
                food.setDescription(newFood.getDescription());
                food.setUrlImage(newFood.getUrlImage());
                food.setLiked(newFood.getLiked());
                food.setRated(newFood.getRated());
                for (int i = 0; i < food.getPriceQuantities().size(); i++) {
                    PriceQuantity PriceQuantityUpdate = food.getPriceQuantities().get(i);
                    if (i < newFood.getPriceQuantities().size()) {
                        PriceQuantity newPriceQuantity = newFood.getPriceQuantities().get(i);
                        PriceQuantityUpdate.setPrice(newPriceQuantity.getPrice());
                        PriceQuantityUpdate.setPriceName(newPriceQuantity.getPriceName());
                    }
                    //newFood.getPriceQuantities().get(i) != null
                }
                break;
            }
        }
    }

    public static String displayDateTimeFromUTCToLocalTime(long millisecondUtc) {
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();

		/* debug: is it local time? */
        Log.d("Time zone: ", tz.getDisplayName());

		/* date formatter in local timezone */
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        sdf.setTimeZone(tz);
        String localTime = sdf.format(new Date(millisecondUtc * 1000)); // I assume your timestamp is in seconds and you're converting to milliseconds?
        Log.d("Time: ", localTime);
        return localTime;
    }

    public static String getFileNameBySize(String originalName, String size){
        String[] splitName = originalName.split("\\.");
        String newName = StringUtil.format("{0}_{1}.{2}",splitName[0],size, splitName[1]);
        return newName;
    }

	/*private static String getDate1(String OurDate)
	{
		try
		{
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
			Date value = formatter.parse(OurDate);

			SimpleDateFormat dateFormatter = new SimpleDateFormat("MM-dd-yyyy HH:mm"); //this format changeable
			dateFormatter.setTimeZone(TimeZone.getDefault());
			OurDate = dateFormatter.format(value);

			//Log.d("OurDate", OurDate);
		}
		catch (Exception e)
		{
			OurDate = "00-00-0000 00:00";
		}
		return OurDate;
	}*/

}
