package com.ballard.utilities;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

public class DateDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener{

	EditText txtDate;
	public DateDialog(View view){
		txtDate = (EditText)view;
	}
	public Dialog onCreateDialog(Bundle saveInstanceState){
		final Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		return new DatePickerDialog(getActivity(),this, year, month, day);
	}
	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		String date = String.format("%s/%s/%s", String.valueOf(dayOfMonth),String.valueOf(monthOfYear),String.valueOf(year));
		txtDate.setText(date);
		// TODO Auto-generated method stub

	}

}
