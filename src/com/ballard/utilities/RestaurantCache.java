package com.ballard.utilities;

import com.ballard.models.Restaurant;

import java.text.NumberFormat;

/**
 * Created by ballard on 3/21/2016.
 */
public class RestaurantCache {
    public static RestaurantCache instance = null;
    private Restaurant restaurant = null;
    private NumberFormat numberFormat;

    public static RestaurantCache GetInstance() {
        if (instance == null) {
            instance = new RestaurantCache();
        }
        return instance;
    }

    private RestaurantCache() {
    }

    public void set(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Restaurant get() {
        return restaurant;
    }

    public NumberFormat getNumberFormat() {
        return numberFormat;
    }

    public void setNumberFormat(NumberFormat numberFormat) {
        this.numberFormat = numberFormat;
    }
}
