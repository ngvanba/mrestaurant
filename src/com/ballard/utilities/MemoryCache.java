package com.ballard.utilities;

import android.graphics.Bitmap;
import android.util.Log;

import com.ballard.activities.GuestMenuActivity;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by ballard on 1/25/2016.
 */
public class MemoryCache {

    private static final String TAG = "MemoryCache";
    private Map<String, Bitmap> cache = Collections.synchronizedMap(
            new LinkedHashMap<String, Bitmap>(10, 1.5f, true));
    private long size = 0;//current allocated size
    private long limit = 1000000;//max memory in bytes
    private int menuSlideIndex = 0;
    private GuestMenuActivity menuActivity;

    public static MemoryCache instance = null;

    public static MemoryCache GetInstance() {
        if (instance == null) {
            instance = new MemoryCache();
        }
        return instance;
    }

    private MemoryCache() {
        //use 25% of available heap size
        setLimit(Runtime.getRuntime().maxMemory() / 4);
    }

    public void setLimit(long new_limit) {
        limit = new_limit;
        Log.i(TAG, "MemoryCache will use up to " + limit / 1024. / 1024. + "MB");
    }

    public Bitmap get(String id) {
        try {
            if (!cache.containsKey(id)) {
                return null;
            }
            return cache.get(id);
        } catch (NullPointerException ex) {
            return null;
        }
    }

    public void put(String id, Bitmap bitmap) {
        try {
            if (!cache.containsKey(id)) {
                cache.put(id, bitmap);
                size += getSizeInBytes(bitmap);
                checkSize();
            }

        } catch (Throwable th) {
        }
    }

    private void checkSize() {
        Log.i(TAG, "cache size=" + size + " length=" + cache.size());
        if (size > limit) {
            Iterator<Map.Entry<String, Bitmap>> iter = cache.entrySet().iterator();//least recently accessed item will be the first one iterated
            while (iter.hasNext()) {
                Map.Entry<String, Bitmap> entry = iter.next();
                size -= getSizeInBytes(entry.getValue());
                iter.remove();
                if (size <= limit) {
                    break;
                }
            }
            Log.i(TAG, "Clean cache. New size " + cache.size());
        }
    }

    public void clear() {
        try {
            cache.clear();
            size = 0;
        } catch (NullPointerException ex) {
        }
    }

    private long getSizeInBytes(Bitmap bitmap) {
        if (bitmap == null) {
            return 0;
        }
        return bitmap.getRowBytes() * bitmap.getHeight();
    }

    public void setCurrentMenuSlide(int menuSlideIndex) {
        this.menuSlideIndex = menuSlideIndex;
    }

    public int getCurrentMenuSlide() {
        return menuSlideIndex;
    }

    public void setGuestMenuActivity(GuestMenuActivity menuActivity) {
        this.menuActivity = menuActivity;
    }

    public GuestMenuActivity getGuestMenuActivity() {
        return menuActivity;
    }
}