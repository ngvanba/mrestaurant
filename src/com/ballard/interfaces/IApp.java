package com.ballard.interfaces;

import com.ballard.constants.CommonConstant;
import com.ballard.models.CommonResult;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by ballard on 6/23/2016.
 */
public interface IApp {

    @GET("app/{appKey}/{countryCode}")
    Call<JsonObject> getSettingApp(@Path("appKey") int appKey, @Path("countryCode") String countryCode);

    @POST("user/login")
    Call<JsonObject> login(@Body JsonObject body);

    @PUT("user/password")
    Call<JsonObject> changePwd(@Body JsonObject body);

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(CommonConstant.SERVER_ADDRESS_RETROFIT)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

}
