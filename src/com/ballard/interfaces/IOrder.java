package com.ballard.interfaces;

import com.ballard.constants.CommonConstant;
import com.ballard.models.CommonResult;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by ballard on 6/27/2016.
 */
public interface IOrder {

    @POST("order")
    Call<JsonObject> postOrder(@Body JsonObject body);

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(CommonConstant.SERVER_ADDRESS_RETROFIT)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
