package com.ballard.interfaces;

import com.ballard.models.CommonResult;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by ballard on 5/8/2016.
 */
public interface IFood {
    @Headers("Content-Type: application/json")
    @POST("foods/like")
    Call<CommonResult> like(@Body JsonObject body);

    @HTTP(method = "DELETE", path = "foods/like", hasBody = true)
    Call<CommonResult> deleteLike(@Body JsonObject body);

    @POST("food/comment")
    Call<CommonResult> postComment(@Body JsonObject body);

    @PUT("food/comment")
    Call<CommonResult> putComment(@Body JsonObject body);

    @HTTP(method = "DELETE", path = "food/comment", hasBody = true)
    Call<CommonResult> deleteComment(@Body JsonObject body);

    @GET("food/comment/{foodId}/{tokenID}")
    Call<JsonObject> getComment(@Path("foodId") int foodId, @Path("tokenID") String tokenID);

    @POST("food/rate")
    Call<CommonResult> postRate(@Body JsonObject body);

    @POST("food/favorite")
    Call<CommonResult> addFavorite(@Body JsonObject body);

    @HTTP(method = "DELETE", path = "food/favorite", hasBody = true)
    Call<CommonResult> deleteFavorite(@Body JsonObject body);

    @GET("food/favorite/{page}/{tokenID}")
    Call<JsonObject> getFavorite(@Path("page") int page, @Path("tokenID") String tokenID);

    @GET("category/{restaurant}/{tokenID}")
    Call<JsonObject> getCategories(@Path("restaurant") int page, @Path("tokenID") String tokenID);

    @GET("foods/{restaurant}/home/{page}/{tokenID}")
    Call<JsonObject> getHomePage(@Path("restaurant") int restaurant, @Path("page") int page, @Path("tokenID") String tokenID);

    @GET("foods/{restaurant}/{typePage}/{page}/{tokenID}")
    Call<JsonObject> getFoodPage(@Path("restaurant") int restaurant, @Path("typePage") String typePage, @Path("page") int page, @Path("tokenID") String tokenID);

    @GET("foods/{restaurant}/{categoryId}/{page}/{tokenID}")
    Call<JsonObject> getCategoryFoodPage(@Path("restaurant") int restaurant, @Path("categoryId") int categoryId, @Path("page") int page, @Path("tokenID") String tokenID);

    @GET("search/{query}/{restaurant}/{tokenID}")
    Call<JsonObject> getSearchFood(@Path("query") String query , @Path("restaurant") int restaurant , @Path("tokenID") String tokenID);

}
