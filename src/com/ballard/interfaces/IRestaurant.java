package com.ballard.interfaces;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by ballard on 6/26/2016.
 */
public interface IRestaurant {
    @GET("restaurants/{keySearch}/{tokenID}")
    Call<JsonObject> getSearchRestaurant(@Path("keySearch") String keySearch , @Path("tokenID") String tokenID);

    @GET("setting/restaurant/{restaurantId}/{tokenID}")
    Call<JsonObject> getRestaurantSetting(@Path("restaurantId") int restaurantId , @Path("tokenID") String tokenID);
}
