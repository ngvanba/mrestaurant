package com.ballard.requests;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.util.Log;

import com.ballard.activities.R;
import com.ballard.models.Food;
import com.ballard.responses.DownloadFinish;
import com.ballard.utilities.MemoryCache;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by ballard on 3/19/2016.
 */
public class DownloadFoodPhoto extends AsyncTask<String, Void, ArrayList<Food>> {
    private ArrayList<Food> listFood;
    private int startIndex;
    private Context context;
    private ProgressDialog progressDialog;
    private DownloadFinish delegate;

    public DownloadFoodPhoto(Context context, ArrayList<Food> listFood, int startIndex, int dialogGravity, DownloadFinish delegate) {
        this.context = context;
        this.listFood = listFood;
        this.startIndex = startIndex;
        this.delegate = delegate;

        progressDialog = ProgressDialog.show(context, null, null);
        progressDialog.setContentView(R.layout.progressbar);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.getWindow().setGravity(dialogGravity);
    }

    protected ArrayList<Food> doInBackground(String... urls) {
        for (int i = startIndex; i < listFood.size(); i++) {
            String urlImage = listFood.get(i).getUrlImage();
            //urlImage = "http://icons.iconarchive.com/icons/crountch/one-piece-jolly-roger/72/Luffys-flag-2-icon.png";
            if (urlImage.length() > 0) {
                Bitmap bitmapCache = MemoryCache.GetInstance().get(urlImage);
                if (bitmapCache == null) {
                    bitmapCache = downloadImage(urlImage);
                    MemoryCache.GetInstance().put(urlImage, bitmapCache);
                }
                listFood.get(i).setIconBitmap(bitmapCache);
            }
        }
        return listFood;
    }

    private Bitmap downloadImage(String urlString) {
        Bitmap resultBitmap = null;
        try {
            InputStream in = new java.net.URL(urlString).openStream();
            resultBitmap = BitmapFactory.decodeStream(in);
            //in.close();
        } catch (IOException e) {
            Log.e("Error", e.getMessage());
        }
        return resultBitmap;
    }

    @Override
    protected void onPostExecute(ArrayList<Food> result) {
        delegate.processComplete(result);
        progressDialog.dismiss();
    }
}
