package com.ballard.requests;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.util.Log;

import com.ballard.activities.R;
import com.ballard.constants.CommonConstant;
import com.ballard.constants.ResquestConstant;
import com.ballard.responses.AsyncResponse;
import com.ballard.utilities.StringUtil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class RequestAsyncTask extends AsyncTask<String, Void, String> {

    private ProgressDialog progressDialog;
    private int dataType;
    private AsyncResponse delegate;
    private Context context;
    private Object sendingData;
    private String loadingMessage = "Loading...";
    private String requestMethod = ResquestConstant.POST;
    private boolean isShowProgressDialog = true;

    // Constructor
    public RequestAsyncTask(AsyncResponse delegate) {
        this.delegate = delegate;
        this.context = (Context) delegate;
    }

    public RequestAsyncTask(Context context, AsyncResponse delegate,
                            int sendDataType, String loadingMessage) {
        this.delegate = delegate;
        this.dataType = sendDataType;
        this.context = context;
        this.loadingMessage = loadingMessage;
    }

    public RequestAsyncTask(Context context, AsyncResponse delegate,
                            Object sendingData, int sendDataType) {
        this.delegate = delegate;
        this.dataType = sendDataType;
        this.context = context;
        this.sendingData = sendingData;
    }

    public RequestAsyncTask(Context context, AsyncResponse delegate,
                            Object sendingData, int sendDataType,
                            String loadingMessage) {
        this.delegate = delegate;
        this.context = context;
        this.dataType = sendDataType;
        this.sendingData = sendingData;
        this.loadingMessage = loadingMessage;
    }

    // End Constructor

    @Override
    protected void onPreExecute() {
        if (isShowProgressDialog) {
            progressDialog = ProgressDialog.show(context, null, null);
            progressDialog.setContentView(R.layout.progressbar);
            progressDialog.setMessage(loadingMessage);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            progressDialog.show();
        }
        super.onPreExecute();
    }// onPreExecute

    @Override
    protected String doInBackground(String... urls) {

        String result = "";

        for (int i = 0; i <= 0; i++) {

            result = invokeConnect(urls[i], sendingData);
        }

        return result;
    }// doInBackground

    /**
     * This method will be open a connection to server for sending a request by POST or GET method.
     *
     * @param requestURL
     * @param dataParams
     * @return
     */
    private String invokeConnect(String requestURL,
                                 Object dataParams) {
        URL url;
        String response = "";
        int responseCode = -1;
        try {
            url = new URL(requestURL);
            System.setProperty("http.keepAlive", "false");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(CommonConstant.TIMEOUT_CONNECTION);
            conn.setConnectTimeout(CommonConstant.TIMEOUT_CONNECTION);
            conn.setRequestMethod(requestMethod);
            if (requestMethod.equals(ResquestConstant.POST) || requestMethod.equals(ResquestConstant.PUT) || requestMethod.equals(ResquestConstant.DELETE)) {
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Connection", "close");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                        os, "UTF-8"));
                if (dataType == ResquestConstant.JSON_DATA) {
                    String data = StringUtil.getJsonDataPost(dataParams);
                    writer.write(data);
                    Log.d("requestURL:dataParams", requestURL + data);
                    //os.write(data.getBytes("UTF-8"));
                } else {
                    writer.write(StringUtil.getPairValuePost(dataParams));
                }

                writer.flush();
                writer.close();
                os.close();
            }

            responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }
            conn.disconnect();
            Log.i("ResponseCode", responseCode + "");
            Log.d("Response", response);
        } catch (MalformedURLException e) {
            Log.e("MalformedURLException", "URL is invalid");
        } catch (EOFException e) {
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("IOException", "Connection is interrupted");
        }
        return response;
    }


    @Override
    protected void onPostExecute(String result) {
        if (isShowProgressDialog) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
        result = result.trim();
        delegate.processFinish(result);
    }

    public void setProgressDialogShow(boolean value) {
        isShowProgressDialog = value;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    public void setDataType(int dataType) {
        this.dataType = dataType;
    }

    public void setLoadingMessage(String loadingMessage) {
        this.loadingMessage = loadingMessage;
    }

	/*public HashMap<String, String> getSendingData() {
        return sendingData;
	}
*/
	/*public void setSendingData(HashMap<String, String> sendingData) {
		this.sendingData = sendingData;
	}*/

	/*public Context getContext() {
		return context;
	}*/

}
