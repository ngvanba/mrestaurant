package com.ballard.requests;


public interface GetUserCallBack<T> {
	public abstract void done(T returnObject);
}
