package com.ballard.customs;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ListView;

import com.ballard.utilities.MemoryCache;

/**
 * Created by ballard on 3/19/2016.
 */
public class MyGestureDetectorMenuSlide extends GestureDetector.SimpleOnGestureListener {
    private static final int SWIPE_THRESHOLD = 100;
    private static final int SWIPE_VELOCITY_THRESHOLD = 100;
    private ListView listView;

    public MyGestureDetectorMenuSlide(ListView listView) {
        this.listView = listView;
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2,
                            float distanceX, float distanceY) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                           float velocityY) {
        boolean result = false;
        try {
            float diffY = e2.getY() - e1.getY();
            float diffX = e2.getX() - e1.getX();
            if (Math.abs(diffX) > Math.abs(diffY)) {
                if (Math.abs(diffX) > SWIPE_THRESHOLD
                        && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                    int pos = listView.pointToPosition((int) e1.getX(), (int) e2.getY());
                    if (diffX > 0) {
                        onSwipeRight(pos);
                    } else {
                        onSwipeLeft(pos);
                    }
                }
            } else {

            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return result;

    }

    public void onSwipeRight(int pos) {
        int currentIndex = MemoryCache.GetInstance().getCurrentMenuSlide();
        if (currentIndex < MemoryCache.GetInstance().getGuestMenuActivity().getMaxMenuSize()) {
            MemoryCache.GetInstance().getGuestMenuActivity().displayView(currentIndex + 1);
        }
    }

    public void onSwipeLeft(int pos) {
        int currentIndex = MemoryCache.GetInstance().getCurrentMenuSlide();
        if (currentIndex > 0) {
            MemoryCache.GetInstance().getGuestMenuActivity().displayView(currentIndex - 1);
        }
    }
}
