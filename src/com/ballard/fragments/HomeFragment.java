package com.ballard.fragments;


import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.ballard.activities.R;
import com.ballard.adapter.FoodListAdapter;
import com.ballard.constants.AppConstant;
import com.ballard.constants.CommonConstant;
import com.ballard.constants.ResquestConstant;
import com.ballard.customs.MyGestureDetectorMenuSlide;
import com.ballard.interfaces.IFood;
import com.ballard.localstore.AppStore;
import com.ballard.localstore.UserLocalStore;
import com.ballard.models.Comment;
import com.ballard.models.Food;
import com.ballard.requests.RequestAsyncTask;
import com.ballard.responses.AsyncResponse;
import com.ballard.utilities.RestaurantCache;
import com.ballard.utilities.StringUtil;
import com.ballard.utilities.Utility;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    private Button bnRateOrder;
    ArrayList<Food> listFoodsServer;
    ArrayList<Food> loadingFoodsPagingUI;
    ArrayList<String> downloadContentPage;
    FoodListAdapter adapter;
    int countPagingServer = 0;
    int pageCount = 1;
    int startingIndex;
    int endingIndex;
    private UserLocalStore userLocalStore;
    private SwipeRefreshLayout swipeRefreshLayout;

    private ListView listView;

    public HomeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_food_view, container, false);
        userLocalStore = new UserLocalStore(getActivity());
        downloadContentPage = new ArrayList<>();

        /*bnRateOrder = (Button) rootView.findViewById(R.id.bnRateOrder);
        bnRateOrder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                *//*OrderFoodFragment orderFoodDialog = new OrderFoodFragment();
                FragmentManager fragmentManager = getFragmentManager();
                orderFoodDialog.show(fragmentManager, "");*//*
            }
        });*/
        listView = (ListView) rootView.findViewById(R.id.list_foods_view);
        listFoodsServer = new ArrayList<Food>();
        loadingFoodsPagingUI = new ArrayList<Food>();
        FragmentManager fragmentManager = getFragmentManager();
        adapter = new FoodListAdapter(getActivity(), loadingFoodsPagingUI, fragmentManager);
        listView.setAdapter(adapter);
        countPagingServer = 0;
        //GetFoods(countPagingServer);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        listFoodsServer.clear();
                                        loadingFoodsPagingUI.clear();
                                        adapter.notifyDataSetChanged();
                                        pageCount = 1;
                                        GetFoods(0);
                                    }
                                }
        );

        // Implementing scroll refresh
        /*listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            int threshold = 1;
            int count = listView.getCount();

            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollStatus) {
                if (scrollStatus == SCROLL_STATE_IDLE) {
                    if (listView.getLastVisiblePosition() >= count - threshold && pageCount <= determinePagingUI()) {

                        startingIndex = CommonConstant.UI_PAGE_SIZE * (pageCount - 1);
                        if (startingIndex < 0) {
                            startingIndex = 0;
                        }
                        endingIndex = startingIndex + CommonConstant.UI_PAGE_SIZE;
                        if (endingIndex > listFoodsServer.size()) {
                            endingIndex = listFoodsServer.size();
                        }
                        for (int i = startingIndex; i < endingIndex; i++) {
                            loadingFoodsPagingUI.add(listFoodsServer.get(i));
                        }
                        pageCount++;
                        *//*DownloadFoodPhoto downloadFoodPhoto = new DownloadFoodPhoto(getActivity(), loadingFoodsPagingUI, startingIndex, Gravity.CENTER,
                                new DownloadFinish() {
                                    @Override
                                    public void processComplete(Object results) {
                                        adapter.notifyDataSetChanged();
                                    }
                                });
                        downloadFoodPhoto.execute();*//*
                       *//* if(loadingFoodsPagingUI.size() >= listFoodsServer.size() - CommonConstant.UI_PAGE_SIZE){
                            countPagingServer++;
                            GetFoods(countPagingServer);
                        }*//*
                    }
                }
            }

            @Override
            public void onScroll(AbsListView absListView, int firstItem, int visibleItemCount, final int totalItems) {
            }
        });*/

        /*listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CurrentSelectFoodDialog currentSelectFoodDialog = new CurrentSelectFoodDialog();
                FragmentManager fragmentManager = getFragmentManager();
                currentSelectFoodDialog.show(fragmentManager, "");
                //Select food item on list
                Food foodSelect = listFoodsServer.get(position);
                //If this food was existed in my order -> get it. Otherwise, keep the selected food
                foodSelect = MyFoodOrder.getInstance().findFoodInOrder(foodSelect);
                //Set to current dialog
                MyFoodOrder.getInstance().setCurrentFood(foodSelect);

            }
        });*/

        final GestureDetector gestureDetector = new GestureDetector(getActivity(),
                new MyGestureDetectorMenuSlide(listView));

        listView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });
        return rootView;
    }

    private int determinePagingUI() {
        int determinePages;
        if (listFoodsServer.size() % CommonConstant.UI_PAGE_SIZE == 0) {
            determinePages = listFoodsServer.size() / CommonConstant.UI_PAGE_SIZE;
        } else {
            determinePages = (listFoodsServer.size() / CommonConstant.UI_PAGE_SIZE) + 1;
        }
        return determinePages;
    }

    protected void GetFoods(int page) {
        swipeRefreshLayout.setRefreshing(true);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(CommonConstant.SERVER_ADDRESS_RETROFIT).client(client)
                .build();
        int restaurant = RestaurantCache.GetInstance().get().getId();
        String token = userLocalStore.getLoggedInUser().getTokenId();
        IFood service = retrofit.create(IFood.class);
        Call<JsonObject> call = service.getHomePage(restaurant, page, token);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject jsonResponse = response.body();
                try {
                    JSONObject objects = (JSONObject) new JSONTokener(jsonResponse.toString()).nextValue();
                    JSONObject objectData = objects.getJSONObject(AppConstant.Common.DATA_KEY.toString());
                    JSONObject objectTopRateData = objectData.getJSONObject(AppConstant.Foods.TOP_RATED_KEY.toString());
                    JSONObject objectPromotionData = objectData.getJSONObject(AppConstant.Foods.PROMOTION_KEY.toString());
                    JSONObject objectFeaturedData = objectData.getJSONObject(AppConstant.Foods.FEATURED_KEY.toString());
                    JSONObject objectNewFoodData = objectData.getJSONObject(AppConstant.Foods.NEW_FOOD_KEY.toString());
                    //Top rated data
                    JSONArray foodsTopRateData = objectTopRateData.getJSONArray(AppConstant.Foods.FOODS_KEY.toString());
                    JSONArray pricesTopRateData = objectTopRateData.getJSONArray(AppConstant.Foods.FOOD_PRICES_KEY.toString());
                    // Promotion data
                    JSONArray foodsPromotionData = objectPromotionData.getJSONArray(AppConstant.Foods.FOODS_KEY.toString());
                    JSONArray pricesPromotionData = objectPromotionData.getJSONArray(AppConstant.Foods.FOOD_PRICES_KEY.toString());
                    // Featured data
                    JSONArray foodsFeaturedData = objectFeaturedData.getJSONArray(AppConstant.Foods.FOODS_KEY.toString());
                    JSONArray pricesFeaturedData = objectFeaturedData.getJSONArray(AppConstant.Foods.FOOD_PRICES_KEY.toString());
                    // Promotion data
                    JSONArray foodsNewFoodData = objectNewFoodData.getJSONArray(AppConstant.Foods.FOODS_KEY.toString());
                    JSONArray pricesNewFoodData = objectNewFoodData.getJSONArray(AppConstant.Foods.FOOD_PRICES_KEY.toString());
                    //Add top rated to list view
                    for (int i = 0; i < foodsTopRateData.length(); i++) {
                        JSONObject object = foodsTopRateData.getJSONObject(i);
                        int foodId = object.getInt(AppConstant.Foods.FOOD_ID_KEY.toString());
                        String foodName = object.getString(AppConstant.Foods.FOOD_NAME_KEY.toString());
                        String foodDescription = object.getString(AppConstant.Foods.FOOD_DESCRIPTION_KEY.toString());
                        double rated = object.getDouble(AppConstant.Foods.RATED_KEY.toString());
                        int liked = object.getInt(AppConstant.Foods.LIKED_KEY.toString());
                        int commentCount = object.getInt(AppConstant.Foods.COMMENT_COUNT_KEY.toString());

                        //Process get image
                        String foodPhotoName = object.getString(AppConstant.Foods.FOOD_PHOTO_KEY.toString());
                        int photoProxyId = object.getInt(AppConstant.Foods.FOOD_PHOTO_URL_KEY.toString());
                        int photoProcessed = object.getInt(AppConstant.Foods.PHOTO_PROCESSED_KEY.toString());
                        String photoProxyUrl = AppStore.GetInstance().getPhotoUrl(photoProxyId);
                        String fullPhotoPath = "";
                        if (photoProcessed == CommonConstant.PROCESSED) {
                            fullPhotoPath = StringUtil.format("{0}/{1}", photoProxyUrl, Utility.getFileNameBySize(foodPhotoName, "200x200"));
                        } else {
                            fullPhotoPath = StringUtil.format("{0}/{1}", photoProxyUrl, foodPhotoName);
                        }

                        Food food = new Food(fullPhotoPath, foodId, foodName, foodDescription, Utility.getPrices(foodId, pricesTopRateData), 1);
                        food.setLiked(liked);
                        food.setRated(rated);
                        food.setCommentCount(commentCount);
                        if (i == 0) {
                            //try {
                                food.setShowHeader(true);
                                food.setHeaderType(CommonConstant.RATED);
                                food.setHeaderItemTitle(mGuestMenuActivity.getString(R.string.rated));
                           /* } catch (Exception ex) {
                                ex.printStackTrace();
                            }*/
                        }
                        listFoodsServer.add(food);
                    }
                    //Add promotion to list view
                    for (int i = 0; i < foodsPromotionData.length(); i++) {
                        JSONObject object = foodsPromotionData.getJSONObject(i);
                        int foodId = object.getInt(AppConstant.Foods.FOOD_ID_KEY.toString());
                        String foodName = object.getString(AppConstant.Foods.FOOD_NAME_KEY.toString());
                        String foodDescription = object.getString(AppConstant.Foods.FOOD_DESCRIPTION_KEY.toString());
                        String foodPhoto = object.getString(AppConstant.Foods.FOOD_PHOTO_KEY.toString());
                        double rated = object.getDouble(AppConstant.Foods.RATED_KEY.toString());
                        int liked = object.getInt(AppConstant.Foods.LIKED_KEY.toString());
                        int commentCount = object.getInt(AppConstant.Foods.COMMENT_COUNT_KEY.toString());

                        //Process get image
                        String foodPhotoName = object.getString(AppConstant.Foods.FOOD_PHOTO_KEY.toString());
                        int photoProxyId = object.getInt(AppConstant.Foods.FOOD_PHOTO_URL_KEY.toString());
                        int photoProcessed = object.getInt(AppConstant.Foods.PHOTO_PROCESSED_KEY.toString());
                        String photoProxyUrl = AppStore.GetInstance().getPhotoUrl(photoProxyId);
                        String fullPhotoPath = "";
                        if (photoProcessed == CommonConstant.PROCESSED) {
                            fullPhotoPath = StringUtil.format("{0}/{1}", photoProxyUrl, Utility.getFileNameBySize(foodPhotoName, "200x200"));
                        } else {
                            fullPhotoPath = StringUtil.format("{0}/{1}", photoProxyUrl, foodPhotoName);
                        }

                        Food food = new Food(fullPhotoPath, foodId, foodName, foodDescription, Utility.getPrices(foodId, pricesPromotionData), 1);
                        food.setRated(rated);
                        food.setLiked(liked);
                        food.setCommentCount(commentCount);
                        if (i == 0) {
                            //try {
                                food.setShowHeader(true);
                                food.setHeaderType(CommonConstant.PROMOTION);
                                food.setHeaderItemTitle(mGuestMenuActivity.getString(R.string.promotion));
                           /* } catch (Exception ex) {
                                ex.printStackTrace();
                            }*/
                        }
                        listFoodsServer.add(food);
                    }
                    //Add featured to list view
                    for (int i = 0; i < foodsFeaturedData.length(); i++) {
                        JSONObject object = foodsFeaturedData.getJSONObject(i);
                        int foodId = object.getInt(AppConstant.Foods.FOOD_ID_KEY.toString());
                        String foodName = object.getString(AppConstant.Foods.FOOD_NAME_KEY.toString());
                        String foodDescription = object.getString(AppConstant.Foods.FOOD_DESCRIPTION_KEY.toString());
                        String foodPhoto = object.getString(AppConstant.Foods.FOOD_PHOTO_KEY.toString());
                        double rated = object.getDouble(AppConstant.Foods.RATED_KEY.toString());
                        int liked = object.getInt(AppConstant.Foods.LIKED_KEY.toString());
                        int commentCount = object.getInt(AppConstant.Foods.COMMENT_COUNT_KEY.toString());

                        //Process get image
                        String foodPhotoName = object.getString(AppConstant.Foods.FOOD_PHOTO_KEY.toString());
                        int photoProxyId = object.getInt(AppConstant.Foods.FOOD_PHOTO_URL_KEY.toString());
                        int photoProcessed = object.getInt(AppConstant.Foods.PHOTO_PROCESSED_KEY.toString());
                        String photoProxyUrl = AppStore.GetInstance().getPhotoUrl(photoProxyId);
                        String fullPhotoPath = "";
                        if (photoProcessed == CommonConstant.PROCESSED) {
                            fullPhotoPath = StringUtil.format("{0}/{1}", photoProxyUrl, Utility.getFileNameBySize(foodPhotoName, "200x200"));
                        } else {
                            fullPhotoPath = StringUtil.format("{0}/{1}", photoProxyUrl, foodPhotoName);
                        }

                        Food food = new Food(fullPhotoPath, foodId, foodName, foodDescription, Utility.getPrices(foodId, pricesFeaturedData), 1);
                        food.setRated(rated);
                        food.setLiked(liked);
                        food.setCommentCount(commentCount);
                        if (i == 0) {
//                            try {
                                food.setShowHeader(true);
                                food.setHeaderType(CommonConstant.FEATURED);
                                food.setHeaderItemTitle(mGuestMenuActivity.getString(R.string.feature));
//                            }catch (Exception ex){
//                                ex.printStackTrace();
//                            }
                        }
                        listFoodsServer.add(food);
                    }
                    //Add new foods to list view
                    for (int i = 0; i < foodsNewFoodData.length(); i++) {
                        JSONObject object = foodsNewFoodData.getJSONObject(i);
                        int foodId = object.getInt(AppConstant.Foods.FOOD_ID_KEY.toString());
                        String foodName = object.getString(AppConstant.Foods.FOOD_NAME_KEY.toString());
                        String foodDescription = object.getString(AppConstant.Foods.FOOD_DESCRIPTION_KEY.toString());
                        String foodPhoto = object.getString(AppConstant.Foods.FOOD_PHOTO_KEY.toString());
                        double rated = object.getDouble(AppConstant.Foods.RATED_KEY.toString());
                        int liked = object.getInt(AppConstant.Foods.LIKED_KEY.toString());
                        int commentCount = object.getInt(AppConstant.Foods.COMMENT_COUNT_KEY.toString());

                        //Process get image
                        String foodPhotoName = object.getString(AppConstant.Foods.FOOD_PHOTO_KEY.toString());
                        int photoProxyId = object.getInt(AppConstant.Foods.FOOD_PHOTO_URL_KEY.toString());
                        int photoProcessed = object.getInt(AppConstant.Foods.PHOTO_PROCESSED_KEY.toString());
                        String photoProxyUrl = AppStore.GetInstance().getPhotoUrl(photoProxyId);
                        String fullPhotoPath = "";
                        if (photoProcessed == CommonConstant.PROCESSED) {
                            fullPhotoPath = StringUtil.format("{0}/{1}", photoProxyUrl, Utility.getFileNameBySize(foodPhotoName, "200x200"));
                        } else {
                            fullPhotoPath = StringUtil.format("{0}/{1}", photoProxyUrl, foodPhotoName);
                        }

                        Food food = new Food(fullPhotoPath, foodId, foodName, foodDescription, Utility.getPrices(foodId, pricesNewFoodData), 1);
                        food.setRated(rated);
                        food.setLiked(liked);
                        food.setCommentCount(commentCount);
                        if (i == 0) {
                            //try {
                                food.setShowHeader(true);
                                food.setHeaderType(CommonConstant.NEW_FOOD);
                                food.setHeaderItemTitle(mGuestMenuActivity.getString(R.string.new_food));
                           /* } catch (Exception ex) {
                                ex.printStackTrace();
                            }*/
                        }
                        listFoodsServer.add(food);
                    }

                    startingIndex = CommonConstant.UI_PAGE_SIZE * (pageCount - 1);
                    if (startingIndex < 0) {
                        startingIndex = 0;
                    }
                    endingIndex = startingIndex + CommonConstant.UI_PAGE_SIZE;
                    if (endingIndex > listFoodsServer.size()) {
                        endingIndex = listFoodsServer.size();
                    }
                    for (int i = startingIndex; i < endingIndex; i++) {
                        loadingFoodsPagingUI.add(listFoodsServer.get(i));
                    }
                    pageCount++;

                    adapter.notifyDataSetChanged();
                    swipeRefreshLayout.setRefreshing(false);
                } catch (JSONException ex) {
                    swipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(getActivity(), getString(R.string.no_data_message), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    @Override
    public void onRefresh() {
        GetFoods(0);
    }

   /* private void fetchFood() {
    }*/
}
