package com.ballard.fragments;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.ballard.activities.R;
import com.ballard.adapter.PriceListAdapter;
import com.ballard.businesses.MyFoodOrder;
import com.ballard.models.Food;
import com.ballard.utilities.RestaurantCache;
import com.ballard.utilities.StringUtil;

import java.text.NumberFormat;
import java.util.Locale;

public class CurrentSelectFoodDialog extends DialogFragment {
    EditText txtRequestNote;
    ListView listViewPrice;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        getDialog().getWindow().setGravity(Gravity.BOTTOM);
//        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, 300);
        View rootView = inflater.inflate(R.layout.current_select_item_foods, container, false);
        listViewPrice = (ListView) rootView.findViewById(R.id.listViewPrice);
        ImageView imgView = (ImageView) rootView.findViewById(R.id.icon);
        TextView txtTitle = (TextView) rootView.findViewById(R.id.txtTitle);
        txtTitle.setText(MyFoodOrder.getInstance().getCurrentSelectFood().getFoodName());
        txtRequestNote = (EditText) rootView.findViewById(R.id.txtRequestNote);

        PriceListAdapter priceAdapter = new PriceListAdapter(getActivity(), MyFoodOrder.getInstance().getCurrentSelectFood());
        listViewPrice.setAdapter(priceAdapter);
        if (MyFoodOrder.getInstance().getCurrentSelectFood().getIconBitmap() != null) {
            imgView.setImageBitmap(MyFoodOrder.getInstance().getCurrentSelectFood().getIconBitmap());
        }

        Locale locale = StringUtil.getLocale(RestaurantCache.GetInstance().get().getDefaultsList().get(0).getCurrencyCode());
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);


        Button bntAddFood = (Button) rootView.findViewById(R.id.bntAddFood);
        bntAddFood.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Food foodCurrent = MyFoodOrder.getInstance().getCurrentSelectFood();
                foodCurrent.setRequestNote(txtRequestNote.getText().toString());
                MyFoodOrder.getInstance().addFoodToMyOrder(foodCurrent);
                MyFoodOrder.getInstance().clearCurrentSelectFood();
                if (MyFoodOrder.getInstance().getNotifyCart() != null) {
                    long quantities = (long) MyFoodOrder.getInstance().getTotalQuantities();
                    String strQuantities = "\t\t" + String.valueOf(quantities);
                    MyFoodOrder.getInstance().getNotifyCart().setText(strQuantities);
                }
                dismiss();
            }
        });
        Button bntCancel = (Button) rootView.findViewById(R.id.bntCancel);
        bntCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);
    }
}