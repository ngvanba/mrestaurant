package com.ballard.fragments;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.ballard.activities.R;
import com.ballard.adapter.CommentListAdapter;
import com.ballard.businesses.MyFoodOrder;
import com.ballard.constants.AppConstant;
import com.ballard.constants.CommonConstant;
import com.ballard.constants.ResquestConstant;
import com.ballard.interfaces.IFood;
import com.ballard.localstore.UserLocalStore;
import com.ballard.models.Comment;
import com.ballard.models.CommonResult;
import com.ballard.models.User;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CommentFoodFragmentDialog extends DialogFragment {
    //public class OrderFoodFragment extends DialogFragment {

    private ListView listSelectedView;
    private CommentListAdapter adapter;
    private UserLocalStore userLocalStore;
    private ArrayList<Comment> listComment;
    private EditText txtWriteComment;
    private ImageButton bntEnterComment;
    private Comment currentComment = null;

    public CommentFoodFragmentDialog() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_food_comment, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        userLocalStore = new UserLocalStore(getActivity());

        listSelectedView = (ListView) rootView.findViewById(R.id.list_comment_foods);
        //
        LinearLayout panelCommentParent = (LinearLayout) rootView.findViewById(R.id.panelCommentParent);
        LinearLayout panelWriteComment = (LinearLayout) rootView.findViewById(R.id.panelWriteComment);
        ImageButton bntBackComment = (ImageButton) rootView.findViewById(R.id.bntBackComment);
        bntBackComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        if (MyFoodOrder.getInstance().isShowPanelWriteComment()) {
            panelCommentParent.setWeightSum(10);
            panelWriteComment.setVisibility(View.VISIBLE);
            bntEnterComment = (ImageButton) rootView.findViewById(R.id.bntEnterComment);
            bntEnterComment.setVisibility(View.INVISIBLE);
            txtWriteComment = (EditText) rootView.findViewById(R.id.txtWriteComment);
            txtWriteComment.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (txtWriteComment.getText().length() > 0) {
                        bntEnterComment.setVisibility(View.VISIBLE);
                    } else {
                        bntEnterComment.setVisibility(View.INVISIBLE);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });


            bntEnterComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    User userLogin = userLocalStore.getLoggedInUser();
                    String requestMethod = ResquestConstant.POST;
                    //Add new
                    if (currentComment == null) {
                        currentComment = new Comment();
                        currentComment.setFoodId(MyFoodOrder.getInstance().getCurrentSelectFood().getFoodId());
                        currentComment.setFullName(userLogin.getEmail());
                        currentComment.setUserEmail(userLogin.getEmail());
                        currentComment.setComment(txtWriteComment.getText().toString());
                        listComment.add(currentComment);
                        requestMethod = ResquestConstant.POST;
                    } else {
                        //Update
                        currentComment.setFullName(userLogin.getEmail());
                        currentComment.setComment(txtWriteComment.getText().toString());
                        requestMethod = ResquestConstant.PUT;
                    }
                    AddUpdateComment(currentComment, requestMethod);
                    adapter.notifyDataSetChanged();
                    txtWriteComment.setText("");
                    currentComment = null;
                }
            });
        } else {
            panelCommentParent.setWeightSum(9);
            panelWriteComment.setVisibility(View.GONE);
        }


        listComment = new ArrayList<Comment>();
        adapter = new CommentListAdapter(getActivity(), listComment);
        listSelectedView.setAdapter(adapter);

        GetComment(MyFoodOrder.getInstance().getCurrentSelectFood().getFoodId());

        listSelectedView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Comment comment = listComment.get(position);
                if (userLocalStore.getLoggedInUser().getEmail().equalsIgnoreCase(comment.getUserEmail())) {
                    currentComment = listComment.get(position);
                    txtWriteComment.setText(currentComment.getComment());
                } else {
                    currentComment = null;
                }

            }
        });

        final GestureDetector gestureDetector = new GestureDetector(getActivity(),
                new MyGestureDetector());

        listSelectedView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });


        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
    }

    class MyGestureDetector extends GestureDetector.SimpleOnGestureListener {
        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public void onShowPress(MotionEvent e) {
            // TODO Auto-generated method stub

        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2,
                                float distanceX, float distanceY) {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public void onLongPress(MotionEvent e) {
            // TODO Auto-generated method stub

        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                               float velocityY) {
            boolean result = false;
            try {
                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD
                            && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        int pos = listSelectedView.pointToPosition((int) e1.getX(), (int) e2.getY());
                        if (diffX > 0) {
                            onSwipeRight(pos);
                        } else {
                            onSwipeLeft(pos);
                        }
                    }
                } else {

                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return result;

        }

        @Override
        public boolean onDown(MotionEvent e) {
            // TODO Auto-generated method stub
            return false;
        }

        public void onSwipeRight(int pos) {
            confirmDelete(pos);
        }

        public void onSwipeLeft(int pos) {
            confirmDelete(pos);
        }

        private void confirmDelete(final int pos) {
            AlertDialog alert = new AlertDialog.Builder(getActivity()).create();
            alert.setTitle(getString(R.string.confirm));
            alert.setMessage(getString(R.string.confirm_delete));
            alert.setButton(DialogInterface.BUTTON_POSITIVE,
                    getString(R.string.ok_button), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            deleteComment(listComment.get(pos));
                            listComment.remove(pos);
                            adapter.notifyDataSetChanged();
                        }
                    });
            alert.setButton(DialogInterface.BUTTON_NEGATIVE,
                    getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // dismiss();
                        }
                    });
            alert.show();
        }


    }

    protected void GetComment(int foodId) {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(CommonConstant.SERVER_ADDRESS_RETROFIT).client(client)
                .build();

        String token = userLocalStore.getLoggedInUser().getTokenId();
        IFood service = retrofit.create(IFood.class);
        Call<JsonObject> call = service.getComment(foodId, token);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject jsonResponse = response.body();
                try {
                    JSONObject objects = (JSONObject) new JSONTokener(jsonResponse.toString()).nextValue();
                    JSONArray objectData = objects.getJSONArray(AppConstant.Common.DATA_KEY.toString());
                    for (int i = 0; i < objectData.length(); i++) {
                        JSONObject object = objectData.getJSONObject(i);
                        int commentId = object.getInt(AppConstant.Comment.COMMENT_ID_KEY.toString());
                        int foodIdDb = object.getInt(AppConstant.Foods.FOOD_ID_KEY.toString());
                        String userEmail = object.getString(AppConstant.Comment.USER_KEY.toString());
                        long createdAt = object.getLong(AppConstant.Comment.CREATED_AT_KEY.toString());
                        long updatedAt = object.getLong(AppConstant.Comment.UPDATED_AT_KEY.toString());
                        String content = object.getString(AppConstant.Comment.CONTENT_KEY.toString());
                        String userDisplayName = object.getString(AppConstant.Comment.USER_DISPLAY_NAME_KEY.toString());
                        //String foodPhoto = object.getString(AppConstant.Foods.FOOD_PHOTO_KEY.toString());

                        Comment comment = new Comment();
                        comment.setCommentId(commentId);
                        comment.setFoodId(foodIdDb);
                        comment.setUserEmail(userEmail);
                        comment.setFullName(userDisplayName);
                        comment.setComment(content);
                        comment.setCreatedAt(createdAt);
                        comment.setUpdatedAt(updatedAt);

                        listComment.add(comment);
                    }
                    adapter.notifyDataSetChanged();
                } catch (JSONException ex) {
                    Toast.makeText(getActivity(), getString(R.string.no_data_message), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }


    private void AddUpdateComment(Comment comment, String methodRequest) {
        JsonObject postData = createJsonObjectOrder(comment, methodRequest);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(CommonConstant.SERVER_ADDRESS_RETROFIT).client(client)
                .build();

        Call<CommonResult> call = null;
        IFood service = retrofit.create(IFood.class);
        if (methodRequest == ResquestConstant.POST) {
            //POST
            call = service.postComment(postData);
        } else {
            //PUT
            call = service.putComment(postData);
        }

        call.enqueue(new Callback<CommonResult>() {
            @Override
            //public void onResponse(Call<CommonResult> response) {
            public void onResponse(Call<CommonResult> call, Response<CommonResult> response) {

                int statusCode = response.code();
                CommonResult jsonResponse = response.body();
                int result = Integer.parseInt(jsonResponse.getResult());
                if (result == 0) {

                }
            }

            @Override
            public void onFailure(Call<CommonResult> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    private void deleteComment(Comment comment) {
        JsonObject postData = new JsonObject();
        String token = userLocalStore.getLoggedInUser().getTokenId();
        postData.addProperty("comment_id", comment.getCommentId());
        postData.addProperty("tokenID", token);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(CommonConstant.SERVER_ADDRESS_RETROFIT).client(client)
                .build();


        IFood service = retrofit.create(IFood.class);
        Call<CommonResult> call = service.deleteComment(postData);
        call.enqueue(new Callback<CommonResult>() {
            @Override
            //public void onResponse(Call<CommonResult> response) {
            public void onResponse(Call<CommonResult> call, Response<CommonResult> response) {

                int statusCode = response.code();
                CommonResult jsonResponse = response.body();
                int result = Integer.parseInt(jsonResponse.getResult());
                if (result == 0) {

                }
            }

            @Override
            public void onFailure(Call<CommonResult> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    private JsonObject createJsonObjectOrder(Comment comment, String methodRequest) {
        JsonObject joComment = new JsonObject();
        String token = userLocalStore.getLoggedInUser().getTokenId();
        if (methodRequest == ResquestConstant.PUT) {
            joComment.addProperty("comment_id", comment.getCommentId());
        }
        joComment.addProperty("tokenID", token);
        joComment.addProperty("food_id", String.valueOf(comment.getFoodId()));
        joComment.addProperty("content", comment.getComment());
        return joComment;
    }


}
