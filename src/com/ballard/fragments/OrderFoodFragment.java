package com.ballard.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ballard.activities.R;
import com.ballard.adapter.FoodSelectAdapter;
import com.ballard.businesses.MyFoodOrder;
import com.ballard.constants.AppConstant;
import com.ballard.constants.CommonConstant;
import com.ballard.constants.ResquestConstant;
import com.ballard.database.DatabaseHelper;
import com.ballard.interfaces.IOrder;
import com.ballard.localstore.AppStore;
import com.ballard.localstore.UserLocalStore;
import com.ballard.models.Food;
import com.ballard.models.PriceQuantity;
import com.ballard.models.Service;
import com.ballard.models.Tax;
import com.ballard.requests.RequestAsyncTask;
import com.ballard.responses.AsyncResponse;
import com.ballard.utilities.RestaurantCache;
import com.ballard.utilities.StringUtil;
import com.ballard.utilities.Utility;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.NumberFormat;
import java.util.Locale;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderFoodFragment extends Fragment {
    //public class OrderFoodFragment extends DialogFragment {

    private ListView listSelectedView;
    private FoodSelectAdapter adapter;
    private int currentIndex;
    private EditText txtQuantity;
    TextView lbTotalWithoutTax, lbTax, lbService, lbTotalIncludeTax;
    private UserLocalStore userLocalStore;

    public OrderFoodFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_order_food, container, false);
        //getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        userLocalStore = new UserLocalStore(getActivity());

        listSelectedView = (ListView) rootView.findViewById(R.id.list_order_foods);

        lbTotalWithoutTax = (TextView) rootView.findViewById(R.id.lbTotalWithoutTax);
        lbTax = (TextView) rootView.findViewById(R.id.lbTax);
        lbService = (TextView) rootView.findViewById(R.id.lbService);
        lbTotalIncludeTax = (TextView) rootView.findViewById(R.id.lbTotalIncludeTax);
        adapter = new FoodSelectAdapter(getActivity(), MyFoodOrder.getInstance().getMyFoodOrder(), this);
        listSelectedView.setAdapter(adapter);
        UpdateTotal();

        final GestureDetector gestureDetector = new GestureDetector(getActivity(),
                new MyGestureDetector());

        listSelectedView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });

        Button bntSubmitOrder = (Button) rootView.findViewById(R.id.bntSubmitOrder);
        bntSubmitOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JsonObject postData = createJsonObjectOrder();
                IOrder service = IOrder.retrofit.create(IOrder.class);
                Call<JsonObject> call = service.postOrder(postData);
                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        JsonObject jsonResponse = response.body();
                        try {
                            Log.i("Print Order:",jsonResponse.toString());
                            JSONObject objects = (JSONObject) new JSONTokener(jsonResponse.toString()).nextValue();
                            JSONArray arrayOrders = objects.getJSONArray(AppConstant.Common.DATA_KEY.toString());
                            JSONObject objectOrder = arrayOrders.getJSONObject(0);
                            JSONArray arrayOrderItems = objectOrder.getJSONArray(AppConstant.Common.ORDER_ITEMS_KEY.toString());
                            JSONArray arrayOrderServices = objectOrder.getJSONArray(AppConstant.Common.ORDER_SERVICES_KEY.toString());
                            JSONArray arrayOrderTax = objectOrder.getJSONArray(AppConstant.Common.ORDER_TAX_KEY.toString());
                            //int orderId = objectOrder.getInt(AppConstant.Order.ORDER_ID_KEY.toString());
                            int result = objects.getInt(AppConstant.Common.RESULT_KEY.toString());

                            if (result == 0) {
                                DatabaseHelper helper = new DatabaseHelper(getActivity());
                                SQLiteDatabase db = helper.getWritableDatabase();
                                if(userLocalStore.getLoggedInUser().isMultipleOrder()){
                                    //Waiter
                                } else {
                                    //Guest
                                    JSONObject orderSucess = MyFoodOrder.getInstance().getSuccessOrder();
                                    if(orderSucess != null){
                                        try{
                                            int orderId = orderSucess.getInt(AppConstant.Order.ORDER_ID_KEY.toString());
                                            String sql = StringUtil.format("DELETE FROM {0} WHERE {1}={2}", AppConstant.Histories.HISTORIES_TABLE_NAME.toString(), AppConstant.Order.ORDER_ID_KEY.toString(), String.valueOf(orderId));
                                            db.execSQL(sql);
                                            sql = StringUtil.format("DELETE FROM {0} WHERE {1}={2}", AppConstant.Histories.ORDER_ITEMS_TABLE_NAME.toString(), AppConstant.Order.ORDER_ID_KEY.toString(), String.valueOf(orderId));
                                            db.execSQL(sql);
                                            sql = StringUtil.format("DELETE FROM {0} WHERE {1}={2}", AppConstant.Histories.ORDER_SERVICES_TABLE_NAME.toString(), AppConstant.Order.ORDER_ID_KEY.toString(), String.valueOf(orderId));
                                            db.execSQL(sql);
                                            sql = StringUtil.format("DELETE FROM {0} WHERE {1}={2}", AppConstant.Histories.ORDER_TAX_TABLE_NAME.toString(), AppConstant.Order.ORDER_ID_KEY.toString(), String.valueOf(orderId));
                                            db.execSQL(sql);
                                        } catch (JSONException ex){
                                        }
                                    }
                                }
                                //Save history
                                ContentValues values = new ContentValues();
                                values.put(AppConstant.Order.ORDER_ID_KEY.toString(), objectOrder.getInt(AppConstant.Order.ORDER_ID_KEY.toString()));
                                values.put(AppConstant.Restaurant.RESTAURANT_ID_KEY.toString(), objectOrder.getInt(AppConstant.Restaurant.RESTAURANT_ID_KEY.toString()));
                                //values.put(AppConstant.Order.USER_ID_KEY.toString(), objectOrder.getInt(AppConstant.Order.USER_ID_KEY.toString()));
                                values.put(AppConstant.Order.USER_ID_KEY.toString(), 16);
                                values.put(AppConstant.Order.TABLE_ID_KEY.toString(), objectOrder.getInt(AppConstant.Order.TABLE_KEY.toString()));
                                values.put(AppConstant.Order.DATETIME_ORDER_KEY.toString(), objectOrder.getLong(AppConstant.Order.DATETIME_ORDER_KEY.toString()));
                                values.put(AppConstant.Order.STATUS_KEY.toString(), objectOrder.getInt(AppConstant.Order.STATUS_KEY.toString()));
                                values.put(AppConstant.Order.AMOUNT_KEY.toString(), objectOrder.getDouble(AppConstant.Order.AMOUNT_KEY.toString()));
                                values.put(AppConstant.Order.SERVICE_AMOUNT_KEY.toString(), objectOrder.getDouble(AppConstant.Order.SERVICE_AMOUNT_KEY.toString()));
                                values.put(AppConstant.Order.TAX_AMOUNT_KEY.toString(), objectOrder.getDouble(AppConstant.Order.TAX_AMOUNT_KEY.toString()));
                                values.put(AppConstant.Order.TOTAL_AMOUNT_KEY.toString(), objectOrder.getDouble(AppConstant.Order.TOTAL_AMOUNT_KEY.toString()));
                                db.insert(AppConstant.Histories.HISTORIES_TABLE_NAME.toString(), null, values);
                                //Save order items table
                                for (int i = 0; i < arrayOrderItems.length(); i++) {
                                    JSONObject orderItemObject = arrayOrderItems.getJSONObject(i);
                                    values = new ContentValues();
                                    values.put(AppConstant.OrderItems.ID_KEY.toString(), orderItemObject.getInt(AppConstant.OrderItems.ID_KEY.toString()));
                                    values.put(AppConstant.OrderItems.ORDER_ID_KEY.toString(), orderItemObject.getInt(AppConstant.OrderItems.ORDER_ID_KEY.toString()));
                                    values.put(AppConstant.OrderItems.FOOD_ID_KEY.toString(), orderItemObject.getInt(AppConstant.OrderItems.FOOD_ID_KEY.toString()));
                                    values.put(AppConstant.OrderItems.FOOD_NAME_KEY.toString(), orderItemObject.getString(AppConstant.OrderItems.FOOD_NAME_KEY.toString()));
                                    values.put(AppConstant.OrderItems.QUANTITY_KEY.toString(), orderItemObject.getDouble(AppConstant.OrderItems.QUANTITY_KEY.toString()));
                                    values.put(AppConstant.OrderItems.NOTES_KEY.toString(), orderItemObject.getString(AppConstant.OrderItems.NOTES_KEY.toString()));
                                    values.put(AppConstant.OrderItems.STATUS_KEY.toString(), orderItemObject.getInt(AppConstant.OrderItems.STATUS_KEY.toString()));
                                    values.put(AppConstant.OrderItems.SIZE_KEY.toString(), orderItemObject.getString(AppConstant.OrderItems.SIZE_KEY.toString()));
                                    values.put(AppConstant.OrderItems.TV_STATUS_KEY.toString(), orderItemObject.getInt(AppConstant.OrderItems.TV_STATUS_KEY.toString()));
                                    values.put(AppConstant.OrderItems.LAST_UPDATE_KEY.toString(), orderItemObject.getLong(AppConstant.OrderItems.LAST_UPDATE_KEY.toString()));
                                    values.put(AppConstant.OrderItems.CANCELED_NOTE_KEY.toString(), orderItemObject.getString(AppConstant.OrderItems.CANCELED_NOTE_KEY.toString()));
                                    values.put(AppConstant.OrderItems.REJECT_NOTE_KEY.toString(), orderItemObject.getString(AppConstant.OrderItems.REJECT_NOTE_KEY.toString()));
                                    values.put(AppConstant.OrderItems.RETURN_NOTE_KEY.toString(), orderItemObject.getString(AppConstant.OrderItems.RETURN_NOTE_KEY.toString()));
                                    db.insert(AppConstant.Histories.ORDER_ITEMS_TABLE_NAME.toString(), null, values);
                                }

                                //Save order services table
                                for (int i = 0; i < arrayOrderServices.length(); i++) {
                                    JSONObject orderServicesObject = arrayOrderServices.getJSONObject(i);
                                    values = new ContentValues();
                                    values.put(AppConstant.OrderServices.SERVICE_ID_KEY.toString(), orderServicesObject.getInt(AppConstant.OrderServices.SERVICE_ID_KEY.toString()));
                                    values.put(AppConstant.OrderServices.ORDER_ID_KEY.toString(), orderServicesObject.getInt(AppConstant.OrderServices.ORDER_ID_KEY.toString()));
                                    values.put(AppConstant.OrderServices.SERVICE_NAME_KEY.toString(), orderServicesObject.getString(AppConstant.OrderServices.SERVICE_NAME_KEY.toString()));
                                    values.put(AppConstant.OrderServices.AMOUNT_KEY.toString(), orderServicesObject.getDouble(AppConstant.OrderServices.AMOUNT_KEY.toString()));
                                    db.insert(AppConstant.Histories.ORDER_SERVICES_TABLE_NAME.toString(), null, values);
                                }
                                //Save order tax table
                                for (int i = 0; i < arrayOrderTax.length(); i++) {
                                    JSONObject orderTaxObject = arrayOrderTax.getJSONObject(i);
                                    values = new ContentValues();
                                    values.put(AppConstant.OrderTax.TAX_ID_KEY.toString(), orderTaxObject.getInt(AppConstant.OrderTax.TAX_ID_KEY.toString()));
                                    values.put(AppConstant.OrderTax.ORDER_ID_KEY.toString(), orderTaxObject.getInt(AppConstant.OrderTax.ORDER_ID_KEY.toString()));
                                    values.put(AppConstant.OrderTax.TAX_NAME_KEY.toString(), orderTaxObject.getString(AppConstant.OrderTax.TAX_NAME_KEY.toString()));
                                    values.put(AppConstant.OrderTax.AMOUNT_KEY.toString(), orderTaxObject.getDouble(AppConstant.OrderTax.AMOUNT_KEY.toString()));
                                    db.insert(AppConstant.Histories.ORDER_TAX_TABLE_NAME.toString(), null, values);
                                }
                                db.close();
                                //Reset Quantity food
                                MyFoodOrder.getInstance().getMyFoodOrder();
                                MyFoodOrder.getInstance().setSuccessOrder(objectOrder);
                                MyFoodOrder.getInstance().clearMyOrder();
                                if (MyFoodOrder.getInstance().getNotifyCart() != null) {
                                    long quantities = (long) MyFoodOrder.getInstance().getTotalQuantities();
                                    String strQuantities = "\t\t" + String.valueOf(quantities);
                                    MyFoodOrder.getInstance().getNotifyCart().setText(strQuantities);
                                }
                                SuccessOrderFragment successOrderFragment = new SuccessOrderFragment();
                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.replace(R.id.frame_container, successOrderFragment);
                                ft.addToBackStack(OrderFoodFragment.class.getName());
                                ft.commit();
                            } else {
                                Toast.makeText(getActivity(), getString(R.string.no_data_message), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException ex) {
                            Toast.makeText(getActivity(), getString(R.string.no_data_message), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        Log.d("Error", t.getMessage());
                    }
                });

            }
        });
        Button bntCancelOrder = (Button) rootView.findViewById(R.id.bntCancelOrder);
        bntCancelOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyFoodOrder.getInstance().clearMyOrder();
                if (getFragmentManager().getBackStackEntryCount() > 0) {
                    getFragmentManager().popBackStack(CommonConstant.TOP_MENU_SCREEN, 0);
                }
                if (MyFoodOrder.getInstance().getNotifyCart() != null) {
                    long quantities = (long) MyFoodOrder.getInstance().getTotalQuantities();
                    String strQuantities = "\t\t" + String.valueOf(quantities);
                    MyFoodOrder.getInstance().getNotifyCart().setText(strQuantities);
                }
            }
        });
        Button bntBackOrder = (Button) rootView.findViewById(R.id.bntBackOrder);
        bntBackOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dismiss();
                //if(getFragmentManager().findFragmentByTag("MAIN_MENU") != null){
                int count = getFragmentManager().getBackStackEntryCount();
                Log.i("Count:", String.valueOf(count));
                if (getFragmentManager().getBackStackEntryCount() > 0) {
                    //getFragmentManager().popBackStack();
                    getFragmentManager().popBackStack(CommonConstant.TOP_MENU_SCREEN, 0);
                }


                //
                //}

            }
        });

        return rootView;
    }

    private JsonObject createJsonObjectOrder() {
        JsonObject joOrder = new JsonObject();
        try {
            JsonArray jaFoodOrder = new JsonArray();
            for (Food foodOrder : MyFoodOrder.getInstance().getMyFoodOrder()) {
                for (PriceQuantity priceQuantity : foodOrder.getPriceQuantities()) {
                    if (priceQuantity.getQuantity() > 0) {
                        JsonObject joItemOrder = new JsonObject();
                        joItemOrder.addProperty("food_id", String.valueOf(foodOrder.getFoodId()));
                        joItemOrder.addProperty("food_name", URLEncoder.encode(foodOrder.getFoodName(), "UTF-8"));
                        joItemOrder.addProperty("quantity", String.valueOf(priceQuantity.getQuantity()));
                        joItemOrder.addProperty("notes", URLEncoder.encode(foodOrder.getRequestNote(), "UTF-8"));
                        joItemOrder.addProperty("size", URLEncoder.encode(priceQuantity.getPriceName(), "UTF-8"));
                        //jaFoodOrder.put(joItemOrder);
                        jaFoodOrder.add(joItemOrder);
                    }
                }
            }
            String token = userLocalStore.getLoggedInUser().getTokenId();
            if(userLocalStore.getLoggedInUser().isMultipleOrder()){
                //Waiter
            } else {
                //Guest
                JSONObject orderSucess = MyFoodOrder.getInstance().getSuccessOrder();
                if(orderSucess != null){
                    try{
                        int orderId = orderSucess.getInt(AppConstant.Order.ORDER_ID_KEY.toString());
                        joOrder.addProperty(AppConstant.Orders.ID_KEY.toString(), orderId);
                        joOrder.addProperty(AppConstant.Orders.IS_PRE_ORDER_KEY.toString(), 1);
                    } catch (JSONException ex){
                    }
                }
            }
            joOrder.addProperty(AppConstant.Common.TOKEN_ID_KEY.toString(), token);
            joOrder.addProperty(AppConstant.Restaurant.RESTAURANT_ID_KEY.toString(), String.valueOf(RestaurantCache.GetInstance().get().getId()));
            joOrder.addProperty("table_id", "1");
            joOrder.addProperty("table_name", URLEncoder.encode("Table1", "UTF-8"));
            joOrder.addProperty(AppConstant.Order.USER_ID_KEY.toString(), String.valueOf(userLocalStore.getLoggedInUser().getUserId()));
            //joOrder.addProperty(AppConstant.Foods.FOODS_KEY.toString(), jaFoodOrder);
            joOrder.add(AppConstant.Foods.FOODS_KEY.toString(), jaFoodOrder);
        } catch (JsonParseException ex) {
            Log.d("Error:", ex.getMessage());
        } catch (UnsupportedEncodingException ex) {

        }

        return joOrder;
    }

    /*private JSONObject createJsonObjectOrder() {
        JSONObject joOrder = new JSONObject();
        try {
            JSONArray jaFoodOrder = new JSONArray();
            for (Food foodOrder : MyFoodOrder.getInstance().getMyFoodOrder()) {
                for (PriceQuantity priceQuantity : foodOrder.getPriceQuantities()) {
                    if (priceQuantity.getQuantity() > 0) {
                        JSONObject joItemOrder = new JSONObject();
                        joItemOrder.put("food_id", String.valueOf(foodOrder.getFoodId()));
                        joItemOrder.put("food_name", URLEncoder.encode(foodOrder.getFoodName(), "UTF-8"));
                        joItemOrder.put("quantity", String.valueOf(priceQuantity.getQuantity()));
                        joItemOrder.put("notes", URLEncoder.encode(foodOrder.getRequestNote(), "UTF-8"));
                        joItemOrder.put("size", URLEncoder.encode(priceQuantity.getPriceName(), "UTF-8"));
                        jaFoodOrder.put(joItemOrder);
                    }
                }
            }
            String token = userLocalStore.getLoggedInUser().getTokenId();
            joOrder.put(AppConstant.Common.TOKEN_ID_KEY.toString(), token);
            joOrder.put(AppConstant.Restaurant.RESTAURANT_ID_KEY.toString(), String.valueOf(RestaurantCache.GetInstance().get().getId()));
            joOrder.put("table_id", "1");
            joOrder.put("table_name", URLEncoder.encode("Table1", "UTF-8"));
            joOrder.put(AppConstant.Order.USER_ID_KEY.toString(), String.valueOf(userLocalStore.getLoggedInUser().getUserId()));
            joOrder.put(AppConstant.Foods.FOODS_KEY.toString(), jaFoodOrder);
        } catch (JSONException ex) {
            Log.d("Error:", ex.getMessage());
        } catch (UnsupportedEncodingException ex) {

        }

        return joOrder;
    }*/


    @Override
    public void onResume() {
        super.onResume();
       /* Window window = getDialog().getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);*/
    }

    class MyGestureDetector extends GestureDetector.SimpleOnGestureListener {
        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public void onShowPress(MotionEvent e) {
            // TODO Auto-generated method stub

        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2,
                                float distanceX, float distanceY) {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public void onLongPress(MotionEvent e) {
            // TODO Auto-generated method stub

        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                               float velocityY) {
            boolean result = false;
            try {
                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD
                            && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        int pos = listSelectedView.pointToPosition((int) e1.getX(), (int) e2.getY());
                        if (diffX > 0) {
                            onSwipeRight(pos);
                        } else {
                            onSwipeLeft(pos);
                        }
                    }
                } else {

                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return result;

        }

        @Override
        public boolean onDown(MotionEvent e) {
            // TODO Auto-generated method stub
            return false;
        }

        public void onSwipeRight(int pos) {
            Toast.makeText(getActivity(),
                    "Swipe to right direction:" + pos,
                    Toast.LENGTH_SHORT).show();
            Log.e("", "onSwipeRight");
            confirmDelete(pos);
        }

        public void onSwipeLeft(int pos) {
            Log.e("", "onSwipeLeft");
            Toast.makeText(getActivity(),
                    "Swipe to left direction:" + pos,
                    Toast.LENGTH_SHORT).show();
            confirmDelete(pos);
        }

        private void confirmDelete(final int pos) {
            AlertDialog alert = new AlertDialog.Builder(getActivity()).create();
            alert.setTitle("Confirm");
            alert.setMessage("Do you want to remove this item?");
            alert.setButton(DialogInterface.BUTTON_POSITIVE,
                    "OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            MyFoodOrder.getInstance().getMyFoodOrder().remove(pos);
                            adapter.notifyDataSetChanged();
                            UpdateTotal();
                        }
                    });
            alert.setButton(DialogInterface.BUTTON_NEGATIVE,
                    "Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // dismiss();
                        }
                    });
            alert.show();
        }


    }

    public void UpdateTotal() {
        Locale locale = StringUtil.getLocale(RestaurantCache.GetInstance().get().getDefaultsList().get(0).getCurrencyCode());
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
        String totalWithoutTax = currencyFormatter.format(MyFoodOrder.getInstance().getTotalMyOrder());
        double totalService = 0;
        double totalTax = 0;
        double totalIncludeTax = 0;
        //Calculate tax
        for (Tax tax : RestaurantCache.GetInstance().get().getTaxList()) {
            if (tax.getTaxType() == AppConstant.TaxType.PERCENT.getValue()) {
                totalTax += MyFoodOrder.getInstance().getTotalMyOrder() * tax.getTaxValue() / 100;
            } else {
                //Fix value
                totalTax += tax.getTaxValue();
            }
        }
        //Calculate service
        for (Service service : RestaurantCache.GetInstance().get().getServiceList()) {
            if (service.getServiceType() == AppConstant.ServiceType.PERCENT.getValue()) {
                totalService += MyFoodOrder.getInstance().getTotalMyOrder() * service.getServiceValue() / 100;
            } else {
                //Fix value
                totalService += service.getServiceValue();
            }
        }
        totalIncludeTax = MyFoodOrder.getInstance().getTotalMyOrder() + totalTax + totalService;

        lbTotalWithoutTax.setText(StringUtil.format("{0}", totalWithoutTax));
        lbTax.setText(StringUtil.format("{0}", currencyFormatter.format(totalTax)));
        lbService.setText(StringUtil.format("{0}", currencyFormatter.format(totalService)));
        lbTotalIncludeTax.setText(StringUtil.format("{0}", currencyFormatter.format(totalIncludeTax)));
    }


   /* private void UpdateTotal(){
        Locale locale = StringUtil.getSettingLocale(getActivity().getString(R.string.default_language));
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
        String total = currencyFormatter.format(FoodOrder.getInstance().getTotalMyOrder());
        if(totalWithoutTax != null){
            totalWithoutTax.setText("Total: " + total);
        }
    }*/


}
