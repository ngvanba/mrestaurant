package com.ballard.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.ballard.activities.R;
import com.ballard.adapter.ViewOrderAdapter;
import com.ballard.businesses.MyFoodOrder;
import com.ballard.constants.AppConstant;
import com.ballard.constants.CommonConstant;
import com.ballard.localstore.UserLocalStore;
import com.ballard.models.Food;
import com.ballard.models.PriceQuantity;
import com.ballard.utilities.RestaurantCache;
import com.ballard.utilities.StringUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ViewOrderFragment extends Fragment {
    //public class ViewOrderFragment extends DialogFragment {
    private ListView listSelectedView;
    private ViewOrderAdapter adapter;
    private int currentIndex;
    private EditText txtQuantity;
    TextView lbTotalWithoutTax, lbTax, lbService, lbTotalIncludeTax;
    private UserLocalStore userLocalStore;

    public ViewOrderFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_view_order, container, false);
        //getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        userLocalStore = new UserLocalStore(getActivity());
        Button bntGoHome = (Button) rootView.findViewById(R.id.bntGoHome);
        bntGoHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getFragmentManager().getBackStackEntryCount() > 0) {
                    getFragmentManager().popBackStack(CommonConstant.TOP_MENU_SCREEN, 0);
                }
            }
        });
        listSelectedView = (ListView) rootView.findViewById(R.id.list_order_foods_view);
        lbTotalWithoutTax = (TextView) rootView.findViewById(R.id.lbTotalWithoutTax);
        lbTax = (TextView) rootView.findViewById(R.id.lbTax);
        lbService = (TextView) rootView.findViewById(R.id.lbService);
        lbTotalIncludeTax = (TextView) rootView.findViewById(R.id.lbTotalIncludeTax);
        viewOrder();

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        /*Window window = getDialog().getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);*/
    }

    private void viewOrder() {
        ArrayList<Food> foodList = new ArrayList<>();
        JSONObject orderSucess = MyFoodOrder.getInstance().getSuccessOrder();
        try {
            double amount = orderSucess.getDouble("amount");
            double serviceAmount = orderSucess.getDouble("service_amount");
            double taxAmount = orderSucess.getDouble("tax_amount");
            double totalAmount = orderSucess.getDouble("total_amount");

            Locale locale = StringUtil.getLocale(RestaurantCache.GetInstance().get().getDefaultsList().get(0).getCurrencyCode());
            NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
            //Price and Description
            String amountMoney = currencyFormatter.format(amount);
            String serviceAmountMoney = currencyFormatter.format(serviceAmount);
            String taxAmountMoney = currencyFormatter.format(taxAmount);
            String totalAmountMoney = currencyFormatter.format(totalAmount);

            lbTotalWithoutTax.setText(amountMoney);
            lbService.setText(serviceAmountMoney);
            lbTax.setText(taxAmountMoney);
            lbTotalIncludeTax.setText(totalAmountMoney);

            JSONArray foodsData = orderSucess.getJSONArray("order_items");
            for (int i = 0; i < foodsData.length(); i++) {
                JSONObject object = foodsData.getJSONObject(i);
                int foodId = object.getInt(AppConstant.Foods.FOOD_ID_KEY.toString());
                String foodName = "";
                String foodNotes = "";
                try {
                    foodName = URLDecoder.decode(object.getString(AppConstant.Foods.FOOD_NAME_KEY.toString()), "UTF-8");
                    foodNotes = URLDecoder.decode(object.getString(AppConstant.Foods.FOOD_NOTES_KEY.toString()), "UTF-8");
                } catch (UnsupportedEncodingException ex) {

                }
                //double price = object.getDouble(AppConstant.Foods.FOOD_PRICE_KEY.toString());
                int quantityOrder = object.getInt(AppConstant.Foods.FOOD_QUANTITY_KEY.toString());
                String fooSsize = object.getString(AppConstant.Foods.FOOD_SIZE_KEY.toString());

                List<PriceQuantity> listPrice = new ArrayList<>();
                PriceQuantity priceQuantity = new PriceQuantity();
                priceQuantity.setQuantity(quantityOrder);
                priceQuantity.setPrice(15);
                priceQuantity.setPriceName(fooSsize);
                listPrice.add(priceQuantity);
                Food foodOrder = new Food("", foodId, foodName, "", listPrice, 0);
                foodOrder.setPriceQuantities(listPrice);
                foodOrder.setRequestNote(foodNotes);
                foodList.add(foodOrder);
            }

        } catch (JSONException ex) {

        }

        ViewOrderAdapter adapter = new ViewOrderAdapter(getActivity(), foodList);
        listSelectedView.setAdapter(adapter);

    }

   /* private void showOrder(int orderId){
        HashMap<String, String> postData = new HashMap<String, String>();
        RequestAsyncTask featureTask = new RequestAsyncTask(getActivity(),
                new AsyncResponse() {

                    @Override
                    public void processFinish(String output) {
                        if (output.length() == 0) {
                            Toast.makeText(getActivity(), getString(R.string.no_data_message), Toast.LENGTH_LONG).show();
                        } else {
                            try {
                                JSONObject objects = (JSONObject) new JSONTokener(output).nextValue();
                                adapter = new ViewOrderAdapter(getActivity(), MyFoodOrder.getInstance().getMyFoodOrder());
                                listSelectedView.setAdapter(adapter);

                            } catch (JSONException ex) {
                                Toast.makeText(getActivity(), getString(R.string.no_data_message), Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }, postData, ResquestConstant.JSON_DATA);
        featureTask.setRequestMethod(ResquestConstant.GET);
        String restaurant =String.valueOf(RestaurantCache.GetInstance().get().getId());
        String token = userLocalStore.getLoggedInUser().getTokenId();

        String url = StringUtil.format("{0}/{1}/{2}/{3}/{4}/{5}", CommonConstant.SERVER_ADDRESS, AppConstant.Foods.FOODS_KEY.toString(), restaurant, AppConstant.Foods.FEATURE_KEY.toString(), token);
        featureTask.execute(url);
    }*/

}
