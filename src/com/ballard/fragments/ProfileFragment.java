package com.ballard.fragments;


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ballard.activities.R;
import com.ballard.constants.CommonConstant;
import com.ballard.constants.ResquestConstant;
import com.ballard.localstore.UserLocalStore;
import com.ballard.models.User;
import com.ballard.requests.RequestAsyncTask;
import com.ballard.responses.AsyncResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends BaseFragment implements View.OnClickListener {

    User user;

    public ProfileFragment() {
        // Required empty public constructor
    }

    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        UserLocalStore userLocalStore = new UserLocalStore(this.getActivity());
        user = userLocalStore.getLoggedInUser();
        rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        // Inflate the layout for this fragment
        EditText edtEmail = (EditText) rootView.findViewById(R.id.txtEmail);
        edtEmail.setText(user.getEmail());
        TextView txtChangePassword = (TextView) rootView.findViewById(R.id.txtChangePassword);
        txtChangePassword.setOnClickListener(this);
        Button button = (Button) rootView.findViewById(R.id.btnSave);
        button.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.txtChangePassword) {
            ChangePasswordFragment newFragment = new ChangePasswordFragment();
            FragmentTransaction transaction = mGuestMenuActivity.getFragmentManager().beginTransaction();
            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.frame_container, newFragment);
            transaction.addToBackStack(null);
            // Commit the transaction
            transaction.commit();
        } else {
            HashMap<String, String> postData = new HashMap<String, String>();
            postData.put(CommonConstant.TOKEN_ID, user.getTokenId());
            EditText editTextFirstName = (EditText) rootView.findViewById(R.id.txtFirstName);
            postData.put(CommonConstant.FIRST_NAME_FIELD, editTextFirstName.getText().toString());
            EditText editTextLastName = (EditText) rootView.findViewById(R.id.txtLastName);
            postData.put(CommonConstant.LAST_NAME_FIELD, editTextLastName.getText().toString());
            postData.put(CommonConstant.EMAIL_FIELD, user.getEmail());
            EditText editTextAddress1 = (EditText) rootView.findViewById(R.id.txtAddress1);
            postData.put(CommonConstant.ADDRESS1_FIELD, editTextAddress1.getText().toString());
            EditText editTextAddress2 = (EditText) rootView.findViewById(R.id.txtAddress2);
            postData.put(CommonConstant.ADDRESS2_FIELD, editTextAddress2.getText().toString());
            EditText editTextCity = (EditText) rootView.findViewById(R.id.txtCity);
            postData.put(CommonConstant.CITY_FIELD, editTextCity.getText().toString());
            EditText editTextCountry = (EditText) rootView.findViewById(R.id.txtCountry);
            postData.put(CommonConstant.COUNTRY_FIELD, editTextCountry.getText().toString());
            EditText editTextPostCode = (EditText) rootView.findViewById(R.id.txtPostCode);
            postData.put(CommonConstant.POSTCODE_FIELD, editTextPostCode.getText().toString());

            RequestAsyncTask loginTask = new RequestAsyncTask(mGuestMenuActivity,
                    new AsyncResponse() {

                        @Override
                        public void processFinish(String output) {
                            if (output.length() == 0) {

                            } else {
                                try {
                                    JSONObject objects = (JSONObject) new JSONTokener(output).nextValue();
                                    JSONArray resultData = objects.getJSONArray(CommonConstant.DATA);
                                    String message = objects.getString(CommonConstant.MESSAGE);
                                    int result = objects.getInt(CommonConstant.RESULT);
                                    //String tokenId = objects.getString(CommonConstant.TOKEN_ID);


                                    if (result == 0) {
//
                                    } else {
                                        // showDialogMessage(message);
                                    }

                                } catch (Exception ex) {
                                    // showDialogMessage(getString(R.string.invalid_account));
                                }

                            }

                        }
                    }, postData, ResquestConstant.JSON_DATA);
            loginTask.setRequestMethod(ResquestConstant.PUT);
            loginTask.execute(CommonConstant.SERVER_ADDRESS + CommonConstant.REGISTER_PAGE);
        }
    }
}
