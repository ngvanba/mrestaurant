package com.ballard.fragments;

import android.app.DialogFragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ballard.activities.R;
import com.ballard.businesses.MyFoodOrder;
import com.ballard.constants.AppConstant;
import com.ballard.constants.CommonConstant;
import com.ballard.interfaces.IFood;
import com.ballard.localstore.UserLocalStore;
import com.ballard.models.CommonResult;
import com.ballard.utilities.StringUtil;
import com.google.gson.JsonObject;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RateFoodFragmentDialog extends DialogFragment {

    private UserLocalStore userLocalStore;
    private Button bntRate1Star;
    private Button bntRate2Stars;
    private Button bntRate3Stars;
    private Button bntRate4Stars;
    private Button bntRate5Stars;

    private LinearLayout panel1Star;
    private LinearLayout panel2Stars;
    private LinearLayout panel3Stars;
    private LinearLayout panel4Stars;
    private LinearLayout panel5Stars;
    private TextView txtRateMessage;
    private int foodId;

    public RateFoodFragmentDialog() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_food_rate, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        userLocalStore = new UserLocalStore(getActivity());

        panel1Star = (LinearLayout) rootView.findViewById(R.id.panel1Star);
        panel2Stars = (LinearLayout) rootView.findViewById(R.id.panel2Stars);
        panel3Stars = (LinearLayout) rootView.findViewById(R.id.panel3Stars);
        panel4Stars = (LinearLayout) rootView.findViewById(R.id.panel4Stars);
        panel5Stars = (LinearLayout) rootView.findViewById(R.id.panel5Stars);
        txtRateMessage = (TextView) rootView.findViewById(R.id.txtRateMessage);

        foodId = MyFoodOrder.getInstance().getCurrentSelectFood().getFoodId();

        clearLinearLayoutLayout();

        ImageButton bntBackRate = (ImageButton) rootView.findViewById(R.id.bntBackRate);
        bntBackRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        bntRate1Star = (Button) rootView.findViewById(R.id.bntRate1Star);
        bntRate1Star.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearLinearLayoutLayout();
                panel1Star.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.item_food_bg_pressed));
                PostRate(foodId, 1);
            }
        });

        bntRate2Stars = (Button) rootView.findViewById(R.id.bntRate2Stars);
        bntRate2Stars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearLinearLayoutLayout();
                panel2Stars.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.item_food_bg_pressed));
                PostRate(foodId, 2);
            }
        });

        bntRate3Stars = (Button) rootView.findViewById(R.id.bntRate3Stars);
        bntRate3Stars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearLinearLayoutLayout();
                panel3Stars.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.item_food_bg_pressed));
                PostRate(foodId, 3);
            }
        });

        bntRate4Stars = (Button) rootView.findViewById(R.id.bntRate4Stars);
        bntRate4Stars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearLinearLayoutLayout();
                panel4Stars.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.item_food_bg_pressed));
                PostRate(foodId, 4);
            }
        });

        bntRate5Stars = (Button) rootView.findViewById(R.id.bntRate5Stars);
        bntRate5Stars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearLinearLayoutLayout();
                panel5Stars.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.item_food_bg_pressed));
                PostRate(foodId, 5);
            }
        });


        return rootView;
    }

    private void clearLinearLayoutLayout() {
        panel1Star.setBackgroundColor(Color.TRANSPARENT);
        panel2Stars.setBackgroundColor(Color.TRANSPARENT);
        panel3Stars.setBackgroundColor(Color.TRANSPARENT);
        panel4Stars.setBackgroundColor(Color.TRANSPARENT);
        panel5Stars.setBackgroundColor(Color.TRANSPARENT);
        txtRateMessage.setText("");
    }


    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
    }


    private void PostRate(int foodId, int starNumber) {
        JsonObject postData = createJsonObjectOrder(foodId, starNumber);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(CommonConstant.SERVER_ADDRESS_RETROFIT).client(client)
                .build();


        IFood service = retrofit.create(IFood.class);
        Call<CommonResult> call = service.postRate(postData);

        call.enqueue(new Callback<CommonResult>() {
            @Override
            //public void onResponse(Call<CommonResult> response) {
            public void onResponse(Call<CommonResult> call, Response<CommonResult> response) {

                int statusCode = response.code();
                CommonResult jsonResponse = response.body();
                int result = Integer.parseInt(jsonResponse.getResult());
                if (result == 0) {
                    String message = StringUtil.format(getString(R.string.rate_success_message), String.valueOf(starNumber));
                    txtRateMessage.setText(message);
                } else {
                    txtRateMessage.setText(getString(R.string.rate_error_message));
                }
            }

            @Override
            public void onFailure(Call<CommonResult> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }


    private JsonObject createJsonObjectOrder(int foodId, int starNumber) {
        JsonObject joRate = new JsonObject();
        String token = userLocalStore.getLoggedInUser().getTokenId();

        joRate.addProperty(AppConstant.Common.TOKEN_ID_KEY.toString(), token);
        joRate.addProperty(AppConstant.Foods.FOOD_KEY.toString(), String.valueOf(foodId));
        joRate.addProperty(AppConstant.Foods.FOOD_KEY.POINTS_KEY.toString(), String.valueOf(starNumber));

        return joRate;
    }


}
