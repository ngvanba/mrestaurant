package com.ballard.fragments;

import android.app.Fragment;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.ballard.activities.R;
import com.ballard.adapter.ViewOrderAdapter;
import com.ballard.adapter.ViewOrderDetailsAdapter;
import com.ballard.businesses.MyFoodOrder;
import com.ballard.constants.AppConstant;
import com.ballard.constants.CommonConstant;
import com.ballard.database.DatabaseHelper;
import com.ballard.localstore.UserLocalStore;
import com.ballard.models.Food;
import com.ballard.models.PriceQuantity;
import com.ballard.utilities.StringUtil;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

public class ViewOrderDetailFragment extends Fragment {
//public class ViewOrderFragment extends DialogFragment {
    private ListView listSelectedView;
    private ViewOrderAdapter adapter;
    private int currentIndex;
    private EditText txtQuantity;
    TextView lbTotalWithoutTax,lbTax,lbService,lbTotalIncludeTax;
    private UserLocalStore userLocalStore;

    public ViewOrderDetailFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_view_order_details, container, false);
        userLocalStore = new UserLocalStore(getActivity());
        //order_details
        TextView lbOrderDetailsHeader = (TextView) rootView.findViewById(R.id.lbOrderDetailsHeader);
        String header = StringUtil.format("{0} - {1}", getString(R.string.order_details), String.valueOf(MyFoodOrder.getInstance().getOrderId()));
        lbOrderDetailsHeader.setText(header);
        Button bntGoHome =(Button)rootView.findViewById(R.id.bntBack);
        bntGoHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getFragmentManager().getBackStackEntryCount() > 0){
                    getFragmentManager().popBackStack(CommonConstant.TOP_MENU_SCREEN, 0);
                }
            }
        });
        listSelectedView = (ListView) rootView.findViewById(R.id.list_order_details_view);
        viewOrder();

        return rootView;
    }


    @Override
    public void onResume()
    {
        super.onResume();
    }

    private void viewOrder(){
        ArrayList<Food> foodList = new ArrayList<>();
        int orderId = MyFoodOrder.getInstance().getOrderId();
        DatabaseHelper helper = new DatabaseHelper(getActivity());
        SQLiteDatabase db = helper.getReadableDatabase();
        String[] tableColumns = new String[]{
                AppConstant.OrderItems.ID_KEY.toString(),
                AppConstant.OrderItems.ORDER_ID_KEY.toString(),
                AppConstant.OrderItems.FOOD_ID_KEY.toString(),
                AppConstant.OrderItems.FOOD_NAME_KEY.toString(),
                AppConstant.OrderItems.QUANTITY_KEY.toString(),
                AppConstant.OrderItems.NOTES_KEY.toString(),
                AppConstant.OrderItems.STATUS_KEY.toString(),
                AppConstant.OrderItems.SIZE_KEY.toString(),
                AppConstant.OrderItems.TV_STATUS_KEY.toString(),
                AppConstant.OrderItems.LAST_UPDATE_KEY.toString(),
                AppConstant.OrderItems.CANCELED_NOTE_KEY.toString(),
                AppConstant.OrderItems.REJECT_NOTE_KEY.toString(),
                AppConstant.OrderItems.RETURN_NOTE_KEY.toString()
        };

        //String whereClause = "1 = 1";
        String whereClause = StringUtil.format("{0} = {1}", AppConstant.OrderItems.ORDER_ID_KEY.toString(), String.valueOf(orderId));
        Cursor cursor = db.query(AppConstant.Histories.ORDER_ITEMS_TABLE_NAME.toString(), tableColumns, whereClause, null, null, null, AppConstant.Order.ORDER_ID_KEY.toString() +" DESC", null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            int foodId = cursor.getInt(2);
            String foodName = "";
            String foodNotes = "";
            try {
                foodName = URLDecoder.decode(cursor.getString(3), "UTF-8");
                foodNotes = URLDecoder.decode(cursor.getString(5), "UTF-8");
            } catch (UnsupportedEncodingException ex){
            }
            int quantityOrder = cursor.getInt(4);
            String footSize = cursor.getString(7);

            List<PriceQuantity> listPrice = new ArrayList<>();
            PriceQuantity priceQuantity = new PriceQuantity();
            priceQuantity.setQuantity(quantityOrder);
            priceQuantity.setPrice(15);
            priceQuantity.setPriceName(footSize);
            listPrice.add(priceQuantity);
            Food foodOrder = new Food("",foodId,foodName,"",listPrice,0);
            foodOrder.setPriceQuantities(listPrice);
            foodOrder.setRequestNote(foodNotes);
            foodList.add(foodOrder);
            cursor.moveToNext();
        }
        db.close();
        ViewOrderDetailsAdapter adapter = new ViewOrderDetailsAdapter(getActivity(), foodList, getFragmentManager());
        listSelectedView.setAdapter(adapter);

    }
}
