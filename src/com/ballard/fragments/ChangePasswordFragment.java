package com.ballard.fragments;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ballard.activities.R;
import com.ballard.constants.CommonConstant;
import com.ballard.interfaces.IApp;
import com.ballard.localstore.UserLocalStore;
import com.ballard.models.User;
import com.google.gson.JsonObject;

import org.json.JSONObject;
import org.json.JSONTokener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePasswordFragment extends BaseFragment implements View.OnClickListener {

    // User object
    User user;

    // Root view
    private View rootView;

    private EditText editTextOldPassword, editTextNewPassword1, editTextNewPassword2;

    private Button btnChangePwd;

    private ProgressDialog progressDialog;

    // Fragment constructor
    public ChangePasswordFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_change_password, container, false);
        UserLocalStore userLocalStore = new UserLocalStore(mGuestMenuActivity);
        user = userLocalStore.getLoggedInUser();
        editTextOldPassword = (EditText) rootView.findViewById(R.id.txtCurrentPassword);
        editTextNewPassword1 = (EditText) rootView.findViewById(R.id.txtNewPassword1);
        editTextNewPassword2 = (EditText) rootView.findViewById(R.id.txtNewPassword2);
        btnChangePwd = (Button) rootView.findViewById(R.id.btnSave);
        btnChangePwd.setOnClickListener(this);
        return rootView;
    }

    /**
     * Create JsonObject, send it to server for changing pwd
     *
     * @param currentPwd
     * @param newPwd
     * @return JsonObject
     */
    private JsonObject createPwdObject(String currentPwd, String newPwd) {
        JsonObject postData = new JsonObject();
        // user.getTokenId is weak object data. Need to check.
        postData.addProperty(CommonConstant.TOKEN_ID, user.getTokenId());
        postData.addProperty(CommonConstant.CURRENT_PASSWORD_FIELD, currentPwd);
        postData.addProperty(CommonConstant.NEW_PASSWORD_FIELD, newPwd);
        return postData;
    }

    /**
     * Showing progress bar
     */
    private void showProgressBar() {
        progressDialog = ProgressDialog.show(mGuestMenuActivity, null, null);
        progressDialog.setContentView(R.layout.progressbar);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.show();
    }

    /**
     * Dismiss progress bar
     */
    private void dismissProgressBar() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        // Perform action on click
        if (editTextNewPassword1.getText().toString().equals(editTextNewPassword2.getText().toString())) {
            showProgressBar();
            IApp service = IApp.retrofit.create(IApp.class);
            Call<JsonObject> call = service.changePwd(createPwdObject(editTextOldPassword.getText().toString(), editTextNewPassword1.getText().toString()));

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    // Get jsonObject from response
                    JsonObject jsonResponse = response.body();
                    Log.i(CommonConstant.TAG, "onResponse: " + jsonResponse.toString());
                    try {
                        JSONObject objects = (JSONObject) new JSONTokener(jsonResponse.toString()).nextValue();
                        String message = objects.getString(CommonConstant.MESSAGE);
                        int result = objects.getInt(CommonConstant.RESULT);
                        if (result == 0) {
                            dismissProgressBar();
                            getFragmentManager().popBackStack();
                            Toast.makeText(mGuestMenuActivity, "Password updated successfully.",
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(mGuestMenuActivity, message,
                                    Toast.LENGTH_LONG).show();
                            // showDialogMessage(message);
                        }
                    } catch (Exception ex) {
                        Log.e(CommonConstant.TAG, "Exception: " + ex.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    dismissProgressBar();
                    Log.d("Error", t.getMessage());
                }
            });
        } else {
            Toast.makeText(getActivity(), "Password does not match the confirm password.",
                    Toast.LENGTH_LONG).show();
        }
    }
}
