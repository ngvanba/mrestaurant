package com.ballard.fragments;


import android.app.Fragment;
import android.os.Bundle;

import com.ballard.activities.GuestMenuActivity;
import com.ballard.activities.LoginActivity;

/**
 * Created by hotaduc on 6/26/16.
 */
public class BaseFragment extends Fragment {

    public static GuestMenuActivity mGuestMenuActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGuestMenuActivity = (GuestMenuActivity) this.getActivity();
    }
}
