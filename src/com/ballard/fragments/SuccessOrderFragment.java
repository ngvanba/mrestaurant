package com.ballard.fragments;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ballard.activities.R;
import com.ballard.adapter.ViewOrderAdapter;
import com.ballard.businesses.MyFoodOrder;
import com.ballard.constants.AppConstant;
import com.ballard.constants.CommonConstant;
import com.ballard.constants.ResquestConstant;
import com.ballard.localstore.UserLocalStore;
import com.ballard.requests.RequestAsyncTask;
import com.ballard.responses.AsyncResponse;
import com.ballard.utilities.RestaurantCache;
import com.ballard.utilities.StringUtil;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.HashMap;

public class SuccessOrderFragment extends Fragment {
//public class SuccessOrderFragment extends DialogFragment {

    private ViewOrderAdapter adapter;
    private int currentIndex;
    private EditText txtQuantity;
    TextView lbTotalWithoutTax,lbTax,lbService,lbTotalIncludeTax;
    private UserLocalStore userLocalStore;

    public SuccessOrderFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_success_order, container, false);
        //getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        userLocalStore = new UserLocalStore(getActivity());
        TextView lbOrderId = (TextView) rootView.findViewById(R.id.lbOrderId);
        Button bntViewCurrentOrder = (Button) rootView.findViewById(R.id.bntViewCurrentOrder);
        Button bntPlaceAnotherOrder = (Button) rootView.findViewById(R.id.bntPlaceAnotherOrder);
        bntPlaceAnotherOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dismiss();
                int count  = getFragmentManager().getBackStackEntryCount();
                Log.i("Counts:", String.valueOf(count));
                if(getFragmentManager().getBackStackEntryCount() > 0){
                    getFragmentManager().popBackStack(CommonConstant.TOP_MENU_SCREEN, 0);
                }
            }
        });
        bntViewCurrentOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewOrderFragment viewOrderFragment = new ViewOrderFragment();
                /*FragmentManager fragmentManager = getFragmentManager();
                viewOrderFragment.show(fragmentManager, "");*/
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.frame_container, viewOrderFragment);
                ft.addToBackStack(ViewOrderFragment.class.getName());
                ft.commit();
            }
        });
        JSONObject successOrder = MyFoodOrder.getInstance().getSuccessOrder();
        try {
            if(successOrder != null){
                int orderId = successOrder.getInt(AppConstant.Orders.ORDER_ID_KEY.toString());
                String orderNo = StringUtil.format("{0}: {1}",getString(R.string.order_no),String.valueOf(orderId));
                lbOrderId.setText(orderNo);
            }
        } catch (JSONException ex){
            ex.printStackTrace();
        }
        return rootView;
    }


    @Override
    public void onResume()
    {
        super.onResume();
        /*Window window = getDialog().getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);*/
    }


}
