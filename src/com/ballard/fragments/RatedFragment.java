package com.ballard.fragments;


import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ballard.activities.GuestMenuActivity;
import com.ballard.activities.R;
import com.ballard.adapter.FoodListAdapter;
import com.ballard.constants.AppConstant;
import com.ballard.constants.CommonConstant;
import com.ballard.constants.ResquestConstant;
import com.ballard.customs.MyGestureDetectorMenuSlide;
import com.ballard.interfaces.IFood;
import com.ballard.localstore.AppStore;
import com.ballard.localstore.UserLocalStore;
import com.ballard.models.Comment;
import com.ballard.models.Food;
import com.ballard.requests.RequestAsyncTask;
import com.ballard.responses.AsyncResponse;
import com.ballard.utilities.RestaurantCache;
import com.ballard.utilities.StringUtil;
import com.ballard.utilities.Utility;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class RatedFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private TextView txtHomeLabel;
    private Button bnRateOrder;
    ArrayList<Food> listFoodsServer;
    ArrayList<Food> loadingFoodsPagingUI;
    ArrayList<String> downloadContentPage;
    FoodListAdapter adapter;
    int countPagingServer = 0;
    int pageCount = 1;
    int startingIndex;
    int endingIndex;
    int refreshPage = 0;
    private UserLocalStore userLocalStore;
    private ListView listView;
    private GuestMenuActivity menuActivity;
    private SwipeRefreshLayout swipeRefreshLayout;

    public RatedFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_food_view, container, false);
        userLocalStore = new UserLocalStore(getActivity());
        downloadContentPage = new ArrayList<>();

        /*bnRateOrder = (Button) rootView.findViewById(R.id.bnRateOrder);
        bnRateOrder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                *//*OrderFoodFragment orderFoodDialog = new OrderFoodFragment();
                FragmentManager fragmentManager = getFragmentManager();
                orderFoodDialog.show(fragmentManager, "");*//*
            }
        });*/
        listView = (ListView) rootView.findViewById(R.id.list_foods_view);
        listFoodsServer = new ArrayList<Food>();
        loadingFoodsPagingUI = new ArrayList<Food>();
        FragmentManager fragmentManager = getFragmentManager();
        adapter = new FoodListAdapter(getActivity(), loadingFoodsPagingUI, fragmentManager);
        listView.setAdapter(adapter);
        //GetFoods(countPagingServer);

        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        listFoodsServer.clear();
                                        loadingFoodsPagingUI.clear();
                                        pageCount = 1;
                                        adapter.notifyDataSetChanged();
                                        GetFoods(countPagingServer);
                                    }
                                }
        );

        // Implementing scroll refresh
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            int threshold = 1;
            int count = listView.getCount();

            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollStatus) {
                if (scrollStatus == SCROLL_STATE_IDLE) {
                    if (listView.getLastVisiblePosition() >= count - threshold && pageCount <= determinePagingUI()) {
                        startingIndex = CommonConstant.UI_PAGE_SIZE * (pageCount - 1);
                        if (startingIndex < 0) {
                            startingIndex = 0;
                        }
                        endingIndex = startingIndex + CommonConstant.UI_PAGE_SIZE;
                        if (endingIndex > listFoodsServer.size()) {
                            endingIndex = listFoodsServer.size();
                        }
                        for (int i = startingIndex; i < endingIndex; i++) {
                            loadingFoodsPagingUI.add(listFoodsServer.get(i));
                        }
                        pageCount++;
                        adapter.notifyDataSetChanged();
                        if (loadingFoodsPagingUI.size() >= listFoodsServer.size() - CommonConstant.UI_PAGE_SIZE) {
                            countPagingServer++;
                            GetFoods(countPagingServer);
                        }
                    }
                }
            }

            @Override
            public void onScroll(AbsListView absListView, int firstItem, int visibleItemCount, final int totalItems) {
            }
        });

        /*listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CurrentSelectFoodDialog currentSelectFoodDialog = new CurrentSelectFoodDialog();
                FragmentManager fragmentManager = getFragmentManager();
                currentSelectFoodDialog.show(fragmentManager, "");
                Food selectFood = listFoodsServer.get(position);
                MyFoodOrder.getInstance().setCurrentFood(selectFood);

            }
        });*/

        final GestureDetector gestureDetector = new GestureDetector(getActivity(),
                new MyGestureDetectorMenuSlide(listView));

        listView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });


        return rootView;
    }

    private int determinePagingUI() {
        int determinePages;
        if (listFoodsServer.size() % CommonConstant.UI_PAGE_SIZE == 0) {
            determinePages = listFoodsServer.size() / CommonConstant.UI_PAGE_SIZE;
        } else {
            determinePages = (listFoodsServer.size() / CommonConstant.UI_PAGE_SIZE) + 1;
        }
        return determinePages;
    }

    protected void GetFoods(int page) {
        swipeRefreshLayout.setRefreshing(true);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(CommonConstant.SERVER_ADDRESS_RETROFIT).client(client)
                .build();

        String token = userLocalStore.getLoggedInUser().getTokenId();
        int restaurantId = RestaurantCache.GetInstance().get().getId();
        IFood service = retrofit.create(IFood.class);
        Call<JsonObject> call = service.getFoodPage(restaurantId, AppConstant.Foods.RATED_KEY.toString(), page, token);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject jsonResponse = response.body();
                try {
                    JSONObject objects = (JSONObject) new JSONTokener(jsonResponse.toString()).nextValue();
                    JSONObject objectData = objects.getJSONObject(AppConstant.Common.DATA_KEY.toString());
                    JSONObject objectTopRateData = objectData.getJSONObject(AppConstant.Foods.TOP_RATED_KEY.toString());
                    JSONArray foodsData = objectTopRateData.getJSONArray(AppConstant.Foods.FOODS_KEY.toString());
                    JSONArray pricesData = objectTopRateData.getJSONArray(AppConstant.Foods.FOOD_PRICES_KEY.toString());
                    for (int i = 0; i < foodsData.length(); i++) {
                        JSONObject object = foodsData.getJSONObject(i);
                        int foodId = object.getInt(AppConstant.Foods.FOOD_ID_KEY.toString());
                        String foodName = object.getString(AppConstant.Foods.FOOD_NAME_KEY.toString());
                        double rated = object.getDouble(AppConstant.Foods.RATED_KEY.toString());
                        int liked = object.getInt(AppConstant.Foods.LIKED_KEY.toString());
                        int commentCount = object.getInt(AppConstant.Foods.COMMENT_COUNT_KEY.toString());
                        //double foodPrice = object.getDouble("food_price");
                        String foodDescription = object.getString(AppConstant.Foods.FOOD_DESCRIPTION_KEY.toString());

                        //Process get image
                        String foodPhotoName = object.getString(AppConstant.Foods.FOOD_PHOTO_KEY.toString());
                        int photoProxyId = object.getInt(AppConstant.Foods.FOOD_PHOTO_URL_KEY.toString());
                        int photoProcessed = object.getInt(AppConstant.Foods.PHOTO_PROCESSED_KEY.toString());
                        String photoProxyUrl = AppStore.GetInstance().getPhotoUrl(photoProxyId);
                        String fullPhotoPath = "";
                        if (photoProcessed == CommonConstant.PROCESSED) {
                            fullPhotoPath = StringUtil.format("{0}/{1}", photoProxyUrl, Utility.getFileNameBySize(foodPhotoName, "200x200"));
                        } else {
                            fullPhotoPath = StringUtil.format("{0}/{1}", photoProxyUrl, foodPhotoName);
                        }

                        Food food = new Food(fullPhotoPath, foodId, foodName, foodDescription, Utility.getPrices(foodId, pricesData), 1);
                        food.setRated(rated);
                        food.setLiked(liked);
                        food.setCommentCount(commentCount);
                        listFoodsServer.add(food);
                    }

                    startingIndex = CommonConstant.UI_PAGE_SIZE * (pageCount - 1);
                    if (startingIndex < 0) {
                        startingIndex = 0;
                    }
                    endingIndex = startingIndex + CommonConstant.UI_PAGE_SIZE;
                    if (endingIndex > listFoodsServer.size()) {
                        endingIndex = listFoodsServer.size();
                    }
                    for (int i = startingIndex; i < endingIndex; i++) {
                        loadingFoodsPagingUI.add(listFoodsServer.get(i));
                    }
                    pageCount++;
                    adapter.notifyDataSetChanged();
                    swipeRefreshLayout.setRefreshing(false);
                } catch (JSONException ex) {
                    swipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(getActivity(), getString(R.string.no_data_message), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                Log.d("Error", t.getMessage());
            }
        });
    }
    /*protected void GetFoods(int page) {
        swipeRefreshLayout.setRefreshing(true);
        HashMap<String, String> postData = new HashMap<String, String>();
        RequestAsyncTask ratedTask = new RequestAsyncTask(getActivity(),
                new AsyncResponse() {

                    @Override
                    public void processFinish(String output) {
                        if (output.length() == 0) {
                            swipeRefreshLayout.setRefreshing(false);
                            Toast.makeText(getActivity(), getString(R.string.no_data_message), Toast.LENGTH_LONG).show();
                        } else {
                            try {
                                JSONObject objects = (JSONObject) new JSONTokener(output).nextValue();
                                JSONObject objectData = objects.getJSONObject(AppConstant.Common.DATA_KEY.toString());
                                JSONObject objectTopRateData = objectData.getJSONObject(AppConstant.Foods.TOP_RATED_KEY.toString());
                                JSONArray foodsData = objectTopRateData.getJSONArray(AppConstant.Foods.FOODS_KEY.toString());
                                JSONArray pricesData = objectTopRateData.getJSONArray(AppConstant.Foods.FOOD_PRICES_KEY.toString());
                                for (int i = 0; i < foodsData.length(); i++) {
                                    JSONObject object = foodsData.getJSONObject(i);
                                    int foodId = object.getInt(AppConstant.Foods.FOOD_ID_KEY.toString());
                                    String foodName = object.getString(AppConstant.Foods.FOOD_NAME_KEY.toString());
                                    double rated = object.getDouble(AppConstant.Foods.RATED_KEY.toString());
                                    int liked = object.getInt(AppConstant.Foods.LIKED_KEY.toString());
                                    int commentCount = object.getInt(AppConstant.Foods.COMMENT_COUNT_KEY.toString());
                                    //double foodPrice = object.getDouble("food_price");
                                    String foodDescription = object.getString(AppConstant.Foods.FOOD_DESCRIPTION_KEY.toString());

                                    //Process get image
                                    String foodPhotoName = object.getString(AppConstant.Foods.FOOD_PHOTO_KEY.toString());
                                    int photoProxyId = object.getInt(AppConstant.Foods.FOOD_PHOTO_URL_KEY.toString());
                                    int photoProcessed = object.getInt(AppConstant.Foods.PHOTO_PROCESSED_KEY.toString());
                                    String photoProxyUrl = AppStore.GetInstance().getPhotoUrl(photoProxyId);
                                    String fullPhotoPath = "";
                                    if (photoProcessed == CommonConstant.PROCESSED) {
                                        fullPhotoPath = StringUtil.format("{0}/{1}", photoProxyUrl, Utility.getFileNameBySize(foodPhotoName, "200x200"));
                                    } else {
                                        fullPhotoPath = StringUtil.format("{0}/{1}", photoProxyUrl, foodPhotoName);
                                    }

                                    Food food = new Food(fullPhotoPath, foodId, foodName, foodDescription, Utility.getPrices(foodId, pricesData), 1);
                                    food.setRated(rated);
                                    food.setLiked(liked);
                                    food.setCommentCount(commentCount);
                                    listFoodsServer.add(food);
                                }

                                startingIndex = CommonConstant.UI_PAGE_SIZE * (pageCount - 1);
                                if (startingIndex < 0) {
                                    startingIndex = 0;
                                }
                                endingIndex = startingIndex + CommonConstant.UI_PAGE_SIZE;
                                if (endingIndex > listFoodsServer.size()) {
                                    endingIndex = listFoodsServer.size();
                                }
                                for (int i = startingIndex; i < endingIndex; i++) {
                                    loadingFoodsPagingUI.add(listFoodsServer.get(i));
                                }
                                pageCount++;
                                adapter.notifyDataSetChanged();
                                swipeRefreshLayout.setRefreshing(false);
                            } catch (JSONException ex) {
                                swipeRefreshLayout.setRefreshing(false);
                                Toast.makeText(getActivity(), getString(R.string.no_data_message), Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }, postData, ResquestConstant.JSON_DATA);
        ratedTask.setProgressDialogShow(false);
        ratedTask.setRequestMethod(ResquestConstant.GET);
        String restaurant = String.valueOf(RestaurantCache.GetInstance().get().getId());
        String token = userLocalStore.getLoggedInUser().getTokenId();
        String url = StringUtil.format("{0}/{1}/{2}/{3}/{4}/{5}", CommonConstant.SERVER_ADDRESS, AppConstant.Foods.FOODS_KEY.toString(), restaurant, AppConstant.Foods.RATED_KEY.toString(), String.valueOf(page), token);
        ratedTask.execute(url);
    }*/

    protected void refreshFoods() {
        refreshPage = 0;
        swipeRefreshLayout.setRefreshing(true);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(CommonConstant.SERVER_ADDRESS_RETROFIT).client(client)
                .build();
        int restaurantId = RestaurantCache.GetInstance().get().getId();
        for (refreshPage = 0; refreshPage <= countPagingServer; refreshPage++) {
            String token = userLocalStore.getLoggedInUser().getTokenId();
            IFood service = retrofit.create(IFood.class);
            Call<JsonObject> call = service.getFoodPage(restaurantId,AppConstant.Foods.RATED_KEY.toString(),refreshPage, token);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    JsonObject jsonResponse = response.body();
                    try {
                        JSONObject objects = (JSONObject) new JSONTokener(jsonResponse.toString()).nextValue();
                        JSONObject objectData = objects.getJSONObject(AppConstant.Common.DATA_KEY.toString());
                        JSONObject objectTopRateData = objectData.getJSONObject(AppConstant.Foods.TOP_RATED_KEY.toString());
                        JSONArray foodsData = objectTopRateData.getJSONArray(AppConstant.Foods.FOODS_KEY.toString());
                        JSONArray pricesData = objectTopRateData.getJSONArray(AppConstant.Foods.FOOD_PRICES_KEY.toString());
                        for (int i = 0; i < foodsData.length(); i++) {
                            JSONObject object = foodsData.getJSONObject(i);
                            int foodId = object.getInt(AppConstant.Foods.FOOD_ID_KEY.toString());
                            String foodName = object.getString(AppConstant.Foods.FOOD_NAME_KEY.toString());
                            double rated = object.getDouble(AppConstant.Foods.RATED_KEY.toString());
                            int liked = object.getInt(AppConstant.Foods.LIKED_KEY.toString());
                            int commentCount = object.getInt(AppConstant.Foods.COMMENT_COUNT_KEY.toString());
                            //double foodPrice = object.getDouble("food_price");
                            String foodDescription = object.getString(AppConstant.Foods.FOOD_DESCRIPTION_KEY.toString());
                            //Process get image
                            String foodPhotoName = object.getString(AppConstant.Foods.FOOD_PHOTO_KEY.toString());
                            int photoProxyId = object.getInt(AppConstant.Foods.FOOD_PHOTO_URL_KEY.toString());
                            int photoProcessed = object.getInt(AppConstant.Foods.PHOTO_PROCESSED_KEY.toString());
                            String photoProxyUrl = AppStore.GetInstance().getPhotoUrl(photoProxyId);
                            String fullPhotoPath = "";
                            if (photoProcessed == CommonConstant.PROCESSED) {
                                fullPhotoPath = StringUtil.format("{0}/{1}", photoProxyUrl, Utility.getFileNameBySize(foodPhotoName, "200x200"));
                            } else {
                                fullPhotoPath = StringUtil.format("{0}/{1}", photoProxyUrl, foodPhotoName);
                            }
                            Food food = new Food(fullPhotoPath, foodId, foodName, foodDescription, Utility.getPrices(foodId, pricesData), 1);
                            food.setRated(rated);
                            food.setLiked(liked);
                            food.setCommentCount(commentCount);
                            Utility.updateFood(foodId, food, loadingFoodsPagingUI);
                        }
                        if (refreshPage >= countPagingServer) {
                            adapter.notifyDataSetChanged();
                            swipeRefreshLayout.setRefreshing(false);
                            refreshPage = 0;
                        }

                    } catch (JSONException ex) {
                        swipeRefreshLayout.setRefreshing(false);
                        Toast.makeText(getActivity(), getString(R.string.no_data_message), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    swipeRefreshLayout.setRefreshing(false);
                    Log.d("Error", t.getMessage());
                }
            });
        }
    }
    /*protected void refreshFoods() {
        refreshPage = 0;
        swipeRefreshLayout.setRefreshing(true);
        for (refreshPage = 0; refreshPage <= countPagingServer; refreshPage++) {
            HashMap<String, String> postData = new HashMap<String, String>();
            RequestAsyncTask ratedTask = new RequestAsyncTask(getActivity(),
                    new AsyncResponse() {

                        @Override
                        public void processFinish(String output) {
                            if (output.length() == 0) {
                                swipeRefreshLayout.setRefreshing(false);
                                Toast.makeText(getActivity(), getString(R.string.no_data_message), Toast.LENGTH_LONG).show();
                            } else {
                                try {
                                    JSONObject objects = (JSONObject) new JSONTokener(output).nextValue();
                                    JSONObject objectData = objects.getJSONObject(AppConstant.Common.DATA_KEY.toString());
                                    JSONObject objectTopRateData = objectData.getJSONObject(AppConstant.Foods.TOP_RATED_KEY.toString());
                                    JSONArray foodsData = objectTopRateData.getJSONArray(AppConstant.Foods.FOODS_KEY.toString());
                                    JSONArray pricesData = objectTopRateData.getJSONArray(AppConstant.Foods.FOOD_PRICES_KEY.toString());
                                    for (int i = 0; i < foodsData.length(); i++) {
                                        JSONObject object = foodsData.getJSONObject(i);
                                        int foodId = object.getInt(AppConstant.Foods.FOOD_ID_KEY.toString());
                                        String foodName = object.getString(AppConstant.Foods.FOOD_NAME_KEY.toString());
                                        double rated = object.getDouble(AppConstant.Foods.RATED_KEY.toString());
                                        int liked = object.getInt(AppConstant.Foods.LIKED_KEY.toString());
                                        int commentCount = object.getInt(AppConstant.Foods.COMMENT_COUNT_KEY.toString());
                                        //double foodPrice = object.getDouble("food_price");
                                        String foodDescription = object.getString(AppConstant.Foods.FOOD_DESCRIPTION_KEY.toString());
                                        //Process get image
                                        String foodPhotoName = object.getString(AppConstant.Foods.FOOD_PHOTO_KEY.toString());
                                        int photoProxyId = object.getInt(AppConstant.Foods.FOOD_PHOTO_URL_KEY.toString());
                                        int photoProcessed = object.getInt(AppConstant.Foods.PHOTO_PROCESSED_KEY.toString());
                                        String photoProxyUrl = AppStore.GetInstance().getPhotoUrl(photoProxyId);
                                        String fullPhotoPath = "";
                                        if (photoProcessed == CommonConstant.PROCESSED) {
                                            fullPhotoPath = StringUtil.format("{0}/{1}", photoProxyUrl, Utility.getFileNameBySize(foodPhotoName, "200x200"));
                                        } else {
                                            fullPhotoPath = StringUtil.format("{0}/{1}", photoProxyUrl, foodPhotoName);
                                        }
                                        Food food = new Food(fullPhotoPath, foodId, foodName, foodDescription, Utility.getPrices(foodId, pricesData), 1);
                                        food.setRated(rated);
                                        food.setLiked(liked);
                                        food.setCommentCount(commentCount);
                                        Utility.updateFood(foodId, food, loadingFoodsPagingUI);
                                    }
                                    if (refreshPage >= countPagingServer) {
                                        adapter.notifyDataSetChanged();
                                        swipeRefreshLayout.setRefreshing(false);
                                        refreshPage = 0;
                                    }

                                } catch (JSONException ex) {
                                    swipeRefreshLayout.setRefreshing(false);
                                    Toast.makeText(getActivity(), getString(R.string.no_data_message), Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    }, postData, ResquestConstant.JSON_DATA);
            ratedTask.setProgressDialogShow(false);
            ratedTask.setRequestMethod(ResquestConstant.GET);
            String restaurant = String.valueOf(RestaurantCache.GetInstance().get().getId());
            String token = userLocalStore.getLoggedInUser().getTokenId();
            String url = StringUtil.format("{0}/{1}/{2}/{3}/{4}/{5}", CommonConstant.SERVER_ADDRESS, AppConstant.Foods.FOODS_KEY.toString(), restaurant, AppConstant.Foods.RATED_KEY.toString(), String.valueOf(refreshPage), token);
            ratedTask.execute(url);
        }
    }*/

    @Override
    public void onRefresh() {
        refreshFoods();
    }

}
