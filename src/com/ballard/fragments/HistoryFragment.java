package com.ballard.fragments;


import android.app.Fragment;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.ballard.activities.R;
import com.ballard.adapter.HistoryOrderAdapter;
import com.ballard.constants.AppConstant;
import com.ballard.constants.CommonConstant;
import com.ballard.customs.MyGestureDetectorMenuSlide;
import com.ballard.database.DatabaseHelper;
import com.ballard.localstore.UserLocalStore;
import com.ballard.models.Order;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment {
    ArrayList<Order> listOrdersDB;
    ArrayList<Order> loadingOrdersPagingUI;
    HistoryOrderAdapter adapter;
    int countPagingServer = 0;
    int pageCount = 1;
    int startingIndex;
    int endingIndex;
    private UserLocalStore userLocalStore;

    private ListView listView;

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_history, container, false);
        userLocalStore = new UserLocalStore(getActivity());

        listView = (ListView) rootView.findViewById(R.id.list_orders_histories_view);
        listOrdersDB = new ArrayList<Order>();
        loadingOrdersPagingUI = new ArrayList<Order>();
        GetOrders(countPagingServer);

        // Implementing scroll refresh
       /* listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            int threshold = 1;
            int count = listView.getCount();

            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollStatus) {
                if (scrollStatus == SCROLL_STATE_IDLE) {
                    if (listView.getLastVisiblePosition() >= count - threshold && pageCount <= determinePagingUI()) {
                        startingIndex = CommonConstant.UI_PAGE_SIZE * (pageCount - 1);
                        if (startingIndex < 0) {
                            startingIndex = 0;
                        }
                        endingIndex = startingIndex + CommonConstant.UI_PAGE_SIZE;
                        if (endingIndex > listOrdersDB.size()) {
                            endingIndex = listOrdersDB.size();
                        }
                        for (int i = startingIndex; i < endingIndex; i++) {
                            loadingOrdersPagingUI.add(listOrdersDB.get(i));
                        }
                        pageCount++;
                        adapter.notifyDataSetChanged();
                        if (loadingOrdersPagingUI.size() >= listOrdersDB.size() - CommonConstant.UI_PAGE_SIZE) {
                            countPagingServer++;
                            GetOrders(countPagingServer);
                        }
                    }
                }
            }

            @Override
            public void onScroll(AbsListView absListView, int firstItem, int visibleItemCount, final int totalItems) {
            }
        });*/

        /*listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CurrentSelectFoodDialog currentSelectFoodDialog = new CurrentSelectFoodDialog();
                FragmentManager fragmentManager = getFragmentManager();
                currentSelectFoodDialog.show(fragmentManager, "");
                Food selectFood = listOrdersDB.get(position);
                MyFoodOrder.getInstance().setCurrentFood(selectFood);

            }
        });*/

        final GestureDetector gestureDetector = new GestureDetector(getActivity(),
                new MyGestureDetectorMenuSlide(listView));

        listView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });
        return rootView;
    }

    private int determinePagingUI() {
        int determinePages;
        if (listOrdersDB.size() % CommonConstant.UI_PAGE_SIZE == 0) {
            determinePages = listOrdersDB.size() / CommonConstant.UI_PAGE_SIZE;
        } else {
            determinePages = (listOrdersDB.size() / CommonConstant.UI_PAGE_SIZE) + 1;
        }
        return determinePages;
    }

    protected void GetOrders(int page) {
        DatabaseHelper helper = new DatabaseHelper(getActivity());
        SQLiteDatabase db = helper.getReadableDatabase();
        String[] tableColumns = new String[]{
                AppConstant.Order.ORDER_ID_KEY.toString(),
                AppConstant.Restaurant.RESTAURANT_ID_KEY.toString(),
                AppConstant.Order.USER_ID_KEY.toString(),
                AppConstant.Order.TABLE_ID_KEY.toString(),
                AppConstant.Order.DATETIME_ORDER_KEY.toString(),
                AppConstant.Order.STATUS_KEY.toString(),
                AppConstant.Order.AMOUNT_KEY.toString(),
                AppConstant.Order.SERVICE_AMOUNT_KEY.toString(),
                AppConstant.Order.TAX_AMOUNT_KEY.toString(),
                AppConstant.Order.TOTAL_AMOUNT_KEY.toString()
        };
        int offset = CommonConstant.UI_PAGE_SIZE * (page - 1);
        //String whereClause = StringUtil.format("1 = 1 LIMIT {0} OFFSET {1}",String.valueOf(CommonConstant.UI_PAGE_SIZE), String.valueOf(offset));
        String whereClause = "1 = 1";

        Cursor cursor = db.query(AppConstant.Histories.HISTORIES_TABLE_NAME.toString(), tableColumns, whereClause, null, null, null, AppConstant.Order.ORDER_ID_KEY.toString() + " DESC", null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Order order = new Order();
            order.setOrderId(cursor.getInt(0));
            order.setRestaurantId(cursor.getInt(1));
            order.setUserId(cursor.getInt(2));
            order.setTable(cursor.getInt(3));
            order.setTimeOrder(cursor.getLong(4));
            order.setStatus(cursor.getInt(5));
            order.setAmount(cursor.getDouble(6));
            order.setServiceAmount(cursor.getDouble(7));
            order.setTaxAmount(cursor.getDouble(8));
            order.setTotalAmount(cursor.getDouble(9));
            listOrdersDB.add(order);
            cursor.moveToNext();
        }
        db.close();

        startingIndex = CommonConstant.UI_PAGE_SIZE * (pageCount - 1);
        if (startingIndex < 0) {
            startingIndex = 0;
        }
        endingIndex = startingIndex + CommonConstant.UI_PAGE_SIZE;
        if (endingIndex > listOrdersDB.size()) {
            endingIndex = listOrdersDB.size();
        }
        for (int i = startingIndex; i < endingIndex; i++) {
            loadingOrdersPagingUI.add(listOrdersDB.get(i));
        }
        pageCount++;

        adapter = new HistoryOrderAdapter(getActivity(), listOrdersDB, getFragmentManager());
        listView.setAdapter(adapter);

    }

}
