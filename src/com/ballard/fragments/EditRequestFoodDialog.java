package com.ballard.fragments;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.ballard.activities.R;
import com.ballard.businesses.MyFoodOrder;
import com.ballard.models.Food;

public class EditRequestFoodDialog extends DialogFragment {
    EditText txtEditRequestNote;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        getDialog().getWindow().setGravity(Gravity.BOTTOM);
//        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, 300);
        View rootView = inflater.inflate(R.layout.request_foods, container, false);
        txtEditRequestNote = (EditText) rootView.findViewById(R.id.txtEditRequestNote);
        Food foodCurrent = MyFoodOrder.getInstance().getCurrentSelectFood();
        txtEditRequestNote.setText(foodCurrent.getRequestNote());

        Button bntUpdateRequest = (Button) rootView.findViewById(R.id.bntUpdateRequest);
        bntUpdateRequest.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Food foodCurrent = MyFoodOrder.getInstance().getCurrentSelectFood();
                foodCurrent.setRequestNote(txtEditRequestNote.getText().toString());
                MyFoodOrder.getInstance().getViewRequestNote().setText(txtEditRequestNote.getText().toString());
                MyFoodOrder.getInstance().clearCurrentSelectFood();
                dismiss();
            }
        });
        Button bntCancel = (Button) rootView.findViewById(R.id.bntCancelEditRequest);
        bntCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);
    }
}