package com.ballard.fragments;


import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ballard.activities.R;
import com.ballard.adapter.FoodListAdapter;
import com.ballard.businesses.SearchData;
import com.ballard.constants.AppConstant;
import com.ballard.constants.CommonConstant;
import com.ballard.constants.ResquestConstant;
import com.ballard.customs.MyGestureDetectorMenuSlide;
import com.ballard.interfaces.IFood;
import com.ballard.localstore.AppStore;
import com.ballard.localstore.UserLocalStore;
import com.ballard.models.Comment;
import com.ballard.models.Food;
import com.ballard.requests.RequestAsyncTask;
import com.ballard.responses.AsyncResponse;
import com.ballard.utilities.RestaurantCache;
import com.ballard.utilities.StringUtil;
import com.ballard.utilities.Utility;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class FoodSearchResultFragment extends Fragment {

    private TextView txtHomeLabel;
    private Button bnRateOrder;
    ArrayList<Food> listFoodsServer;
    ArrayList<Food> loadingFoodsPagingUI;
    ArrayList<String> downloadContentPage;
    FoodListAdapter adapter;
    int countPagingServer = 0;
    int pageCount = 1;
    int startingIndex;
    int endingIndex;
    private UserLocalStore userLocalStore;
    int refreshPage = 0;
    TextView lbNotFound;

    private ListView listView;

    public FoodSearchResultFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_food_view, container, false);
        userLocalStore = new UserLocalStore(getActivity());
        downloadContentPage = new ArrayList<>();

        lbNotFound = (TextView)rootView.findViewById(R.id.lbNotFound);
        listView = (ListView) rootView.findViewById(R.id.list_foods_view);
        listFoodsServer = new ArrayList<Food>();
        FragmentManager fragmentManager = getFragmentManager();
        adapter = new FoodListAdapter(getActivity(), listFoodsServer, fragmentManager);
        listView.setAdapter(adapter);
        searchFood(SearchData.getInstance().getKeySearch());

        return rootView;
    }

    protected void searchFood(String keySearch){

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(CommonConstant.SERVER_ADDRESS_RETROFIT).client(client)
                .build();

        String token = userLocalStore.getLoggedInUser().getTokenId();
        int restaurant = RestaurantCache.GetInstance().get().getId();
        try {
            keySearch = URLEncoder.encode(keySearch, "UTF-8");
        } catch (UnsupportedEncodingException e) {
        }
        IFood service = retrofit.create(IFood.class);
        Call<JsonObject> call = service.getSearchFood(keySearch, restaurant, token);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject jsonResponse = response.body();
                try {
                    JSONObject objects = (JSONObject) new JSONTokener(jsonResponse.toString()).nextValue();
                    JSONObject objectData = objects.getJSONObject(AppConstant.Common.DATA_KEY.toString());
                    JSONArray foodsData = objectData.getJSONArray(AppConstant.Foods.FOODS_KEY.toString());
                    JSONArray pricesData = objectData.getJSONArray(AppConstant.Foods.FOOD_PRICES_KEY.toString());
                    for (int i = 0; i < foodsData.length(); i++) {
                        JSONObject object = foodsData.getJSONObject(i);
                        int foodId = object.getInt(AppConstant.Foods.FOOD_ID_KEY.toString());
                        String foodName = object.getString(AppConstant.Foods.FOOD_NAME_KEY.toString());
                        String foodDescription = object.getString(AppConstant.Foods.FOOD_DESCRIPTION_KEY.toString());
                        double rated = object.getDouble(AppConstant.Foods.RATED_KEY.toString());
                        int liked = object.getInt(AppConstant.Foods.LIKED_KEY.toString());
                        int commentCount = object.getInt(AppConstant.Foods.COMMENT_COUNT_KEY.toString());
                        //Process get image
                        String foodPhotoName = object.getString(AppConstant.Foods.FOOD_PHOTO_KEY.toString());
                        int photoProxyId = object.getInt(AppConstant.Foods.FOOD_PHOTO_URL_KEY.toString());
                        int photoProcessed = object.getInt(AppConstant.Foods.PHOTO_PROCESSED_KEY.toString());
                        String photoProxyUrl = AppStore.GetInstance().getPhotoUrl(photoProxyId);
                        String fullPhotoPath = "";
                        if (photoProcessed == CommonConstant.PROCESSED) {
                            fullPhotoPath = StringUtil.format("{0}/{1}", photoProxyUrl, Utility.getFileNameBySize(foodPhotoName, "200x200"));
                        } else {
                            fullPhotoPath = StringUtil.format("{0}/{1}", photoProxyUrl, foodPhotoName);
                        }
                        Food food = new Food(fullPhotoPath, foodId, foodName, foodDescription, Utility.getPrices(foodId, pricesData), 1);
                        food.setRated(rated);
                        food.setLiked(liked);
                        food.setCommentCount(commentCount);
                        listFoodsServer.add(food);
                    }
                    adapter.notifyDataSetChanged();
                    if(listFoodsServer.size() == 0){
                        lbNotFound.setVisibility(View.VISIBLE);
                    } else {
                        lbNotFound.setVisibility(View.GONE);
                    }
                } catch (JSONException ex) {
                    Toast.makeText(getActivity(), getString(R.string.no_data_message), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

}
