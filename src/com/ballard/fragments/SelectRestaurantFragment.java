package com.ballard.fragments;


import android.app.DialogFragment;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ballard.activities.GuestMenuActivity;
import com.ballard.activities.R;
import com.ballard.adapter.RestaurantListAdapter;
import com.ballard.adapter.RestaurantSearchAutoCompleteAdapter;
import com.ballard.constants.AppConstant;
import com.ballard.constants.CommonConstant;
import com.ballard.constants.ResquestConstant;
import com.ballard.customs.DelayAutoCompleteTextView;
import com.ballard.database.DatabaseHelper;
import com.ballard.interfaces.IFood;
import com.ballard.interfaces.IRestaurant;
import com.ballard.localstore.UserLocalStore;
import com.ballard.models.Comment;
import com.ballard.models.Default;
import com.ballard.models.Restaurant;
import com.ballard.models.Service;
import com.ballard.models.Tax;
import com.ballard.requests.RequestAsyncTask;
import com.ballard.responses.AsyncResponse;
import com.ballard.utilities.MemoryCache;
import com.ballard.utilities.RestaurantCache;
import com.ballard.utilities.StringUtil;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectRestaurantFragment extends DialogFragment {

    private ListView listRestaurantView;
    DelayAutoCompleteTextView txtSearchRestaurant;
    private ArrayList<Restaurant> listRestaurant;
    private RestaurantListAdapter adapter;
    private UserLocalStore userLocalStore;
    private RestaurantSearchAutoCompleteAdapter autoCompleteAdapter;
    private TextView viewNotFound;

    public SelectRestaurantFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_select_restaurant, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        listRestaurant = new ArrayList<Restaurant>();
        listRestaurantView = (ListView) rootView.findViewById(R.id.list_restaurant_view);
        adapter = new RestaurantListAdapter(getActivity(), listRestaurant);
        listRestaurantView.setAdapter(adapter);

        userLocalStore = new UserLocalStore(getActivity());

        txtSearchRestaurant = (DelayAutoCompleteTextView) rootView.findViewById(R.id.txtSearchRestaurant);
        txtSearchRestaurant.setThreshold(2);

        autoCompleteAdapter = new RestaurantSearchAutoCompleteAdapter(getActivity());
        txtSearchRestaurant.setAdapter(autoCompleteAdapter);

        txtSearchRestaurant.setLoadingIndicator(
                (android.widget.ProgressBar) rootView.findViewById(R.id.pb_loading_indicator));
        txtSearchRestaurant.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                String restaurantName = (String) adapterView.getItemAtPosition(position);
                txtSearchRestaurant.setText(restaurantName);
                searchRestaurant(restaurantName);
            }
        });

        viewNotFound = (TextView) rootView.findViewById(R.id.viewNotFound);

        listRestaurantView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Get Restaurant
                Restaurant restaurant = listRestaurant.get(position);
                //Save select restaurant
                userLocalStore.saveRestaurantSelect(restaurant);
                //Cache restaurant in memory
                RestaurantCache.GetInstance().set(restaurant);

                //Save database select restaurant
                DatabaseHelper helper = new DatabaseHelper(getActivity());
                SQLiteDatabase db = helper.getWritableDatabase();
                ContentValues values = new ContentValues();

                values.put(AppConstant.Restaurant.RESTAURANT_ID_KEY.toString(), restaurant.getId());
                values.put(AppConstant.Restaurant.RESTAURANT_NAME_KEY.toString(), restaurant.getName());
                values.put(AppConstant.Restaurant.RESTAURANT_OWNER_KEY.toString(), restaurant.getOwner());
                values.put(AppConstant.Restaurant.RESTAURANT_CHAIN_ID_KEY.toString(), restaurant.getChainsId());
                values.put(AppConstant.Restaurant.RESTAURANT_WIFI_KEY.toString(), restaurant.getWifiName());
                values.put(AppConstant.Restaurant.RESTAURANT_LAT_KEY.toString(), restaurant.getLatitude());
                values.put(AppConstant.Restaurant.RESTAURANT_LNG_KEY.toString(), restaurant.getLongitude());
                values.put(AppConstant.Restaurant.RESTAURANT_ADDRESS1_KEY.toString(), restaurant.getAddress1());
                values.put(AppConstant.Restaurant.RESTAURANT_ADDRESS2_KEY.toString(), restaurant.getAddress2());
                values.put(AppConstant.Restaurant.RESTAURANT_CITY_KEY.toString(), restaurant.getCity());
                values.put(AppConstant.Restaurant.RESTAURANT_COUNTRY_KEY.toString(), restaurant.getCountry());
                values.put(AppConstant.Restaurant.RESTAURANT_ZIP_KEY.toString(), restaurant.getZipCode());
                values.put(AppConstant.Restaurant.RESTAURANT_PHONE_KEY.toString(), restaurant.getCellphone());
                values.put(AppConstant.Restaurant.RESTAURANT_TAGS_KEY.toString(), restaurant.getTags());
                values.put(AppConstant.Restaurant.RESTAURANT_PHOTOS_KEY.toString(), restaurant.getPhotoUrl());
                String sql = StringUtil.format("DELETE FROM {0} WHERE {1}={2}", AppConstant.Restaurant.RESTAURANT_TABLE_NAME.toString(), AppConstant.Restaurant.RESTAURANT_ID_KEY.toString(), String.valueOf(restaurant.getId()));
                db.execSQL(sql);

                db.insert(AppConstant.Restaurant.RESTAURANT_TABLE_NAME.toString(), null, values);
                db.close();

                boolean isOnline = true; //Get online or offline mode
                if (isOnline) {
                    processOnRestaurantSetting();
                } else {
                    //Offline mode will get setting from DB
                }
                // startActivity(new Intent(getActivity(), GuestMenuActivity.class));
            }
        });

        return rootView;
    }

    protected void processOnRestaurantSetting() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(CommonConstant.SERVER_ADDRESS_RETROFIT).client(client)
                .build();

        String token = userLocalStore.getLoggedInUser().getTokenId();
        int restaurantId = RestaurantCache.GetInstance().get().getId();
        IRestaurant service = retrofit.create(IRestaurant.class);
        Call<JsonObject> call = service.getRestaurantSetting(restaurantId, token);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject jsonResponse = response.body();
                try {
                    JSONObject objects = (JSONObject) new JSONTokener(jsonResponse.toString()).nextValue();
                    JSONObject objectData = objects.getJSONObject(AppConstant.Common.DATA_KEY.toString());
                    JSONArray objectDefaultData = objectData.getJSONArray(AppConstant.Restaurant.RESTAURANT_DEFAULT_KEY.toString());
                    JSONArray objectTaxData = objectData.getJSONArray(AppConstant.Restaurant.RESTAURANT_TAX_KEY.toString());
                    JSONArray objectServiceData = objectData.getJSONArray(AppConstant.Restaurant.RESTAURANT_SERVICE_KEY.toString());

                    List<Default> defaultList = new ArrayList<>();
                    //Get default setting and save db, load for using
                    for (int i = 0; i < objectDefaultData.length(); i++) {
                        JSONObject object = objectDefaultData.getJSONObject(i);
                        int defaultId = object.getInt(AppConstant.Restaurant.DEFAULT_ID_KEY.toString());
                        int restaurantId = object.getInt(AppConstant.Restaurant.RESTAURANT_ID_KEY.toString());
                        String allowPushMenu = object.getString(AppConstant.Restaurant.ALLOW_PUBLISH_MENU_KEY.toString());
                        String allowWifiMenu = object.getString(AppConstant.Restaurant.ALLOW_WIFI_MENU_KEY.toString());
                        String allowCodeMenu = object.getString(AppConstant.Restaurant.ALLOW_CODE_MENU_KEY.toString());
                        String menuAccessCode = object.getString(AppConstant.Restaurant.MENU_ACCESS_CODE_KEY.toString());
                        String currencyCode = object.getString(AppConstant.Restaurant.CURRENCY_TYPE_KEY.toString());
                        String wifiName = object.getString(AppConstant.Restaurant.WIFI_KEY.toString());
                        Default objectDefault = new Default(defaultId, restaurantId, allowPushMenu, allowWifiMenu, allowCodeMenu, menuAccessCode, currencyCode, wifiName);
                        defaultList.add(objectDefault);
                    }
                    List<Tax> taxList = new ArrayList<>();
                    //Get tax and save db, load for using
                    for (int i = 0; i < objectTaxData.length(); i++) {
                        JSONObject object = objectTaxData.getJSONObject(i);
                        int taxId = object.getInt(AppConstant.Restaurant.TAX_ID_KEY.toString());
                        String taxName = object.getString(AppConstant.Restaurant.TAX_NAME_KEY.toString());
                        int taxType = object.getInt(AppConstant.Restaurant.TAX_TYPE_KEY.toString());
                        String taxValue = object.getString(AppConstant.Restaurant.TAX_VALUE_KEY.toString());
                        taxValue = taxValue.replace('%', ' ').trim();
                        Tax tax = new Tax(taxId, taxName, taxType, Double.valueOf(taxValue));
                        taxList.add(tax);
                    }
                    List<Service> serviceList = new ArrayList<>();
                    //Get service and save db, load for using
                    for (int i = 0; i < objectServiceData.length(); i++) {
                        JSONObject object = objectServiceData.getJSONObject(i);
                        int serviceId = object.getInt(AppConstant.Restaurant.SERVICE_ID_KEY.toString());
                        String serviceName = object.getString(AppConstant.Restaurant.SERVICE_NAME_KEY.toString());
                        int serviceType = object.getInt(AppConstant.Restaurant.SERVICE_TYPE_KEY.toString());
                        String serviceValue = object.getString(AppConstant.Restaurant.SERVICE_VALUE_KEY.toString());
                        serviceValue = serviceValue.replace('%', ' ').trim();
                        Service service = new Service(serviceId, serviceName, serviceType, Double.valueOf(serviceValue));
                        serviceList.add(service);
                    }
                    RestaurantCache.GetInstance().get().setDefaultsList(defaultList);
                    RestaurantCache.GetInstance().get().setTaxList(taxList);
                    RestaurantCache.GetInstance().get().setServiceList(serviceList);

                    Locale locale = StringUtil.getLocale(RestaurantCache.GetInstance().get().getDefaultsList().get(0).getCurrencyCode());
                    NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
                    RestaurantCache.GetInstance().setNumberFormat(currencyFormatter);

                    startActivity(new Intent(getActivity(), GuestMenuActivity.class));
                    dismiss();
                    getActivity().finish();
                    } catch (JSONException ex) {
                    Toast.makeText(getActivity(), getString(R.string.no_data_message), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }


    protected void searchRestaurant(String keySearch){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(CommonConstant.SERVER_ADDRESS_RETROFIT).client(client)
                .build();

        String token = userLocalStore.getLoggedInUser().getTokenId();
        try {
            keySearch = URLEncoder.encode(keySearch, "UTF-8");
        } catch (UnsupportedEncodingException e) {
        }
        IRestaurant service = retrofit.create(IRestaurant.class);
        Call<JsonObject> call = service.getSearchRestaurant(keySearch, token);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonObject jsonResponse = response.body();
                try {
                    JSONObject objects = (JSONObject) new JSONTokener(jsonResponse.toString()).nextValue();
                    JSONArray results = objects.getJSONArray(AppConstant.Common.DATA_KEY.toString());
                    for (int i = 0; i < results.length(); i++) {
                        JSONObject object = results.getJSONObject(i);
                        int restaurantId = object.getInt(AppConstant.Restaurant.RESTAURANT_ID_KEY.toString());
                        String restaurantName = object.getString(AppConstant.Restaurant.RESTAURANT_NAME_KEY.toString());
                        String owner = object.getString(AppConstant.Restaurant.RESTAURANT_OWNER_KEY.toString());
                        int chainsId = object.getInt(AppConstant.Restaurant.RESTAURANT_CHAIN_ID_KEY.toString());
                        double latitude = object.getDouble(AppConstant.Restaurant.RESTAURANT_LAT_KEY.toString());
                        double longitude = object.getDouble(AppConstant.Restaurant.RESTAURANT_LNG_KEY.toString());
                        String wifiName = object.getString(AppConstant.Restaurant.RESTAURANT_WIFI_KEY.toString());
                        String address1 = object.getString(AppConstant.Restaurant.RESTAURANT_ADDRESS1_KEY.toString());
                        String address2 = object.getString(AppConstant.Restaurant.RESTAURANT_ADDRESS2_KEY.toString());
                        String city = object.getString(AppConstant.Restaurant.RESTAURANT_CITY_KEY.toString());
                        String country = object.getString(AppConstant.Restaurant.RESTAURANT_COUNTRY_KEY.toString());
                        String zip = object.getString(AppConstant.Restaurant.RESTAURANT_ZIP_KEY.toString());
                        String phone = object.getString(AppConstant.Restaurant.RESTAURANT_PHONE_KEY.toString());
                        String tags = object.getString(AppConstant.Restaurant.RESTAURANT_TAGS_KEY.toString());
                        String photos = object.getString(AppConstant.Restaurant.RESTAURANT_PHOTOS_KEY.toString());
                        photos = "";
                        Restaurant restaurant = new Restaurant(restaurantId, restaurantName, owner, chainsId, latitude, longitude, wifiName, address1, address2, city, country, zip, phone, tags, photos);
                        listRestaurant.add(restaurant);
                    }

                    adapter.notifyDataSetChanged();
                    //new DownloadImageRestaurantTask(listRestaurant, 0, Gravity.CENTER).execute();
                    if (listRestaurant.size() > 0) {
                        viewNotFound.setVisibility(View.GONE);
                    } else {
                        viewNotFound.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException ex) {
                    ex.printStackTrace();
                    Toast.makeText(getActivity(), getString(R.string.no_data_message), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    /*protected void searchRestaurant(String keySearch) {
        listRestaurant.clear();
        adapter.notifyDataSetChanged();
        HashMap<String, String> postData = new HashMap<String, String>();
        RequestAsyncTask restaurantTask = new RequestAsyncTask(getActivity(),
                new AsyncResponse() {
                    @Override
                    public void processFinish(String output) {
                        if (output.length() == 0) {
                            Toast.makeText(getActivity(), getString(R.string.no_data_message), Toast.LENGTH_LONG).show();
                        } else {
                            try {
                                JSONObject objects = (JSONObject) new JSONTokener(output).nextValue();
                                JSONArray results = objects.getJSONArray(AppConstant.Common.DATA_KEY.toString());
                                for (int i = 0; i < results.length(); i++) {
                                    JSONObject object = results.getJSONObject(i);
                                    int restaurantId = object.getInt(AppConstant.Restaurant.RESTAURANT_ID_KEY.toString());
                                    String restaurantName = object.getString(AppConstant.Restaurant.RESTAURANT_NAME_KEY.toString());
                                    String owner = object.getString(AppConstant.Restaurant.RESTAURANT_OWNER_KEY.toString());
                                    int chainsId = object.getInt(AppConstant.Restaurant.RESTAURANT_CHAIN_ID_KEY.toString());
                                    double latitude = object.getDouble(AppConstant.Restaurant.RESTAURANT_LAT_KEY.toString());
                                    double longitude = object.getDouble(AppConstant.Restaurant.RESTAURANT_LNG_KEY.toString());
                                    String wifiName = object.getString(AppConstant.Restaurant.RESTAURANT_WIFI_KEY.toString());
                                    String address1 = object.getString(AppConstant.Restaurant.RESTAURANT_ADDRESS1_KEY.toString());
                                    String address2 = object.getString(AppConstant.Restaurant.RESTAURANT_ADDRESS2_KEY.toString());
                                    String city = object.getString(AppConstant.Restaurant.RESTAURANT_CITY_KEY.toString());
                                    String country = object.getString(AppConstant.Restaurant.RESTAURANT_COUNTRY_KEY.toString());
                                    String zip = object.getString(AppConstant.Restaurant.RESTAURANT_ZIP_KEY.toString());
                                    String phone = object.getString(AppConstant.Restaurant.RESTAURANT_PHONE_KEY.toString());
                                    String tags = object.getString(AppConstant.Restaurant.RESTAURANT_TAGS_KEY.toString());
                                    String photos = object.getString(AppConstant.Restaurant.RESTAURANT_PHOTOS_KEY.toString());
                                    photos = "";
                                    Restaurant restaurant = new Restaurant(restaurantId, restaurantName, owner, chainsId, latitude, longitude, wifiName, address1, address2, city, country, zip, phone, tags, photos);
                                    listRestaurant.add(restaurant);
                                }

                                adapter.notifyDataSetChanged();
                                //new DownloadImageRestaurantTask(listRestaurant, 0, Gravity.CENTER).execute();
                                if (listRestaurant.size() > 0) {
                                    viewNotFound.setVisibility(View.GONE);
                                } else {
                                    viewNotFound.setVisibility(View.VISIBLE);
                                }
                            } catch (JSONException ex) {
                                Toast.makeText(getActivity(), getString(R.string.no_data_message), Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }, postData, ResquestConstant.JSON_DATA);
        restaurantTask.setRequestMethod(ResquestConstant.GET);
        String token = userLocalStore.getLoggedInUser().getTokenId();
        try {
            keySearch = URLEncoder.encode(keySearch, "UTF-8");
        } catch (UnsupportedEncodingException e) {
        }
        String url = StringUtil.format("{0}/{1}/{2}/{3}", CommonConstant.SERVER_ADDRESS, AppConstant.Restaurant.RESTAURANTS_KEY.toString(), keySearch.trim(), token);
        restaurantTask.execute(url);
    }*/


}
