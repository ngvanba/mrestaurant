package com.ballard.tasks;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.ballard.models.User;
import com.ballard.requests.GetUserCallBack;

public class FetchUserDataAsyncTask extends AsyncTask<Void, Void, User> {

    User user;
    GetUserCallBack<User> callBack;
    ProgressDialog progressDialog;

    public FetchUserDataAsyncTask(User user, GetUserCallBack<User> callBack, ProgressDialog progressDialog) {
        this.user = user;
        this.callBack = callBack;
        this.progressDialog = progressDialog;
    }

    @SuppressWarnings("deprecation")
    @Override
    protected User doInBackground(Void... args) {
//		ArrayList<NameValuePair> dataToSend = new ArrayList<NameValuePair>();
//		dataToSend.add(new BasicNameValuePair("email",user.getEmail()));
//		dataToSend.add(new BasicNameValuePair("password",user.getPassword()));
//		
//		HttpParams httpParams = new BasicHttpParams();
//		HttpConnectionParams.setConnectionTimeout(httpParams, CommonConstant.TIMEOUT_CONNECTION);
//		HttpConnectionParams.setSoTimeout(httpParams, CommonConstant.TIMEOUT_CONNECTION);
//		
//		HttpClient httpClient = new DefaultHttpClient(httpParams);
//		HttpPost httpPost = new HttpPost(CommonConstant.SERVER_ADDRESS + "/Account/GetUser");
//		User returnUser = null;
//		try{
//			httpPost.setEntity(new UrlEncodedFormEntity(dataToSend));
//			HttpResponse httpResponse = httpClient.execute(httpPost);
//			HttpEntity entity = httpResponse.getEntity();
//			String result = EntityUtils.toString(entity);
//			JSONObject object = new JSONObject(result);			
//			if(object.length() > 0){
//				int userId = object.getInt("UserId");
//				String email = object.getString("Email");
//				String lastName = object.getString("LastName");
//				String firstName = object.getString("FirstName");
//				String password = object.getString("Password");
//				long birthday = object.getLong("Birthday");
//				String address = object.getString("Address");
//				returnUser = new User();
//				returnUser.setUserId(userId);
//				returnUser.setEmail(email);
//				returnUser.setFirstName(firstName);
//				returnUser.setLastName(lastName);
//				returnUser.setPassword(password);
//				Date dateBirth = new Date();
//				dateBirth.setTime(birthday);
//				returnUser.setBirthday(dateBirth);
//				returnUser.setAddress(address);
//				
//			}
//		}catch(Exception ex){
//			ex.printStackTrace();
//		}		
//		
        // TODO Auto-generated method stub
        return null;//returnUser;
    }

    @Override
    protected void onPostExecute(User returnUser) {
        progressDialog.dismiss();
        callBack.done(returnUser);
        super.onPostExecute(returnUser);
    }


}
