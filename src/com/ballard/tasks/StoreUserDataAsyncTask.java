package com.ballard.tasks;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.ballard.models.ResponseStatus;
import com.ballard.models.User;
import com.ballard.requests.GetUserCallBack;

public class StoreUserDataAsyncTask extends AsyncTask<Void, Void, ResponseStatus> {

    User user;
    GetUserCallBack<ResponseStatus> callBack;
    ProgressDialog progressDialog;

    public StoreUserDataAsyncTask(User user, GetUserCallBack<ResponseStatus> callBack, ProgressDialog progressDialog) {
        this.user = user;
        this.callBack = callBack;
        this.progressDialog = progressDialog;
    }

    @SuppressWarnings("deprecation")
    @Override
    protected ResponseStatus doInBackground(Void... args) {
//		ArrayList<NameValuePair> dataToSend = new ArrayList<NameValuePair>();
//		dataToSend.add(new BasicNameValuePair("email",user.getEmail()));
//		dataToSend.add(new BasicNameValuePair("lastname",user.getLastName()));
//		dataToSend.add(new BasicNameValuePair("firstname",user.getFirstName()));
//		dataToSend.add(new BasicNameValuePair("password",user.getPassword()));		
//		dataToSend.add(new BasicNameValuePair("birthday",Long.toString(user.getBirthday().getTime())));
//		dataToSend.add(new BasicNameValuePair("address",user.getAddress()));
//		
//		HttpParams httpParams = new BasicHttpParams();
//		HttpConnectionParams.setConnectionTimeout(httpParams, CommonConstant.TIMEOUT_CONNECTION);
//		HttpConnectionParams.setSoTimeout(httpParams, CommonConstant.TIMEOUT_CONNECTION);
//		
//		HttpClient httpClient = new DefaultHttpClient(httpParams);
//		HttpPost httpPost = new HttpPost(CommonConstant.SERVER_ADDRESS + "/Account/Register");
//		ResponseStatus responseStatus = new ResponseStatus();
//		try{
//			httpPost.setEntity(new UrlEncodedFormEntity(dataToSend));
//			HttpResponse httpResponse = httpClient.execute(httpPost);
//			HttpEntity entity = httpResponse.getEntity();
//			String result = EntityUtils.toString(entity);
//			JSONObject object = new JSONObject(result);			
//			if(object.length() > 0){
//				responseStatus.setStatus(object.getInt("updateStatus"));
//				responseStatus.setMessage(object.getString("message"));
//			}
//		}catch(Exception ex){
//			ex.printStackTrace();
//		}		

        // TODO Auto-generated method stub
        return null;//responseStatus;
    }

    @Override
    protected void onPostExecute(ResponseStatus result) {
        progressDialog.dismiss();
        callBack.done(result);
        super.onPostExecute(result);
    }

}
