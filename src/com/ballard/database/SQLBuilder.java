package com.ballard.database;

import com.ballard.constants.AppConstant;

/**
 * Created by ballard on 3/5/2016.
 */
public class SQLBuilder {

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String NUMERIC_TYPE = " NUMERIC";
    private static final String REAL_TYPE = " REAL";
    private static final String BLOB_TYPE = " BLOB";
    private static final String COMMA_SEP = ",";

    public static final int SQL_CREATE_TABLE_RESTAURANT  = 1;
    public static final int SQL_CREATE_TABLE_HISTORIES  = 2;
    public static final int SQL_CREATE_TABLE_ORDER_ITEMS  = 3;
    public static final int SQL_CREATE_TABLE_ORDER_SERVICES  = 4;
    public static final int SQL_CREATE_TABLE_ORDER_TAX  = 5;

    public static String getSQL(int chooseQuery){
        String sqlResult = "";
        switch (chooseQuery){
            case SQL_CREATE_TABLE_RESTAURANT:
                sqlResult = buildCreateRestaurantTable();
                break;
            case SQL_CREATE_TABLE_HISTORIES:
                sqlResult = buildCreateHistoriesTable();
                break;
            case SQL_CREATE_TABLE_ORDER_ITEMS:
                sqlResult = buildCreateOrderItemsTable();
                break;
            case SQL_CREATE_TABLE_ORDER_SERVICES:
                sqlResult = buildCreateOrderServicesTable();
                break;
            case SQL_CREATE_TABLE_ORDER_TAX:
                sqlResult = buildCreateOrderTaxTable();
                break;
            default:
        }
        return sqlResult;
    }

    private static String buildCreateRestaurantTable(){
        final String SQL_CREATE_TABLE =
                "CREATE TABLE IF NOT EXISTS " + AppConstant.Restaurant.RESTAURANT_TABLE_NAME.toString() + " (" +
                        AppConstant.Restaurant.RESTAURANT_ID_KEY.toString() + INTEGER_TYPE + COMMA_SEP +
                        AppConstant.Restaurant.RESTAURANT_NAME_KEY.toString() + TEXT_TYPE + COMMA_SEP +
                        AppConstant.Restaurant.RESTAURANT_OWNER_KEY.toString() + TEXT_TYPE + COMMA_SEP +
                        AppConstant.Restaurant.RESTAURANT_CHAIN_ID_KEY.toString() + INTEGER_TYPE + COMMA_SEP +
                        AppConstant.Restaurant.RESTAURANT_WIFI_KEY.toString() + TEXT_TYPE + COMMA_SEP +
                        AppConstant.Restaurant.RESTAURANT_LAT_KEY.toString() + REAL_TYPE + COMMA_SEP +
                        AppConstant.Restaurant.RESTAURANT_LNG_KEY.toString() + REAL_TYPE + COMMA_SEP +
                        AppConstant.Restaurant.RESTAURANT_ADDRESS1_KEY.toString() + TEXT_TYPE + COMMA_SEP +
                        AppConstant.Restaurant.RESTAURANT_ADDRESS2_KEY.toString() + TEXT_TYPE + COMMA_SEP +
                        AppConstant.Restaurant.RESTAURANT_CITY_KEY.toString() + TEXT_TYPE + COMMA_SEP +
                        AppConstant.Restaurant.RESTAURANT_COUNTRY_KEY.toString() + TEXT_TYPE + COMMA_SEP +
                        AppConstant.Restaurant.RESTAURANT_ZIP_KEY.toString() + TEXT_TYPE + COMMA_SEP +
                        AppConstant.Restaurant.RESTAURANT_PHONE_KEY.toString() + TEXT_TYPE + COMMA_SEP +
                        AppConstant.Restaurant.RESTAURANT_TAGS_KEY.toString() + TEXT_TYPE + COMMA_SEP +
                        AppConstant.Restaurant.RESTAURANT_PHOTOS_KEY.toString() + TEXT_TYPE +
                        " )";
        return SQL_CREATE_TABLE;

    }

    private static String buildCreateHistoriesTable(){
        final String SQL_CREATE_TABLE =
                "CREATE TABLE IF NOT EXISTS " + AppConstant.Histories.HISTORIES_TABLE_NAME.toString() + " (" +
                        AppConstant.Order.ORDER_ID_KEY.toString() + INTEGER_TYPE + COMMA_SEP +
                        AppConstant.Restaurant.RESTAURANT_ID_KEY.toString() + INTEGER_TYPE + COMMA_SEP +
                        AppConstant.Order.USER_ID_KEY.toString() + INTEGER_TYPE + COMMA_SEP +
                        AppConstant.Order.TABLE_ID_KEY.toString() + INTEGER_TYPE + COMMA_SEP +
                        AppConstant.Order.DATETIME_ORDER_KEY.toString() + NUMERIC_TYPE + COMMA_SEP +
                        AppConstant.Order.STATUS_KEY.toString() + INTEGER_TYPE + COMMA_SEP +
                        AppConstant.Order.AMOUNT_KEY.toString() + REAL_TYPE + COMMA_SEP +
                        AppConstant.Order.SERVICE_AMOUNT_KEY.toString() + REAL_TYPE + COMMA_SEP +
                        AppConstant.Order.TAX_AMOUNT_KEY.toString() + REAL_TYPE + COMMA_SEP +
                        AppConstant.Order.TOTAL_AMOUNT_KEY.toString() + REAL_TYPE +
                        " )";
        return SQL_CREATE_TABLE;
    }

    private static String buildCreateOrderItemsTable(){
        final String SQL_CREATE_TABLE =
                "CREATE TABLE IF NOT EXISTS " + AppConstant.Histories.ORDER_ITEMS_TABLE_NAME.toString() + " (" +
                        AppConstant.OrderItems.ID_KEY.toString() + INTEGER_TYPE + COMMA_SEP +
                        AppConstant.OrderItems.ORDER_ID_KEY.toString() + INTEGER_TYPE + COMMA_SEP +
                        AppConstant.OrderItems.FOOD_ID_KEY.toString() + INTEGER_TYPE + COMMA_SEP +
                        AppConstant.OrderItems.FOOD_NAME_KEY.toString() + TEXT_TYPE + COMMA_SEP +
                        AppConstant.OrderItems.QUANTITY_KEY.toString() + REAL_TYPE + COMMA_SEP +
                        AppConstant.OrderItems.NOTES_KEY.toString() + TEXT_TYPE + COMMA_SEP +
                        AppConstant.OrderItems.STATUS_KEY.toString() + INTEGER_TYPE + COMMA_SEP +
                        AppConstant.OrderItems.SIZE_KEY.toString() + TEXT_TYPE + COMMA_SEP +
                        AppConstant.OrderItems.TV_STATUS_KEY.toString() + INTEGER_TYPE + COMMA_SEP +
                        AppConstant.OrderItems.LAST_UPDATE_KEY.toString() + NUMERIC_TYPE + COMMA_SEP +
                        AppConstant.OrderItems.CANCELED_NOTE_KEY.toString() + TEXT_TYPE + COMMA_SEP +
                        AppConstant.OrderItems.REJECT_NOTE_KEY.toString() + TEXT_TYPE + COMMA_SEP +
                        AppConstant.OrderItems.RETURN_NOTE_KEY.toString() + TEXT_TYPE +
                        " )";
        return SQL_CREATE_TABLE;
    }

    private static String buildCreateOrderServicesTable(){
        final String SQL_CREATE_TABLE =
                "CREATE TABLE IF NOT EXISTS " + AppConstant.Histories.ORDER_SERVICES_TABLE_NAME.toString() + " (" +
                        AppConstant.OrderServices.SERVICE_ID_KEY.toString() + INTEGER_TYPE + COMMA_SEP +
                        AppConstant.OrderServices.ORDER_ID_KEY.toString() + INTEGER_TYPE + COMMA_SEP +
                        AppConstant.OrderServices.SERVICE_NAME_KEY.toString() + TEXT_TYPE + COMMA_SEP +
                        AppConstant.OrderServices.AMOUNT_KEY.toString() + REAL_TYPE +
                        " )";
        return SQL_CREATE_TABLE;
    }

    private static String buildCreateOrderTaxTable(){
        final String SQL_CREATE_TABLE =
                "CREATE TABLE IF NOT EXISTS " + AppConstant.Histories.ORDER_TAX_TABLE_NAME.toString() + " (" +
                        AppConstant.OrderTax.TAX_ID_KEY.toString() + INTEGER_TYPE + COMMA_SEP +
                        AppConstant.OrderTax.ORDER_ID_KEY.toString() + INTEGER_TYPE + COMMA_SEP +
                        AppConstant.OrderTax.TAX_NAME_KEY.toString() + TEXT_TYPE + COMMA_SEP +
                        AppConstant.OrderTax.AMOUNT_KEY.toString() + REAL_TYPE +
                        " )";
        return SQL_CREATE_TABLE;
    }
}
