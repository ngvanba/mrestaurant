package com.ballard.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by ballard on 3/5/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 4;
    public static final String DATABASE_NAME = "MenuRestaurant.db";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS Restaurant");
        db.execSQL("DROP TABLE IF EXISTS order_items");
        db.execSQL("DROP TABLE IF EXISTS histories");
        db.execSQL(SQLBuilder.getSQL(SQLBuilder.SQL_CREATE_TABLE_RESTAURANT));
        db.execSQL(SQLBuilder.getSQL(SQLBuilder.SQL_CREATE_TABLE_HISTORIES));
        db.execSQL(SQLBuilder.getSQL(SQLBuilder.SQL_CREATE_TABLE_ORDER_ITEMS));
        Log.i("SQL:", (SQLBuilder.getSQL(SQLBuilder.SQL_CREATE_TABLE_ORDER_ITEMS)));
        db.execSQL(SQLBuilder.getSQL(SQLBuilder.SQL_CREATE_TABLE_ORDER_SERVICES));
        Log.i("SQL:", (SQLBuilder.getSQL(SQLBuilder.SQL_CREATE_TABLE_ORDER_SERVICES)));
        db.execSQL(SQLBuilder.getSQL(SQLBuilder.SQL_CREATE_TABLE_ORDER_TAX));
        Log.i("SQL:", (SQLBuilder.getSQL(SQLBuilder.SQL_CREATE_TABLE_ORDER_TAX)));
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        //db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

}
