package com.ballard.responses;

public interface AsyncResponse {
    void processFinish(String output);
}
