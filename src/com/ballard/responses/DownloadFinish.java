package com.ballard.responses;

/**
 * Created by ballard on 3/19/2016.
 */
public interface DownloadFinish {
    void processComplete(Object results);
}
